const nodemailer = require('nodemailer')
const { Converter } = require('showdown')

let md = new Converter({
  simplifiedAutoLink: true, // automatically convert links...
  excludeTrailingPunctuationFromURLs: true, // ...but not the trailing punctuation
  simpleLineBreaks: true, // use two line breaks to start a new paragraph
  emoji: true, // because why not 😉
})

class HoolMailer {
  constructor (options) {
    if (!options) throw 'No options defined. How to send email?'
    if (options.dummy) {
      // dummy set up; no emails will actually be sent
      this.dummy = true
    } else {
      if (!options.host) throw 'Please specify the email host'
      if (!options.username) throw 'Please specify username for authentication'
      if(!options.password) throw 'Please specify password for authentication'

      if (!options.defaultFrom) {
        console.warn(`No default email sender specified! Using ${options.username} instead`)
        this.defaultFrom = options.username
      } else {
        this.defaultFrom = options.defaultFrom
      }

      this.host = options.host
      this.username = options.username
      this.password = options.password
      this.port = options.port || 587
      this.secure = options.secure == false ? false : true

      this.transport = nodemailer.createTransport({
        host: this.host,
        port: this.port,
        secure: this.secure,
        auth: {
          user: this.username,
          pass: this.password,
        },
      })
    }
  }

  async sendMail(message) {
    // auto-fill defaults
    if (!message.from) {
      message.from = this.defaultFrom
    }

    // add HTML version!
    if (!message.html) {
      message.html = md.makeHtml(message.text)
    }

    // send dummy or real emails as appropriate
    if (this.dummy) {
      console.log(`\n\n-------------------------- BEGIN EMAIL --------------------------\n`)
      console.log(`From: ${message.from}\nTo:${message.to}\nSubject:${message.subject||'(no subject)'}\n\n${message.text}`)
      console.log(`\n--------------------------- END EMAIL ---------------------------`)
    } else {
      console.log(`Sending email: ${message.from} -> ${message.to}`)
      return await this.transport.sendMail(message)
    }
  }
}

module.exports = {
  HoolMailer,
}
