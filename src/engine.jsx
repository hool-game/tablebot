const mongoose = require('mongoose')
const uuid = require('uuid')
const { jid } = require('@xmpp/component')
const { EventEmitter } = require('events')
const crypto_shuffle = require('crypto-shuffle')

// serialiser (for email verification)
const { URLSafeTimedSerializer } = require("@constemi/itsdangerjs")

// config section (change these to change settings)

const SUITS = new Map([
  ['C', 'clubs'],
  ['D', 'diamonds'],
  ['H', 'hearts'],
  ['S', 'spades'],
])

const RANKS = new Map ([
  ['2', 'two'],
  ['3', 'three'],
  ['4', 'four'],
  ['5', 'five'],
  ['6', 'six'],
  ['7', 'seven'],
  ['8', 'eight'],
  ['9', 'nine'],
  ['10', 'ten'],
  ['J', 'jack'],
  ['Q', 'queen'],
  ['K', 'king'],
  ['A', 'ace'],
])

const SIDES = new Map([
  ['N', 'North'],
  ['E', 'East'],
  ['S', 'South'],
  ['W', 'West'],
])

// card object type
class Card {
  constructor(v) {
    const card_re = new RegExp(
        '^(?<suit>(' +
        [...SUITS.keys()].join('|') +
        '))(?<rank>(' +
        [...RANKS.keys()].join('|') +
        '))$', 'i')

    let r = card_re.exec(v)
    if (!r) throw TypeError(`Invalid card value: ${v}`)

    this.value = v
    this.suit = r.groups.suit.toUpperCase()
    this.rank = r.groups.rank.toUpperCase()
  }

  toBSON() {
    return this.value
  }

  static makeDeck() {
    var deck = []
    for (let s of SUITS.keys()) {
      for (let r of RANKS.keys()) {
        deck.push(new this(`${s}${r}`))
      }
    }
    return deck
  }

  /*
   * Decides whether one card is greater or less
   * than another. Compares first by suit and then
   * by rank.
   *
   * returns:
   *   difference between suits if suits are different
   *   difference between ranks if ranks are different
   *   0 if both suit and rank are the same
   *
   * "difference" can be a positive or negative number, depending
   * on whether the first card is of higher or lower value than
   * the second.
   */

  static compare(c1, c2) {
    // compare by suit then rank
    let s1 = [...SUITS.keys()].indexOf(c1.suit)
    let s2 = [...SUITS.keys()].indexOf(c2.suit)

    // return by suit value if different
    if (s1 != s2) {
      return s1 - s2
    }

    // otherwise, suit must be same
    // compare by rank instead
    let r1 = [...RANKS.keys()].indexOf(c1.rank)
    let r2 = [...RANKS.keys()].indexOf(c2.rank)

    return r1 - r2
  }

  static shuffle(deck) {
    return crypto_shuffle(deck)
  }

  // Also known as "sort cards"
  static unshuffle(deck) {
    deck.sort(Card.compare)
    return deck
  }
}

class BareJID extends jid.JID {
  toBSON() {
    return this.bare().toString()
  }
}

// define schemas
class BareJIDSchema extends mongoose.SchemaType {
  cast(v) {
    // if it's not a JID, convert to one
    if (v.constructor != BareJID && v.constructor != jid.JID) {
      v = jid(v).bare()
    }

    // if it's a JID but not a bare one, convert
    if (v.constructor == jid.JID) {
      v = new BareJID(v.local, v.domain)
    }

    // now it's definitely a BareJID, so return away :)
    return v
  }
}

mongoose.Schema.Types.BareJID = BareJIDSchema

var emailSchema= new mongoose.Schema({
  address: {
    type: String,
    required: true,
  },
  verified: {
    type: Boolean,
    default: false,
  }
})

var userSchema = new mongoose.Schema({
  jid: {
    type: BareJID,
    required: true,
  },
  name: {
    type: String,
    required: false,
  },
  email: {
    type: emailSchema,
    required: true,
    default: {
      address: null,
      verified: false,
    }
  },
  location: {
    type: String,
    default: 'The Internet',
  },
  status: {
    type: String,
    required: true,
    default: 'unverified',
    enum: [
      'unverified',
      'active',
      'banned',
    ]
  }
})

userSchema.statics.exists =  async function(user_jid) {
  // strip resource: use bare JID only
  if (user_jid.constructor == jid.JID) {
    user_jid = user_jid.bare()
  } else {
    // also convert to JID while we're about it
    user_jid = jid(user_jid).bare()
  }

  return await this.countDocuments({jid: user_jid}) >= 1
}

class CardSchema extends mongoose.SchemaType {
  cast(v) {
    // if it's a card, don't convert
    if (v.constructor == Card) {
      return v
    }

    return new Card(v)
  }
}

mongoose.Schema.Types.Card = CardSchema

var bidSchema = new mongoose.Schema({
  side: {
    type: String,
    enum: [...SIDES.keys(), null], // N, E, S, W
  },
  bidType: {
    type: String,
    enum: [
      'level',
      'redouble',
      'double',
      'pass',
    ],
  },
  level: Number,
  suit: {
    type: String,
    enum: [...SUITS.keys(), 'NT'],
  },
})

var bidRoundSchema = new mongoose.Schema({
  bids: [bidSchema],
  winnerIndex: {
    type: Number, // indexes to this.bids
  },
})

bidRoundSchema.methods.getWinningBid = function(dealer) {
  if (!dealer) {
    throw Error("Right now, this function only works if you specify a dealer")
  }
  // FIXME: get dealer from table instead of having to receive it here

  // calculate winner in necessary
  if (!this.winnerIndex) {

    // ordered list of modifiers
      let modifiers = [
        'pass',
        'double',
        'redouble',
        'level',
      ]

    // first, collect level bids
    let levelBids = this.bids.filter(b => b.bidType == 'level')

    // if no level bids, get by highest modifier
    if (levelBids.length == 0) {

      let sortedBids = this.bids.sort((b1, b2) =>
        modifiers.indexOf(b1.bidType) - modifiers.indexOf(b2.bidType)
      ).reverse()

      // get the max modifier by sorting, then matching against highest
      let highestModifiers = this.bids.filter(b =>
        b.bidType == sortedBids[0].bidType)

      // set it
      this.winnerIndex = this.bids.findIndex(b =>
        b.side == highestModifiers[0].side)
    } else {
      // otherwise, compare level bids

      // same formula, but sorting by number this time
      let highestLevel = levelBids.sort((b1, b2) =>
          b1.level - b2.level
        ).reverse()[0].level

      let highestNumbers = levelBids.filter(b =>
        b.level == highestLevel)

      if (highestNumbers.length == 1) {

        // set it
        this.winnerIndex = this.bids.findIndex(b =>
          b.side == highestNumbers[0].side)

      } else {

        // compare by suit
        let highestSuit = highestNumbers.sort((b1, b2) =>
          [...SUITS.keys(), 'NT'].indexOf(b1.suit) - [...SUITS.keys(), 'NT'].indexOf(b2.suit)
        ).reverse()[0].suit
        let highestSuits = highestNumbers.filter(b =>
          b.suit == highestSuit)

        if (highestSuits.length == 1) {

          // set it
          this.winnerIndex = this.bids.findIndex(b =>
            b.side == highestSuits[0].side)
        } else {

          // compare by dealer

          // first, check proximity to dealer (going anticlockwise)
          let positionChange = { 'N': 0, 'E': 1, 'S': 2, 'W': 3 }
          let i = positionChange[dealer]
          let positionHierarchy = {
            N: (3+i) % 4,
            E: (2+i) % 4,
            S: (1+i) % 4,
            W: (0+i) % 4,
          }

          // bigger is better
          let highestPlayers = highestSuits.sort((b1, b2) =>
            positionHierarchy[b1.side] - positionHierarchy[b2.side]
          ).reverse()

          // set it
          this.winnerIndex = this.bids.findIndex(b =>
            b.side == highestPlayers[0].side)
        }
      }
    }
  }

  return this.bids[this.winnerIndex]
}

bidRoundSchema.methods.getDeclarer = function(dealer) {
  if (!dealer) {
    throw Error("Right now, this function only works if you specify a dealer")
  }
  // FIXME: get dealer from table instead of having to receive it here

  return this.getWinningBid(dealer).side
}

var tableSchema = new mongoose.Schema({
  table_id: String,

  // room details

  occupants: [{
    jid: String, // JID of user
    role: {
      type: String,
      default: 'kibitzer',
      enum: ['player', 'kibitzer','inactive'],
    },
    seat: {
      type: String,
      enum: [...SIDES.keys(), null], // N, E, S, W
    },
    seenHands: {
      type: Map, // one of: N, E, S, W (see SIDES)
      of: {
        seen:{
          type: Boolean,
          default: false,
        }
      },

      // auto-fill with all false
      default: new Map([...SIDES.keys()].map(s => [ s, { seen: false } ])),
    },
    status: {
      type: String,
      default: 'active',
      enum: ['invited', 'waiting', 'active', 'banned', 'unavailable', 'left'],
    },
    joinTime: {
      type: Date,
      default: Date.now,
    },
    host: {
      type: Boolean,
      default: false,
    },
    seen_deal: {
      type: Boolean,
      default: false,
    },
  }],

  // seat details

  seats: {
    type: Map, // one of: N, E, S, W (see SIDES)
    of: {
      user: String, // JID of user
      undoCount: {
        type: Number,
        default: 0,
      },
      maxUndoCount: {
        type: Number,
        default: 5,
      },
      cards: [Card],
      infoShared: [{
        infoType: {
          type: String,
          enum: [
            'HCP',
            'C',
            'D',
            'H',
            'S',
            'pattern'
          ],
        },
        infoValue: [Number], // list for pattern, else single number
      }],
    },
  },

  gameHistory:[{
    seats: {
      type: Map, // one of: N, E, S, W (see SIDES)
      of: {
        user: String,
        cards: [Card],
      }
    },
    dealOutTime: {
      type: Date,
      default: Date.now,
    }
  }],

  openSides: {
    type: Map, // one of: N, E, S, W (see SIDES)
    of: {
      cards: [Card],
    },
  },


  claimedTricks: {
    NS: {
      type: Number,
      default: 0
    },
    EW :{
      type: Number,
      default: 0
    },
    claimant:{
      type: String
    }
  },

  claimRequest: {
    side: {
      type: String,
      enum: [...SIDES.keys()],
    },
    amount :{
      type: Number,
      default: 0
    },
    requestees:[{
      side: {
        type: String,
        enum: [...SIDES.keys()],
      },
      response:{
        type: Boolean,
        default: false
      }
    }],
  },
  // game details

  stage: {
    type: String,
    enum: [
      'scoreboard',
      'inactive',
      'infosharing',
      'bidding',
      'play',
      'deleted'
    ],
    default: 'inactive',
  },

  dealer: {
    type: String,
    enum: [...SIDES.keys()],
    default: 'N',
  },

  turn: {
    type: String,
    enum: [...SIDES.keys()],
    default: 'N',
  },

  bids: [bidRoundSchema],
  // bidRound: function() { return this.bids.length },

  contract: {
    level: Number,
    suit: {
      type: String,
      enum: [...SUITS.keys(), 'NT'],
      default: 'NT',
    },
    double: Boolean,
    redouble: Boolean,
    declarer: {
      type: String,
      enum: [...SIDES.keys()],
      default: 'N',
    },
  },

  tricks: [{
    cards:[{
      side: {
        type: String,
        enum: [...SIDES.keys()],
      },
      card: Card,
    }],
    // suit: function () { return this.cards[0].card.suit },
    // leader: function() { return this.cards[0].side },
    winner: Number, // index of card in this.cards
  }],

  runningScore: {
    type: Map, // represents side: NS or EW
    of: {
      type: Number,
      default: 0,
    },
  },

  undoRequest: {
    side: {
      type: String,
      enum: [...SIDES.keys()],
    },
    suit: {
      type: String,
      enum: [...SUITS.keys()],
    },
    rank: {
      type: String,
      enum: [...RANKS.keys()],
    },
    requestees:[{
      side: {
        type: String,
        enum: [...SIDES.keys()],
      },
      response:{
        type: Boolean,
        default: false
      }
    }],
  },

  readyToPlay:[],
  notReady:[],

  changeDealVotes: [{
    requestee: {
      type: String
    },
    type: {
      type: String
    }
  }]
})


// quickly (blindly) create an empty table
//make sure you are passing jid without resource
tableSchema.statics.createNewTable = async function(jid) {
  table = new this({
    table_id: jid,
    seats: new Map(),
    openSides: new Map(),
    occupants: [],
    stage: 'inactive',
    tricks: [],
    readyToPlay:[],
    notReady:[],
    gameHistory: [],
    runningScore: new Map([ // TODO: make dynamic
      ['NS', 0],
      ['EW', 0],
    ]),
  })

  // set up empty sides
  for (let side of [...SIDES.keys()]) {
    table.seats.set(side, {
      user: null,
      cards: [],
      infoShared: [],
    })
  }

  return table
}

// Fetch or create a new table by ID
tableSchema.statics.getTableById = async function(table_jid) {
  table_jid = jid(table_jid).bare(); // returns a JID without resource
  var table = await this.findOne({ table_id: table_jid })

  if (!table) {
    // set up new table
    table = await this.createNewTable(table_jid)
  }

  return table
}

// Fetch or create a new table by ID
tableSchema.statics.getActiveTableById = async function(table_jid) {
  table_jid = jid(table_jid).bare();// returns a JID without resource
  var table = await this.findOne({ table_id: table_jid })
   return table
}

// Fetch the active table list
tableSchema.statics.getActiveTables = async function() {
  var tables = await this.find({ stage: { $ne: 'deleted' } })
  return tables
}

// Get the player sitting opposite a certain side
tableSchema.methods.getPartner = function (side) {
  let sides = [...SIDES.keys()]
  let partnerDistance = Math.floor(sides.length/2)
  return sides[(sides.indexOf(side) + partnerDistance) % sides.length]
}

// Get the opponents of a certain side
tableSchema.methods.getOpponents = function (side) {
  let partner = this.getPartner(side)

  // return all sides which are neither this side nor its partner
  return [...SIDES.keys()].filter(s => ![side, partner].includes(s))
}

// Move the given user to a given seat
tableSchema.methods.changeSeat = function (userIndex, seat) {
  // user can be found at this.occupants[userIndex]

  // first, fail if seat already occupied
  // the !!seat is to skip this test when seat is null (kibitzing)
  if (!!seat && !!this.seats.get(seat).user) {
    throw Error(`Cannot move user to ${seat}: already occupied`)
  }

  // remove from existing seat if neccessary
  if (!!this.occupants[userIndex].seat) {
    let s = this.seats.get(this.occupants[userIndex].seat)
    s.user = null
    this.seats.set(this.occupants[userIndex].seat, s)
  }

  // empty seat implies kibitzer
  if (!seat || seat == 'none') {
    this.occupants[userIndex].seat = null
    this.occupants[userIndex].role = 'kibitzer'

    // Mark all sides as seenHands for Kibitzers
    for (let side_key of [...SIDES.keys()]) {
      let seenHand = this.occupants[userIndex].seenHands.get(side_key)
      seenHand.seen = true
      this.occupants[userIndex].seenHands.set(side_key, seenHand)
    }
  } else {
    // add to new seat
    let s = this.seats.get(seat)
    s.user = this.occupants[userIndex].jid
    this.seats.set(seat, s)

    // add to new seen seat
    let seenHand = this.occupants[userIndex].seenHands.get(seat)
    seenHand.seen = true
    this.occupants[userIndex].seenHands.set(seat, seenHand)

    // update user
    this.occupants[userIndex].seat = seat
    this.occupants[userIndex].role = 'player'
  }
}

// Get the index of a particular user in the occupants list
tableSchema.methods.userIndex = function (userJID) {
  return this.occupants.findIndex(u => {
    return u.jid == userJID
  })
}

// Create a random deck and deal the cards out to each player
tableSchema.methods.dealCards = function() {
  let deck = Card.shuffle(Card.makeDeck())

  let seats = [...this.seats.entries()]
  let currentSeat = 0

  for (c of deck) {
    currentSeat = (currentSeat + 1) % seats.length
    seats[currentSeat][1].cards.push(c)
  }

  let historySeats = {}
  for (seat of seats) {
    seat[1].cards = Card.unshuffle(seat[1].cards)
    this.set(seat[0], seat[1])
    historySeats[seat[0]] =  {cards:seat[1].cards, user:seat[1].user}
  }

  this.gameHistory.push({seats:historySeats});
}


// Put back the played cards back to deck and deal the cards out to each player
tableSchema.methods.putBackCardsFromTrick = function() {

  let tricks = [...this.tricks]

  for (let index = tricks.length-1; index >= 0; index--) {
    const trick = tricks[index];
    for(let card of trick.cards){
      let s = this.seats.get(card.side)
      // s.cards.push(card)
      s.cards.push(`${card.card.suit}${card.card.rank}`)
      this.seats.set(card.side, s)
    }
  }

  this.seats.forEach((v,k) => {
    v.cards = Card.unshuffle(v.cards)
    this.set(k,v)
  })

  //Reset to empty
  this.tricks = []

}

// Clear all info and set stage to inactive
tableSchema.methods.tableRedeal = async function() {

  this.tricks = []
  this.seats.forEach((v,k) => {
    v.infoShared = []
    v.cards = []
    this.set(k,v)
  })
  this.bids = []
  this.contract = {
    suit:"NT",
    declarer:"N"
  }
  this.claimRequest = []
  this.changeDealVotes = []
  this.undoRequest = []
  this.runningScore = new Map([
    ['NS', 0],
    ['EW', 0],
  ])
  this.stage = 'inactive'

}

// Reset all info and set stage to inactive
tableSchema.methods.tableResetDeal = async function() {

  if(this.stage == 'scoreboard')
  {
    let trickScore = this.trickScore()
    let dealScore = this.hoolScore(this.contract,trickScore['NS'],trickScore['EW'])

    //update running score
    this.runningScore.set('NS', this.runningScore.get('NS') - dealScore['NS'])
    this.runningScore.set('EW', this.runningScore.get('EW') - dealScore['EW'])

    //Set to previous dealer
    let sides = [...SIDES.keys()]

    this.dealer = sides[(sides.indexOf(this.dealer) - 1) % sides.length]
  }

  this.seats.forEach((v,k) => {
    v.infoShared = []
    this.set(k,v)
  })
  this.bids = []
  this.contract = {
    suit:"NT",
    declarer:"N"
  }
  this.claimRequest = []
  this.changeDealVotes = []
  this.undoRequest = []

  this.stage = 'inactive'

}
// go to next turn (based on SIDES; looping round at end)
tableSchema.methods.incrementTurn = function() {
  let sides = [...SIDES.keys()]

  this.turn = sides[(sides.indexOf(this.turn) + 1) % sides.length]
  return this.turn
}

// calculate final contract (when bidding is over)
tableSchema.methods.getContract = function() {
  let finalContract = {
    level: null,
    suit: null,
    double: false,
    redouble: false,
    declarer: null,
  }

  // go through bids backwards
  for (i=this.bids.length-1;i>=0;i--) {
    let thisBid = this.bids[i].getWinningBid(this.dealer)

    // process based on winning bid type
    if (thisBid.bidType == 'double') {
      finalContract.double = true
    } else if (thisBid.bidType == 'redouble') {
      finalContract.redouble = true
    } else if (thisBid.bidType == 'level') {
      finalContract.level = thisBid.level
      finalContract.suit = thisBid.suit
      finalContract.declarer = thisBid.side
      break
    }
  }

  // clear 'all pass' contract
  if (!finalContract.level || !finalContract.suit) {
    finalContract = {}
  }

  // save it
  this.contract = finalContract

  return finalContract
}

// get info (suit count, pattern or HCP)
tableSchema.methods.getHandInfo = function(infoType, sideOrArray) {
  // if it's a side, fetch the cards ourselves
  if (typeof(sideOrArray) == 'string') {
    sideOrArray = this.seats.get(sideOrArray).cards
  }

  // now, either way, assume we have an array of Cards to work with

  if (infoType == 'pattern') {
    // count cards in each suit
    let suitMap = new Map()

    for (let card of sideOrArray) {
      // increment count
      let c = suitMap.get(card.suit) || 0
      suitMap.set(card.suit, c+1)
    }

    // sort and return the counts we have
    return [...suitMap.values()].sort()
  }

  else if (infoType == 'HCP') {
    // count high card points
    let count = 0

    for (let card of sideOrArray) {
      // increment count
      switch(card.rank) {
        case 'A':
          count += 4
          break
        case 'K':
          count += 3
          break
        case 'Q':
          count += 2
          break
        case 'J':
          count += 1
          break
      }
    }

    return count
  }

  else if ([...SUITS.keys()].indexOf(infoType) >= 0) {
    let count = sideOrArray.filter(c => c.suit == infoType).length

    return count
  }

  else {
    throw new TypeError(`Invalid info type: ${infoType}`)
  }
}

// get trick score (number of tricks for each side)
tableSchema.methods.trickScore = function() {
  let tricks_NS = 0
  let tricks_EW = 0
  // TODO: make this generic (so it works regardless of name and
  // number of sides

  for(let t of this.tricks){

    // don't do empty tricks
    if (t.winner != null) {
      if(t.cards[t.winner] && ['N','S'].includes(t.cards[t.winner].side)){
        tricks_NS +=1
      } else if (t.cards[t.winner] && ['E','W'].includes(t.cards[t.winner].side)){
        tricks_EW +=1
      }
    }
  }

  return {
    'NS': tricks_NS,
    'EW': tricks_EW,
  }
}

tableSchema.methods.hoolScore = function(contract,tricks_NS,tricks_EW) {

  let declarerTricks
  let finalScore;
      if(tricks_NS+tricks_EW!=SUITS.size *RANKS.size/SIDES.size){
               throw Error(`something is wrong; tricks do not add: ${tricks_NS} + ${tricks_EW} != ${SUITS.size*RANKS.size/SIDES.size}`)
      }

      if(['N','S'].includes(contract.declarer)){
            declarerTricks = tricks_NS;

       } else if(['E','W'].includes( contract.declarer)){
             declarerTricks = tricks_EW;
         }

       else{
         throw Error(`Declarer must be one of N,S,E or W`)
       }

 if(declarerTricks>=(contract.level + 6)){
         let score =contract.level * 10 ;

    // Add bonus
         let bonuses =[0,100,200,400]
        let bonusAllocation={
                           1:{C:bonuses[0],D:bonuses[0],H:bonuses[0],S:bonuses[0],NT:bonuses[0]},
                           2:{C:bonuses[0],D:bonuses[0],H:bonuses[0],S:bonuses[0],NT:bonuses[0]},
                           3:{C:bonuses[0],D:bonuses[0],H:bonuses[0],S:bonuses[0],NT:bonuses[1]},
                           4:{C:bonuses[0],D:bonuses[0],H:bonuses[1],S:bonuses[1],NT:bonuses[1]},
                           5:{C:bonuses[1],D:bonuses[1],H:bonuses[1],S:bonuses[1],NT:bonuses[1]},
                           6:{C:bonuses[2],D:bonuses[2],H:bonuses[2],S:bonuses[2],NT:bonuses[2]},
                           7:{C:bonuses[3],D:bonuses[3],H:bonuses[3],S:bonuses[3],NT:bonuses[3]}
                         }
           console.log('contract.level', contract.level)
           score += bonusAllocation[String(contract.level)][contract.suit]

        //add overtrick bonus
           score += (declarerTricks - (contract.level + 6)) * 10

         //save score
         if(['N','S'].includes(contract.declarer)){
               finalScore = {'NS':score , 'EW':0}
         }
         else{
               finalScore = {'EW':score, 'NS':0}
         }
 }

 else {
   score = ((contract.level + 6) - declarerTricks) * 20

   if(['N','S'].includes(contract.declarer)){
         finalScore={'NS':0, 'EW':score}
         }
     else{
       finalScore={'EW':0, 'NS':score}
   }
 }


    //apply double and redouble
    if(contract.redouble) {
             finalScore={
                      'NS':finalScore['NS']*4,
                      'EW':finalScore['EW']*4
                       }
     }
     else if(contract.double){
         finalScore={
                     'NS':finalScore['NS']*2,
                     'EW':finalScore['EW']*2
                   }
     }

  return finalScore
}

// Check whether table has claim Request
tableSchema.methods.hasClaimRequest = function() {
  return this.claimRequest && this.claimRequest.side && this.claimRequest.requestees && this.claimRequest.requestees.length
}

// Check whether table has claim Request
tableSchema.methods.isFaithfulUser = function(userIndex, seat) {
  let seenHand = this.occupants[userIndex].seenHands.get(seat)
  return !seenHand.seen
}

tableSchema.methods.isAllowedToSit= function(userIndex, seat) {
	let allowed = false

	// Allowed to sit if user in New game and scoreboard stage
	if(['inactive','scoreboard'].indexOf(this.stage) >= 0){
	  allowed = true
	}

	//other stage required faithfullness
	if(['bidding','play','infosharing'].indexOf(this.stage) >= 0){
    if(userIndex >= 0)
    {
      if(this.occupants[userIndex].seat==seat || this.occupants[userIndex].seat){ //wants to sit and connect in same seat
        allowed = true
      }
      else if(this.isFaithfulUser(userIndex, seat)){
        allowed = true
      }
      else
        return false
    }

    //cardplay allow anyone to take dummy's seat
    if(this.stage == 'play'){
      if(seat == this.getPartner(this.contract.declarer) ){
        allowed = true
      }
    }
	}

  return allowed
}

tableSchema.methods.clearUser = function(user) {
  let userIndex = this.userIndex(user)

  if (userIndex < 0) {
    throw Error(`${user} User does not exist`)
  }

  if(this.occupants[userIndex].role == 'player')
  {
    let s = this.seats.get(this.occupants[userIndex].seat)
    s.user = null
    this.seats.set(this.occupants[userIndex].seat, s)
  }

  //clear role and status
  this.occupants[userIndex].status = 'left'
  this.occupants[userIndex].role = 'inactive'
  this.occupants[userIndex].invite = {} //empty invites

  if (this.occupants[userIndex].host == true) {
    this.occupants[userIndex].host = false
    let nextHostList = this.occupants.filter(
      o => { return (o.status == 'active' && o.role == 'player' && o.jid != this.occupants[userIndex].jid)}
    )
    if(nextHostList.length === 0){
      return null;
    }
    else{
      let nextHost = nextHostList.sort((h1, h2) =>
      {
        let a = new Date(h1.joinTime)
        let b = new Date(h2.joinTime)
        if (a > b)
          return 1
        if (a < b)
          return -1
        return 0
      }
      )[0]
      if(nextHost)
      {
        let nextHostIndex = this.userIndex(nextHost.jid)
        this.occupants[nextHostIndex].host = true
      }
    }
  }
  let currentHost = this.occupants.filter(
    o => { return (o.host == true)}
  )
  return currentHost.length ? currentHost[0].jid: null
}

//Scoreboard Mark ready
tableSchema.methods.markReady = function(side) {

  //add side to ready to play
  this.readyToPlay.push(side)
  //remove from not Ready
  this.notReady.pop(side)

  if(this.occupants.filter( o => o.status == 'active' && o.role == 'player').length== 4 && this.readyToPlay.filter((item, i, ar) => ar.indexOf(item) === i).length==4){
    //Set stage to info
    this.stage = 'infosharing'
    this.openSides = new Map()
    this.seats.forEach((v,k) => {
      v.cards = []
      v.infoShared = []
      this.set(k,v)
    })

    //deal our cards
    if ([...this.seats.values()].every(s => s.cards.length == 0)) {
      this.dealCards()

      // add to new seen seat
      this.occupants.forEach((o,k) => {

        if(o.role == 'player'){
          //Reset all the sides to false
          for (let side_key of [...SIDES.keys()]) {
            let seenHand = this.occupants[k].seenHands.get(side_key)
            seenHand.seen = false
            this.occupants[k].seenHands.set(side_key, seenHand)
          }
          let seenHand = o.seenHands.get(o.seat)
          seenHand.seen = true
          this.occupants[k].seenHands.set(o.seat, seenHand)
        }
        else{
          // Mark all sides as seenHands for Kibitzers
          for (let side_key of [...SIDES.keys()]) {
            let seenHand = this.occupants[k].seenHands.get(side_key)
            seenHand.seen = true
            this.occupants[k].seenHands.set(side_key, seenHand)
          }
        }
      })

      // ...where "etc" includes bids...
      this.bids = []

      // ...and tricks!
      this.tricks = []

      //Set Claim Request as null
      this.claimRequest = null

      //Set Undo Request as empty
      this.undoRequest = {}
    }
    return true
  } else {


    return false
  }
}

var User = mongoose.model('User', userSchema)
var Table = mongoose.model('Table', tableSchema)


// Main Engine

class HoolEngine extends EventEmitter {

    /**
     * Constructor for HoolEngine
     *
     * A helpful function that sets everything up
     * for us in one go instead of having to run
     * multiple commands.
     *
     * Currently supported options (inside the
     * `options` object) are:
     *
     *  - mongoConnection - the database connection
     *  - transmitter - a HoolTransmitter object
     *
     * More options may be added if we need them, but
     * that's about it for now!
     */

    constructor(options = {}, ...args) {
      // call the parent constructor first
      super(...args)

      if (!!options.db) {
        this.setDB(options.db)
      }

      if (!!options.transmitter) {
        this.listen(options.transmitter)
      }

      if (!!options.mailer) {
        this.mailer = options.mailer
      }

      /*
       * FIXME: the following parameters are only required for
       * user registration. They should ideally be moved to
       * somewhere outside the engine.
       *
       * Once moved, the references to these variables will also
       * have to be updated in server.jsx.
       */

      if (!!options.serialiser) {
        this.serialiser = options.serialiser
      } else  if (!!options.serializer) { // spell check!
        this.serialiser = options.serializer
      } else {
        this.serialiser = URLSafeTimedSerializer
      }

      if (!!options.secret_key) {
        this.secret_key = options.secret_key
      } else {
        console.warn('No secret key specified for HoolEngine! You will not be able to register new users.')
      }

      if (!!options.http_domain) {
        this.http_domain = options.http_domain
      } else {
        console.warn(`No HTTP domain specified for HoolEngine! XMPP domain will be used as default`)
      }

      // for direct server interaction
      if (!!options.serverHandler) {
        this.serverHandler = options.serverHandler
      }
    }

    setDB(mongoConnection) {
      this.db = mongoConnection
    }

    // table list functions (for jabber:iq:search)
    async getTableList() {
      return await Table.getActiveTables()
    }

    async getTableInfo(table_id) {
      return await Table.getActiveTableById(table_id)
    }

    /**
     * Auth handlers
     *
     * These functions are to handle user management and authentication.
     * They are more likely to be tightly integrated with the XMPP
     * server; however we are going with standardised interfaces as
     * much as possible. Ideally, software-specific functionality
     * would be broken out into different modules.
     */

    async getUserInfo(user_jid) {
      return await User.findOne({jid: user_jid})
    }

    /*
     * FIXME: these user management functions use quite a few additional
     * libraries (eg. itsdangerjs, nodemailer) which aren't used
     * elsewhere in the engine code. The same goes for some of the
     * variables like secret_key and serialiser.
     *
     * Ideally, these special-case functions should be moved out to a
     * separate library and imported here in a sensible fashion.
     */
    setMailer(mailer) {
      this.mailer = mailer
    }

    setServerHandler(serverHandler) {
      this.serverHandler = serverHandler
    }

    async handleRegistration (registration, useJSON=false, defaultDomain=null) {

      // FIXME: validate data properly before proceeding!
      if (!registration.username || !registration.email) {
        registration.error = {
          type: 'modify',
          tag: 'not-acceptable',
          text: `Please specify a username and email`,
        }

        return registration
      }

      // to calculate the user JID later down the line
      let userJID

      /*
       * for XMPP-based registration only: the user must be already
       * authenticated with an existing XMPP server. In that case, the
       * username must match the current XMPP username and will
       * thenceforward be linked with the XMPP account.
       *
       * If the user is not logged in (or is logging in via the HTTP
       * API) then we will handle that case later by attempting to
       * create an account on the spot.
       */

      if (!!registration.from) {

        /*
         * XMPP-based registration
         *
         * check that both usernames are the same (the one on the
         * main XMPP server and the local Hool table service one).
         * If they are not the same, the user is not allowed to register.
         */

        if (registration.from.local != registration.username) {
          registration.error = {
            type: 'cancel',
            tag: 'conflict',
            text: `Component username (${registration.username}) does not match current XMPP username (${registration.from.local}).`,
          }

          return registration
        }

        // compute local JID
        userJID = jid(`${registration.username}@${registration.from.domain}`)

        // check for username conflict
        if (!!await User.findOne({jid: userJID.toString()})) {
          registration.error = {
            type: 'cancel',
            tag: 'conflict',
            text: 'That username is already taken. Please try another one.',
          }

          return registration
        }
      } else {
        /*
         * HTTP-based registration
         *
         * we need to try creating an XMPP account before registering
         * our local component account. If the XMPP account creation
         * fails (due to username conflict or some other reason) then
         * we will, too.
         *
         * While creating the account, a domain will automatically be
         * specified based on the default configuration.
         */

        // first, check that the server handler even exists
        if (!this.serverHandler) {
          console.error('No server handler. Password cannot be changed.')

          registration.error = {
            type: 'cancel',
            tag: 'not-implemented',
            text: 'Unfortunately, this server does not support HTTP registration.',
          }

          return registration
        }

        // compute local JID based on default setting
        try {
          userJID = jid(`${registration.username}@${defaultDomain || this.server.defaultDomain || this.transmitter.domain}`)
        } catch (e) {
          registration.error = {
            type: 'modify',
            tag: 'not-acceptable',
            text: 'Invalid uername format',
          }
          return registration
        }

        // try to actually create the account
        let result = await this.serverHandler.createUser(userJID, registration.password)

        if (result.status != 'ok') {
          // TODO: parse error message properly
          registration.error = {
            type: 'cancel',
            tag: 'conflict',
            text: `Something went wrong while registering account: ${result.error}`
          }
          return registration
        }
      }

      // enter user details
      let u = new User()
      u.jid = userJID
      u.name = registration.name
      u.email.address = registration.email
      u.location = registration.location

      // generate email verification key
      let serialiser = new this.serialiser(this.secret_key, u.jid.toString())
      let verification_link = `${this.http_domain||'https://' + this.transmitter.domain}/${ useJSON? 'api/' : '' }verify_email?jid=${encodeURIComponent(u.jid.toString())}&key=${encodeURIComponent(serialiser.dumps(u.email.address))}`

      // actually save the user
      await u.save()

      if (!!this.mailer) {
        // send email
        this.mailer.sendMail({
          to: u.email.address,
          subject: "Verify your Hool email address",
          text: `Hello ${u.name || u.jid.local},\n\nWelcome to your brand new Hool account! Before you get started, we need to do one more thing to verify your email address. Please click on the link below to activate your account:\n\n${verification_link}\n\nCheers 🙂\nThe Hool Team`
        })
      } else {
        // otherwise, print to console
        console.log('Email sending is not set up. Please direct users to this link:', verification_link)
      }

      // set JID
      registration.jid = userJID

      return registration
    }


    async handleEmailVerification(verification) {
      // fetch user and check that it matches
      let q = await User.updateOne({
        "jid": verification.jid,
        "email.address": verification.email,
      },
      {
        "email.verified": true,
        "status": "active",
      })

      /*
       * now, q has the following attributes:
       * n - number of items returned
       * nModified - number of items modified (not already updated)
       * ok - whether everything is okay or not
       */

      if (q.ok != 1) {
        // database operation failed for some reason
        verification.error = {
          type: 'cancel',
          tag: 'server-error',
          text: 'Something unexpected happened. Please contact the server admin immediately :('
        }
      } else if (q.n == 0) {
        // no matching records found
        verification.error = {
          type: 'cancel',
          tag: 'not-found',
          text: 'No matching user with that email address. Perhaps it was updated later?',
        }
      } else if (q.nModified == 0) {
        // records found but they've already been updated
        verification.verified = true
        verification.message = 'The email has already been verified!'
      } else {
        // everything went off as expected
        verification.verified = true
        verification.message = 'Your email has been verified.'
      }

      return verification
    }


    async handlePasswordReset(reset) {

      // first, check that the user exists
      let u = await User.findOne({
        jid: reset.jid,
        "email.address": reset.email,
      })

      if (!u) {
        reset.error = {
          type: 'cancel',
          tag: 'not-found',
          text: 'No matching user with that JID and email address.',
        }

        return reset
      }

      // see if we even support managing the server directly
      if (!this.serverHandler) {
        console.error('No server handler. Password cannot be changed.')

        reset.error = {
          type: 'cancel',
          tag: 'not-implemented',
          text: 'Unfortunately, this server does not support password resets.',
        }

        return reset
      }

      // actually attempt to change the password
      let result = await this.serverHandler.changeUserPassword(jid.toString(), reset.password)
      if (result.status != 'ok') {
        console.error(`Error changing password: ${result.error}`)
        reset.error = {
          type: 'cancel',
          tag: 'server-error',
          text: 'Unfortunately, something went wrong and we could not change your password',
        }
        return reset
      }

      reset.changed = true
      reset.message = 'Your password has been changed'
      console.log(`password changed for ${u.jid.toString()}`)
      return reset
    }


    // gameplay handlers

    async handleTableJoin (request) {
      let events = []

      if (!request.role || request.role == 'none') request.role = 'kibitzer'

      console.log(`${request.user} wants to join ${request.table} as ${request.role}`)

      /**
       * Fetch table (if it exists)
       */

      let table = await Table.getTableById(request.table)

      if(request.role === 'currentPresence'){
        // broadcast presences
        for (let occupant of table.occupants.filter(
          o => { return o.status == 'active' }
        )) {
          // inform new occupant of everyone else
          events.push({
            event: 'presence',
            to: request.user,
            user: occupant.jid,
            role: occupant.seat,
            table: request.table,
          })
        }
        return events
      }

      // if first occupant, set as host
      let isHost = table.occupants.length == 0 ? true : false

      /**
       * Decide role and seat
       *
       * This will decide the role. If 'any' seat is requested,
       * the software will randomly assign a free seat as the
       * "requested seat" for the user.
       * Note that this doesn't save the user to the table: that'll
       * happen later when the other checks are passed. We need a
       * 'requested seat' now because otherwise we'd have to
       * duplicate calcualtions in all the other branches of the
       * code.
       */

      let role = request.role
      let seat = null

      // first, find the user in the occupants list
      let userIndex = table.userIndex(request.user)

      // default role: kibitzers
      if (!role || role == 'none') request.role = 'kibitzer'

      // Not allowed to join in the deleted table
      if (table.stage == 'deleted') {
        console.error(`${request.user} failed to join: Table Deleted`)
        events.push({
          event: 'presence',
          to: request.user, // send to self
          user: request.user,
          role: role == 'kibitzer' ? role : seat,
          table: request.table,
          error: {
            type: 'cancel',
            tag: 'conflict',
            text: 'Trying to join in the deleted table',
          },
        })
        return events
      }
      // if role is a side or 'any', mark as player
      if ([...SIDES.keys(), 'ANY'].indexOf(role.toUpperCase()) >= 0) {

        // if 'ANY' then randomly assign seat
        if (role.toUpperCase() == 'ANY') {
          let free_seats = [...table.seats.entries()].filter((s) => {
            // empty or occupied by self
            return (!s[1].user && table.isAllowedToSit(userIndex, s[0])) || s[1].user == request.user
          })

          if (free_seats.length < 1) {
            // fail: no free seats
            console.error(`${request.user} failed to join: no free seats`)
            events.push({
              event: 'presence',
              to: request.user, // send to self
              user: request.user,
              role: role == 'kibitzer' ? role : seat,
              table: request.table,
              error: {
                type: 'cancel',
                tag: 'conflict',
                text: 'No free seats',
              },
            })
            return events
          }

          seat = free_seats[0][0]
        } else {
          // if not randomly assigned, fix chosen seat
          seat = role.toUpperCase()
          if(!table.isAllowedToSit(userIndex, seat)){
            // fail: not allowed to sit
            console.error(`${request.user} failed to join: not allowed to sit in this side`)
            events.push({
              event: 'presence',
              to: request.user, // send to self
              user: request.user,
              role: seat,
              table: request.table,
              error: {
                type: 'cancel',
                tag: 'conflict',
                text: 'not allowed to sit in this side',
              },
            })
            return events
          }
        }

        role = 'player'
      }

      /**
       * Check if allowed to play
       */
      let existingPlayer = false
      if (userIndex < 0) {
        /* case 1: new occupant */

        if (role == 'player') {
          let s = table.seats.get(seat)

          // fail if already occupied
          if (s.user) {
            if (s.user == request.user) {
              table.occupants[userIndex].status = 'active'
            } else {
              console.error(`${request.user} failed to join ${seat}: already occupied`)
              events.push({
                event: 'presence',
                to: request.user, // send to self
                user: request.user,
                role: role == 'kibitzer' ? role : seat,
                table: request.table,
                error: {
                  type: 'cancel',
                  tag: 'conflict',
                  text: 'Already occupied',
                },
              })
              return events
            }
          }

          // add to seat
          s.user = request.user
          table.seats.set(seat, s)
          table.readyToPlay.push(seat)
        }
        let seenHands = new Map();
        // set up empty seenHands
        for (let side_key of [...SIDES.keys()]) {
          seenHands.set(side_key, {
            seen: false
          })
        }

        // add to occupant list
        table.occupants.push({
          jid: request.user,
          role: role,
          seat: seat,
          seenHands: seenHands,
          status: 'active',
          host: isHost,
        })

        // success message will go out later
      } else if (role == 'kibitzer') {
        /* case 2: player or kibitzer -> kibitzer */
        table.changeSeat(userIndex, null)
        table.occupants[userIndex].status = 'active'

        // success message will come out later
      } else if (role == 'player') {
        /* case 3: player -> player */

        let u = table.occupants[userIndex]

        if (u.seat == seat) {
          // already on seat; do nothing
          console.warn(`${request.user} is already on ${seat}`)

          // set user to active
          if (u.status != 'active') {
            table.occupants[userIndex].status = 'active'
            table.occupants[userIndex].role = 'player'
            table.readyToPlay.push(seat)
            // table.save()
          }

          /*
           * Inform the user, just in case.
           * (It might be a duplicate message because the
           * connection broke or something)
           */

          for (let occupant of table.occupants.filter(
            o => { return o.status == 'active' }
          )) {
              events.push({
                event: 'presence',
                to: request.user,
                user: occupant.jid,
                role: occupant.seat,
                table: request.table,
              })
              // inform everyone of new occupant
              events.push({
                event: 'presence',
                to: occupant.jid,
                user: request.user,
                role: role == 'kibitzer' ? role : seat,
                table: request.table,
              })
          }
          existingPlayer = true
          // return events // end here so we don't spam everyone else
        } else {
          if (table.stage == 'inactive') {
            /* case 3.1: inactive table */

            // fail if chosen seat is taken
            if (!!table.seats.get(seat).user) {
              console.error(`${request.user} failed to join ${seat}: already occupied`)
              events.push({
                event: 'presence',
                to: request.user, // send to self
                user: request.user,
                role: role == 'kibitzer' ? role : seat,
                table: request.table,
                error: {
                  type: 'cancel',
                  tag: 'conflict',
                  text: 'Already occupied',
                },
              })
              return
            }

            // apply changes
            table.changeSeat(userIndex, seat)
            table.occupants[userIndex].status = 'active'
            table.readyToPlay.push(seat)
            // success message will come later
          } else {
            /* case 3.2: active table */

            // disallow players (can't move in mid-game)
            if (u.role == 'player') {
              console.error(`${request.user} can't change seats in mid-game!`)
              events.push({
                event: 'presence',
                to: request.user, // send to self
                user: request.user,
                role: role == 'kibitzer' ? role : seat,
                table: request.table,
                error: {
                  type: 'auth',
                  tag: 'forbidden',
                  text: 'Players cannot change seats mid-game',
                },
              })
              return events
            } else {
              // fail if chosen seat is taken
              if (!!table.seats.get(seat).user) {
                console.error(`${request.user} failed to join ${seat}: already occupied`)
                events.push({
                  event: 'presence',
                  to: request.user, // send to self
                  user: request.user,
                  role: role == 'kibitzer' ? role : seat,
                  table: request.table,
                  error: {
                    type: 'cancel',
                    tag: 'conflict',
                    text: 'Already occupied',
                  },
                })
                return events
              }

              // allow kibitzers in dummy seat
              if (
                table.contract &&
                table.contract.declarer &&
                table.getPartner(table.contract.declarer) == side
              ) {
                // apply changes
                table.changeSeat(userIndex, seat)
                table.occupants[userIndex].status = 'active'
              } else {
                console.error(`${request.user} can't take a seat in mid-game!`)
                events.push({
                  event: 'presence',
                  to: request.user, // send to self
                  user: request.user,
                  role: role == 'kibitzer' ? role : seat,
                  table: request.table,
                  error: {
                    type: 'auth',
                    tag: 'forbidden',
                    text: 'Kibitzers cannot take non-dummy seats mid-game',
                  },
                })
                return events
              }
            }
          }
        }
      }

      let gameStartedAlready = true

      // if there are no empty seats, then start the game!
      let seatsEmpty = [...table.seats.values()].some(s => !s.user)
      if (!seatsEmpty && table.stage == 'inactive') {
        table.stage = 'infosharing'
        gameStartedAlready = false

        // Deal cards only if all seats are empty
        if ([...table.seats.values()].every(s => s.cards.length == 0)) {
          table.dealCards()

          //...but then, also clear infoshares, etc...
          table.seats.forEach((v,k) => {
            v.infoShared = []
            table.set(k,v)
          })

          // add to new seen seat
          table.occupants.forEach((o,k) => {
            if(o.role == 'player'){
              let seenHand = o.seenHands.get(o.seat)
              seenHand.seen = true
              table.occupants[k].seenHands.set(o.seat, seenHand)
            }
            else{
              // Mark all sides as seenHands for Kibitzers
              for (let side_key of [...SIDES.keys()]) {
                let seenHand = table.occupants[k].seenHands.get(side_key)
                seenHand.seen = true
                table.occupants[k].seenHands.set(side_key, seenHand)
              }
            }
          })

          // ...where "etc" includes bids...
          table.bids = []

          // ...and tricks!
          table.tricks = []

          //Set Claim Request as null
          table.claimRequest = null

          //Set Undo Request as empty
          table.undoRequest = {}
        }
      }

      if(!existingPlayer)
      {
        // broadcast presences
        for (let occupant of table.occupants.filter(
          o => { return o.status == 'active' && o.jid != request.user }
        )) {
          // inform new occupant of everyone else
          events.push({
            event: 'presence',
            to: request.user,
            user: occupant.jid,
            role: occupant.seat,
            table: request.table,
          })

          // inform everyone of new occupant
          events.push({
            event: 'presence',
            to: occupant.jid,
            user: request.user,
            role: role == 'kibitzer' ? role : seat,
            table: request.table,
          })
        }

        // finally, inform self of self
        events.push({
          event: 'presence',
          to: request.user,
          user: request.user,
          role: role == 'kibitzer' ? role : seat,
          table: request.table,
        })
      }



      if (!seatsEmpty && !gameStartedAlready) {
        let allHands = []
        // inform players of their own cards
        for (let [side, s] of table.seats.entries()) {
          events.push({
            event: 'state',
            to: s.user,
            table: request.table,
            status: 'active',
            hands: [{
              side: side,
              cards: s.cards.slice().reverse(),
            }]
          })

          allHands.push({
            side: side,
            cards: s.cards.slice().reverse(),
          })
        }

        // also inform all the kibitzers
        for (let occupant of table.occupants.filter(
          o => { return o.status == 'active' && o.role == 'kibitzer' }
        )) {
          events.push({
            event: 'state',
            to: occupant.jid,
            table: request.table,
            status: 'active',
            hands: allHands,
          })
        }
      }

      //Send Catchup Info if table is not inactive
      if(gameStartedAlready && ['scoreboard', 'infosharing', 'bidding','play'].indexOf(table.stage)>=0){
        events = await this.catchupInfo(table, request, role, seat, events)
      }
      // save table changes
      await table.save()

      return events
    }

    async catchupInfo(table, request, role, seat, events){
      let viewableHands = []
        let bids = []
        let contractInfo = null
        let tricksInfo = []
        // inform players of their own cards
        for (let [side, s] of table.seats.entries()) {
          let allowSideCards = false
          if(role == 'kibitzer')
          {
            allowSideCards = true
          }
          else{
            if(side == seat || table.openSides.get(side)){
              allowSideCards = true
            }
          }
          // console.log(s.infoShared,s)
          viewableHands.push({
            side: side,
            cards: allowSideCards ? s.cards.slice().reverse():null,
            infoshared: s.infoShared
          })
        }
        if(['scoreboard', 'bidding','play'].indexOf(table.stage)>=0 && table.bids.length){
          for (var i=0;i<=table.bids.length-1;i++) {
            // calculate winning bid
            let winningSide
            if (table.bids.length > 0) {
              let winningBid = (table.bids[i]
                .getWinningBid(table.dealer));
              winningSide = winningBid ? winningBid.side : null
            }

            for (let b of table.bids[i].bids) {
              bids.push({
                user: table.seats.get(b.side).user,
                side: b.side,
                type: b.bidType,
                level: b.level,
                suit: b.suit,
                won: b.side == winningSide,
                round: i+1
              })
            }
          }
        }
        if(['scoreboard','play'].indexOf(table.stage)>=0){
          // calculate winning bid
          contractInfo = table.contract
          tricksInfo = table.tricks
        }
        let trickScore = table.trickScore();
        let scoreInfo = {
          trick: [
            { side: 'NS', value: trickScore['NS'] },
            { side: 'EW', value: trickScore['EW'] },
          ],
        }
        let currentHost = table.occupants.filter(
          o => { return (o.host == true)}
        )

        let hostValue = (currentHost[0].role == "player")?currentHost[0].seat:currentHost[0].role

        events.push({
          event: 'state',
          to: request.user,
          user: request.user,
          role: role == 'kibitzer' ? role : seat,
          table: request.table,
          tableinfo: {stage:table.stage, turn:table.turn, dealer:table.dealer, host:hostValue},
          status: 'active',
          hands: viewableHands,
          bids: bids,
          contract: contractInfo,
          tricksinfo: tricksInfo,
          score: scoreInfo
        })
      return events
    }

    async handleTableLeave (request) {
      let events = []

      let table = await Table.findOne({ table_id: request.table })
        if (!table) return events// TODO: give some error or something

        console.log(`${request.user} wants to leave ${request.table}`)

        // mark occupant as inactive
        let userIndex = table.userIndex(request.user)
        table.occupants[userIndex].status = 'unavailable'

        if(table.occupants[userIndex].role=='player')
          table.readyToPlay.pop(table.occupants[userIndex].seat)
        // tell everyone of update
        for (let occupant of table.occupants.filter(
          o => { return o.status == 'active' && o.jid != request.user }
        )) {
          // inform everyone of occupant leaving
          events.push({
            event: 'presence',
            to: occupant.jid,
            user: request.user,
            table: request.table,
            type: 'unavailable',
          })
        }

        // confirm to self
        events.push({
          event: 'presence',
          to: request.user,
          user: request.user,
          table: request.table,
          type: 'unavailable',
        })

        // save and return
        await table.save()

        return events
    }

    async handleTableExit (request) {
      let events = []

      let table = await Table.findOne({ table_id: request.table })
        if (!table) return events// TODO: give some error or something

        console.log(`${request.user} wants to Exit ${request.table}`)

        //remove user
        let newHost = table.clearUser(request.user)
        // tell everyone of update
        let currentHost = table.occupants.filter(
          o => { return (o.host == true)}
        )
        let hostValue = (currentHost[0].role == "player")?currentHost[0].seat:currentHost[0].role

        for (let occupant of table.occupants.filter(
          o => { return o.status == 'active' && o.jid != request.user }
        )) {
          // inform everyone of occupant leaving
          events.push({
            event: 'presence',
            to: occupant.jid,
            user: request.user,
            table: request.table,
            type: 'unavailable',
          })

          events.push({
            event: 'state',
            to: occupant.jid,
            user: request.user,
            table: request.table,
            tableinfo: {stage:table.stage, turn:table.turn, dealer:table.dealer, host:hostValue},
          })
        }

        // confirm to self
        events.push({
          event: 'presence',
          to: request.user,
          user: request.user,
          table: request.table,
          type: 'unavailable',
        })

        // save and return

        if(newHost===null)
        {
          table.stage = 'deleted'
          // const del = await Table.deleteOne({ table_id: request.table })
        }
        await table.save()
        return events
    }

    async handleInfoshare (infoshare) {
      let events = []

      let table = await Table.findOne({ table_id: infoshare.table })
      if (!table) return // TODO: give some error or something

      console.log(`${infoshare.user} wants to share ${infoshare.type}: ${infoshare.value}`)
      // table, user, side, type, value

      let userIndex = table.userIndex(infoshare.user)

      // check if active player
      if (
        userIndex == -1 ||
        table.occupants[userIndex].status != 'active' ||
        table.occupants[userIndex].role != 'player'
        ) {
        console.error(`${infoshare.user} is not an active player`)
        infoshare.error = {type: 'auth', text: `${infoshare.user} is not an active player`}
          infoshare.to = infoshare.user
          infoshare.event = 'infoshare'
          events.push(infoshare)
        return events
      }

      // check if game is in infosharing stage
      if (table.stage != 'infosharing') {
        console.error(`${infoshare.table} is not in infosharing stage right now`)
        infoshare.error = {text: `${infoshare.table} is not in infosharing stage right now`}
          infoshare.to = infoshare.user
          infoshare.event = 'infoshare'
          events.push(infoshare)
        return events
      }

      // get side if not specified; validate otherwise
      if (!infoshare.side) {
        infoshare.side = table.occupants[userIndex].seat
      } else if (infoshare.side != table.occupants[userIndex].seat) {
        // don't allow random sides
        console.error(`${infoshare.user} is not allowed to play for ${infoshare.side}`)
        infoshare.error = {type: 'auth', text: `${infoshare.user} is not allowed to play for ${infoshare.side}`}
          infoshare.to = infoshare.user
          infoshare.event = 'infoshare'
          events.push(infoshare)
        return events
      }

      // check that it's their turn
      if (table.turn != infoshare.side) {
        console.error(`It's not ${infoshare.side}'s turn right now!`)
        infoshare.error = {text: `It is not ${infoshare.side}'s turn right now!`}
          infoshare.to = infoshare.user
          infoshare.event = 'infoshare'
          events.push(infoshare)
        return events
      }

      // check format
      if ([
        'C',
        'D',
        'H',
        'S',
        'HCP',
        'pattern'
        ].indexOf(infoshare.type) == -1
      ) {
        console.error(`Invalid info type: ${infoshare.type}`)
        infoshare.error = {text: `Invalid info type`}
        infoshare.to = infoshare.user
        infoshare.event = 'infoshare'
        events.push(infoshare)
        return events
      }

      // check for duplicate share
      if (table.seats.get(infoshare.side).infoShared
        .some(i => i.infoType == infoshare.type)) {
          console.error(`${infoshare.user} has already shared ${infoshare.type}`)
          infoshare.error = {text: `${infoshare.type} has already been shared by ${infoshare.side}`}
          infoshare.to = infoshare.user
          infoshare.event = 'infoshare'
          events.push(infoshare)
          return events
      }

      // check info values
      let pattern

      if (infoshare.type == 'pattern') {
        pattern = table.getHandInfo('pattern', infoshare.side)

        // validate or set infoshare
        if (!infoshare.value) {
          infoshare.value = pattern.join(',')
        } else if (infoshare.value.split(',').join(',') != patternString.join(',')) {
          console.error(`Incorrect pattern for ${infoshare.side}: ${infoshare.value}: should be ${patternString}`)
          infoshare.error = {text: `invalid pattern`}
          infoshare.to = infoshare.user
          infoshare.event = 'infoshare'
          events.push(infoshare)
          return events
        }
      }

      else {

        let value = table.getHandInfo(infoshare.type, infoshare.side)

        // validate or set count
        if (!infoshare.value) {
          infoshare.value = value
        } else if (infoshare.value != value) {
          console.error(`Incorrect ${infoshare.type} info for ${infoshare.side}: ${infoshare.value}: should be ${value}`)
          infoshare.error = {text: `invalid ${infoshare.type} info`}
          infoshare.to = infoshare.user
          infoshare.event = 'infoshare'
          events.push(infoshare)
          return events
        }
      }

      // all well; save info
      let info = {
        infoType: infoshare.type,
        infoValue: pattern || [infoshare.value],
      }

      let s = table.seats.get(infoshare.side)
      s.infoShared.push(info)
      table.seats.set(infoshare.side, s)

      // next turn!
      table.incrementTurn()

      /* When 2 infos each shared, move to next stage
       * We're using >= so that even if there's some glitch and
       * more infos get sent out we can still carry on instead
       * of being stuck in an endless loop.
      */

      if ([...table.seats.values()]
        .every(s => s.infoShared.length >= 2)
      ) {
        table.stage = 'bidding'
      }

      // tell everyone about it
      for (let occupant of table.occupants.filter(
        o => { return o.status == 'active' }
      )) {
        events.push({
          event: 'infoshare',
          to: occupant.jid,
          table: infoshare.table,
          user: infoshare.user,
          side: infoshare.side,
          type: infoshare.type,
          value: infoshare.value,
        })
      }

      // save it!
      await table.save()

      return events
    }


    async handleBid(bid) {
      let events = []

      let table = await Table.findOne({ table_id: bid.table })
      if (!table) return // TODO: give some error or something

      console.log(`${bid.user} wants to make ${bid.type} bid: ${bid.level}${bid.suit} round:${bid.round}`)
      // table, user, side, type, value

      let userIndex = table.userIndex(bid.user)

      // check if active player
      if (
        userIndex == -1 ||
        table.occupants[userIndex].status != 'active' ||
        table.occupants[userIndex].role != 'player'
        ) {
        console.error(`${bid.user} is not an active player`)
        bid.error = {type: 'auth', text: `${bid.user} is not an active player`}
          bid.to = bid.user
          bid.event = 'bid'
          events.push(bid)
        return events
      }

      // check if game is in bidding stage
      if (table.stage != 'bidding') {
        console.error(`${bid.table} is not in bidding stage right now`)
        bid.error = {text: `${bid.table} is not in bidding stage right now`}
          bid.to = bid.user
          bid.event = 'bid'
          events.push(bid)
        return events
      }

      // get side if not specified; validate otherwise
      if (!bid.side) {
        bid.side = table.occupants[userIndex].seat
      } else if (bid.side != table.occupants[userIndex].seat) {
        // don't allow random sides
        console.error(`${bid.user} is not allowed to play for ${bid.side}`)
        bid.error = {type: 'auth', text: `${bid.user} is not allowed to play for ${bid.side}`}
          bid.to = bid.user
          bid.event = 'bid'
          events.push(bid)
        return events
      }

      // check format
      if ([
        'level',
        'redouble',
        'double',
        'pass'
        ].indexOf(bid.type) == -1
      ) {
        console.error(`Invalid bid: ${bid.type}`)
        bid.error = {text: `Invalid bid`}
        bid.to = bid.user
        bid.event = 'bid'
        events.push(bid)
        return events
      }

      // check format again for level bids
      if (bid.type == 'level'){

        // level must be between 1 and 7
        if (bid.level > 7 || bid.level < 1) {
          console.error(`Invalid bid level: ${bid.level}`)
          bid.error = {text: `Invalid bid level`}
          bid.to = bid.user
          bid.event = 'bid'
          events.push(bid)
          return events
        }

        // suit must be a valid suit or NT
        if (![...SUITS.keys(), 'NT'].includes(bid.suit)) {
          console.error(`Invalid suit: ${bid.suit}`)
          bid.error = {text: `Invalid bid suit`}
          bid.to = bid.user
          bid.event = 'bid'
          events.push(bid)
          return events
        }
      } else {
        // otherwise, clear extraneous fields
        bid.level = undefined
        bid.suit = undefined
      }

      // start round if not started
      if (table.bids.length == 0) {
        table.bids.push({ bids: [], winner: null }) // empty list containing bids
      }

      // Fetch the last (most recent) bid
      let currentRound = table.bids[table.bids.length-1]

      // Check for duplicate bid
      let dupeBid = currentRound.bids.find(b => b.side == bid.side)
      if (!!dupeBid) {
        /* if it's the same bid, pretend to accept
         * (probably a connection error) */
        if (
          dupeBid.type == bid.type &&
          dupeBid.level == bid.level &&
          dupeBid.suit == bid.suit
        ) {
          // send a dummy message to keep the client happy
          bid.to = bid.user
          bid.event = 'bid'
          events.push(bid)
          return events
        }

        // if not, then there's something shady going on!
        else {
          // reject them and get out
          bid.to = bid.user
          bid.error = {text: 'Already bid once in this round.'}
          console.error(bid.error.text)
          bid.event = 'bid'
          events.push(bid)
          return events
        }
      }

      /*
       * helper function to broadcast bids
       *
       * (so we don't have to repeat the code again
       * and again and again)
       */

      function broadcastBids() {
        // calculate winning bid
        let winningSide
        if (table.bids.length > 0) {
          let winningBid = (table.bids[table.bids.length-1]
            .getWinningBid(table.dealer));
          winningSide = winningBid ? winningBid.side : null;
        }
        console.log(`the side is: ${winningSide}`)

        let bids = []

        for (let b of table.bids[table.bids.length-1].bids) {
          bids.push({
            side: b.side,
            type: b.bidType,
            level: b.level,
            suit: b.suit,
            won: b.side == winningSide,
            round: table.bids.length,
          })
        }

        for (let o of table.occupants.filter(
          o => o.status == 'active')
        ) {
          console.log(`Broadcasting to ${o.jid}: round ${table.bids.length} of bidding`)
          events.push({
            event: 'state',
            table: table.table_id,
            to: o.jid,
            bids: bids,
            contract: table.contract.level ? table.contract : undefined,
          })
        }
      }

      /*
       * another function, this time to announce the fact of
       * bidding - that is, indicate that a bid has been
       * made by a side, but without revealing what the
       * actual value is.
       *
       * partial kibitzers and the player from the side who
       * made the bid, of course, get to see the full value.
       * and so do all full kibitzers - don't forget!
       */

      function partiallyAnnounceBid(bid, round) {
        for (let o of table.occupants.filter(
          o => o.status == 'active'
        )) {
          let fullView = (o.seat == bid.side ||
            (o.seat == null && o.role == 'kibitzer'))
          if (fullView) {
            console.log(`Broadcasting to ${o.jid}: ${bid.side}'s bid of ${bid.bidType} ${bid.level}${bid.suit}`)
          } else {
            console.log(`Broadcasting to ${o.jid}: ${bid.side} has made a bid`)
          }
          events.push({
            event: 'bid',
            table: table.table_id,
            to: o.jid,
            user: table.seats.get(bid.side).user,
            side: bid.side,
            type: fullView ? bid.bidType : null,
            level: fullView ? bid.level : null,
            suit: fullView ? bid.suit: null,
            round: round
          })
        }
      }

      /*
       * last but not the least, here's one to end the
       * bidding, calculate the final contract, and move on
       * to the next stage of the game.
       */

      function endBidding() {
        table.contract = table.getContract()

        // end of game if all pass
        if (!table.contract || !table.contract.level) {
          table.stage = 'scoreboard'
          table.readyToPlay = []
          table.incrementTurn()
        } else {
          // turn goes to person after declarer
          let sides = [...SIDES.keys()]

          table.turn = (sides[
              sides.indexOf(table.contract.declarer) // declarer
              + 1 // the seat after
               % sides.length // mod 4 because the table goes round
            ])

          table.stage = 'play'
        }
      }

      /*
       * the main logic begins!
       */


      /*
       *                            _
       *  _ __ ___  _   _ _ __   __| |   ___  _ __   ___
       * | '__/ _ \| | | | '_ \ / _` |  / _ \| '_ \ / _ \
       * | | | (_) | |_| | | | | (_| | | (_) | | | |  __/
       * |_|  \___/ \__,_|_| |_|\__,_|  \___/|_| |_|\___| r1
       *
       *
       */


      if (table.bids.length == 1) {

        // don't allow doubles and redoubles
        if (['level', 'pass'].indexOf(bid.type) < 0) {
          bid.to = bid.user
          bid.error = {text: 'illegal bid'}
          console.error(bid.error.text)
          this.emit('bid', bid)
          return
        }

        // save the bid
        table.bids[table.bids.length-1].bids.push({
          side: bid.side,
          bidType: bid.type,
          level: bid.level,
          suit: bid.suit,
        })

        // announce it
        bid.bidType = bid.type
        partiallyAnnounceBid.call(this, bid, "1")

        // handle round completion
        if (table.bids[table.bids.length-1].bids.length == 4) {

          // end game if all pass!
          if (table.bids[table.bids.length-1].bids.every(b => (
            b.bidType == 'pass'
          ))) {
            endBidding.call(this)

            // note down the old hands, for broadcasting later
            let oldHands = [...SIDES.keys()].map(side => {
                  return {
                    side: side,
                    cards: (table
                      .seats
                      .get(side)
                      .cards
                      .slice()
                      .reverse())
                  }
                })
            // note down this round's bids also, again for broadcasting
            let bids = []
            let round = table.bids[table.bids.length-1]

            for (let bid of round.bids) {
              let winningBid = round.getWinningBid(table.dealer)
              let winningSide = winningBid ? winningBid.side : null

              console.log('processing:', bid)

              bids.push({
                side: bid.side,
                type: bid.bidType,
                level: bid.level,
                suit: bid.suit,
                won: bid.side == winningSide,
                round: table.bids.length,
              })
            }

            // clear cards (so next game can start)
            table.seats.forEach((v,k) => {
              v.infoShared = []
              v.cards = []
              table.set(k,v)
            })

            table.bids = []
            table.tricks = []
            table.claimRequest = null
            table.undoRequest = {}
            // reset stage
            table.stage = 'scoreboard' // FIXME: should be scoreboard

            // broadcast score to everyone
            for (let occupant of table.occupants.filter(
              o => { return o.status == 'active' }
            )) {
              events.push({
                event: 'state',
                to: occupant.jid,
                table: table.table_id,
                status: 'active', // TODO: include stage
                bids: bids,
                score: {
                  deal: [
                    { side: 'NS', value: 0 },
                    { side: 'EW', value: 0 },
                  ],
                  running: [
                    { side: 'NS', value: table.runningScore.get('NS') },
                    { side: 'EW', value: table.runningScore.get('EW') },
                  ],
                },
                hands: oldHands,
              })
            }
          } else {
            // next round otherwise
            broadcastBids.call(this)

            table.bids.push({ bids: [], winner: null})
          }
        }
      }

      /*
       *                            _   _
       *  _ __ ___  _   _ _ __   __| | | |___      _____
       * | '__/ _ \| | | | '_ \ / _` | | __\ \ /\ / / _ \
       * | | | (_) | |_| | | | | (_| | | |_ \ V  V / (_) |
       * |_|  \___/ \__,_|_| |_|\__,_|  \__| \_/\_/ \___/  r2
       *
       *
       */

      else if (table.bids.length == 2) {
        // check that side is opponent of round 1 declarer
        let prevRound = table.bids[table.bids.length-2]
        let prevDeclarer = prevRound.getDeclarer(table.dealer)
        if (!table.getOpponents(prevDeclarer).includes(bid.side)) {
          bid.to = bid.user
          bid.error = {text: `It is not ${bid.side}'s turn right now!`}
          console.error(bid.error.text)
          bid.event = 'bid'
          events.push(bid)
          return events
        }

        // check that bid is double, pass, or level
        if (!['double', 'pass', 'level'].includes(bid.type)) {
          bid.to = bid.user
          bid.error = {text: 'Illegal bid'}
          console.error(bid.error.text)
          bid.event = 'bid'
          events.push(bid)
          return events
        }

        // check that level bids are higher than previous one
        if (bid.type == 'level') {
          let prevWinningBid = prevRound.getWinningBid(table.dealer)
          if (
            bid.level < prevWinningBid.level ||
            (
              bid.level == prevWinningBid.level &&
              bid.suit <= prevWinningBid.suit
            )
          ) {
            bid.to = bid.user
            bid.error = {text: 'Bid too small'}
            console.error(bid.error.text)
            bid.event = 'bid'
            events.push(bid)
            return events
          }
        }

        // save the bid
        table.bids[table.bids.length-1].bids.push({
          side: bid.side,
          bidType: bid.type,
          level: bid.level,
          suit: bid.suit
        })

        // announce the bid
        bid.bidType = bid.type
        partiallyAnnounceBid.call(this, bid, "2")

        // handle end-of-round
        if (table.bids[table.bids.length-1].bids.length >= 2) {

          // bidding ends on pass...
          if ((table.bids[table.bids.length-1]
            .getWinningBid(table.dealer)
            .bidType) == 'pass') {
            endBidding.call(this) // bidding over; move to next stage!
            broadcastBids.call(this)
          } else {
            // ...or moves on to the next round
            broadcastBids.call(this)
            table.bids.push({ bids: [], winner: null})
          }
        }
      }

      /*
       *                            _   _   _
       *  _ __ ___  _   _ _ __   __| | | |_| |__  _ __ ___  ___
       * | '__/ _ \| | | | '_ \ / _` | | __| '_ \| '__/ _ \/ _ \
       * | | | (_) | |_| | | | | (_| | | |_| | | | | |  __/  __/
       * |_|  \___/ \__,_|_| |_|\__,_|  \__|_| |_|_|  \___|\___| r3
       *
       *
       */

      else if (table.bids.length == 3) {
        let prevBid = table.bids[table.bids.length-2].getWinningBid(table.dealer)
        if (prevBid.bidType == 'double') {
          // round 2 was a double


          // only the declarer can play now
          if (bid.side != table.bids[0].getDeclarer(table.dealer)) {
            bid.to = bid.user
            bid.error = {text: `It is not ${bid.side}'s turn right now!`}
            console.error(bid.error.text)
            bid.event = 'bid'
            events.push(bid)
            return events
          }

          // declarer can only redouble or pass
          if (!['redouble', 'pass'].includes(bid.type)) {
            bid.to = bid.user
            bid.error = {text: 'Illegal bid'}
            console.error(bid.error.text)
            bid.event = 'bid'
            events.push(bid)
            return events
          }

          // save the bid
          table.bids[table.bids.length-1].bids.push({
            side: bid.side,
            bidType: bid.type,
            level: bid.level,
            suit: bid.suit
          })

          // announce the bid
          bid.bidType = bid.type
          partiallyAnnounceBid.call(this, bid, "3")

          // calculate contract and end stage
          endBidding.call(this)
          broadcastBids.call(this)
        } else {
          // round 2 was a raise

          // only opponents of previous declarer can bid
          if (!table.getOpponents(prevBid.side)
            .includes(bid.side)) {
              bid.to = bid.user
              bid.error = {text: `It is not ${bid.side}'s turn right now!`}
              console.error(bid.error.text)
              bid.event = 'bid'
              events.push(bid)
              return events
          }

          // bid must be double, pass, or level
          if (!['double', 'pass', 'level'].includes(bid.type)) {
            bid.to = bid.user
            bid.error = {text: 'Illegal bid'}
            console.error(bid.error.text)
            bid.event = 'bid'
            events.push(bid)
            return events
          }

          // if it's a level, make sure it's a raise
          if (bid.type == 'level') {
            let suits = [...SUITS.keys(), 'NT']

            // reject if new bid <= old bid
            console.log('prev bid', prevBid, 'new bid', bid, 'old bid', prevBid.suit, 'new bid', bid.suit, 'indexes', bid.suit, ' <= ', prevBid.suit)
            if (
              bid.level < prevBid.level ||
              (
                bid.level == prevBid.level &&
                suits.indexOf(bid.suit) <= suits.indexOf(prevBid.suit)
              )
            ) {
              bid.to = bid.user
              bid.error = {text: 'Bid too small'}
              console.error(bid.error.text)
              bid.event = 'bid'
              events.push(bid)
              return events
            }
          }

          // save the bid
          table.bids[table.bids.length-1].bids.push({
            side: bid.side,
            bidType: bid.type,
            level: bid.level,
            suit: bid.suit
          })

          // announce the bid
          bid.bidType = bid.type
          partiallyAnnounceBid.call(this, bid, "3")

          if (table.bids[table.bids.length-1].bids.length >= 2) {

            // bidding ends on pass...
            if ((table.bids[table.bids.length-1]
              .getWinningBid(table.dealer)
              .bidType) == 'pass'
            ) {
              endBidding.call(this)
              broadcastBids.call(this)
            } else {
              // ...or moves on to the next round
              broadcastBids.call(this)
              table.bids.push({ bids: [], winner: null})
            }
          }

        }
      }

      /*
       *                            _    __
       *  _ __ ___  _   _ _ __   __| |  / _| ___  _   _ _ __
       * | '__/ _ \| | | | '_ \ / _` | | |_ / _ \| | | | '__|
       * | | | (_) | |_| | | | | (_| | |  _| (_) | |_| | |
       * |_|  \___/ \__,_|_| |_|\__,_| |_|  \___/ \__,_|_| r4
       *
       */

      else if (table.bids.length == 4) {
        let prevRound = table.bids[table.bids.length-2]

        if (prevRound.getWinningBid(table.dealer).bidType == 'double') {
          // case 4.1: round 3 was double

          // only round 2 declarer can play
          if (bid.side != table.bids[1].getDeclarer(table.dealer)) {
            bid.to = bid.user
            bid.error = {text: `It is not ${bid.side}'s turn right now!`}
            console.error(bid.error.text)
            bid.event = 'bid'
            events.push(bid)
            return events
          }

          // move has to be redouble or pass
          if (!['redouble', 'pass'].includes(bid.type)) {
            bid.to = bid.user
            bid.error = {text: 'Illegal bid'}
            console.error(bid.error.text)
            bid.event = 'bid'
            events.push(bid)
            return events
          }

          // save the bid
          table.bids[table.bids.length-1].bids.push({
            side: bid.side,
            bidType: bid.type,
            level: bid.level,
            suit: bid.suit
          })

          // announce the bid
          bid.bidType = bid.type
          partiallyAnnounceBid.call(this, bid, "4")

          // move to next stage: only 1 bid so no need to wait
          endBidding.call(this)

          // tell everyone about it
          broadcastBids.call(this)

        } else {
          // case 4.2: round 3 bid was raise

          // only opponents of previous declarer can bid
          if (!table.getOpponents(prevRound.getDeclarer(table.dealer))
            .includes(bid.side)) {
              bid.to = bid.user
              bid.error = {text: `It is not ${bid.side}'s turn right now!`}
              console.error(bid.error.text)
              this.emit('bid', bid)
              return
          }

          // move can only be double or pass
          if (!['double', 'pass'].includes(bid.type)) {
            bid.to = bid.user
            bid.error = {text: 'Illegal bid'}
            console.error(bid.error.text)
            bid.event = 'bid'
            events.push(bid)
            return events
          }

          // save the bid
          table.bids[table.bids.length-1].bids.push({
            side: bid.side,
            bidType: bid.type,
            level: bid.level,
            suit: bid.suit,
          })

          // announce the bid
          bid.bidType = bid.type
          partiallyAnnounceBid.call(this, bid, "4")

          // handle end of round
          if (table.bids[table.bids.length-1].bids.length >= 2) {

              // bidding ends on pass...
              if ((table.bids[table.bids.length-1]
                .getWinningBid(table.dealer)
                .bidType) == 'pass') {
                endBidding.call(this) // calculate contract
                broadcastBids.call(this) // tell everyone and exit
              } else {
                // ...or moves on to the next round
                broadcastBids.call(this)
                table.bids.push({ bids: [], winner: null})
              }
          }


        }
      }

      /*
       *                            _    __ _
       *  _ __ ___  _   _ _ __   __| |  / _(_)_   _____
       * | '__/ _ \| | | | '_ \ / _` | | |_| \ \ / / _ \
       * | | | (_) | |_| | | | | (_| | |  _| |\ V /  __/
       * |_|  \___/ \__,_|_| |_|\__,_| |_| |_| \_/ \___| r5
       *
       */

      else if (table.bids.length == 5) {
        // only round 2 declarer can bid
        if (bid.side != table.bids[2].getDeclarer(table.dealer)) {
          bid.to = bid.user
          bid.error = {text: `It is not ${bid.side}'s turn right now!`}
          console.error(bid.error.text)
          bid.event = 'bid'
          events.push(bid)
          return events
        }

        // bid must be a redouble or pass
        if (!['redouble', 'pass'].includes(bid.type)) {
          bid.to = bid.user
          bid.error = {text: 'Illegal bid'}
          console.error(bid.error.text)
          bid.event = 'bid'
          events.push(bid)
          return events
        }

        // save the bid
        table.bids[table.bids.length-1].bids.push({
          side: bid.side,
          bidType: bid.type,
          level: bid.level,
          suit: bid.suit
        })

        // announce the bid
        bid.bidType = bid.type
        partiallyAnnounceBid.call(this, bid, "5")

        // only one bid, so move to next stage immediately
        endBidding.call(this)
        broadcastBids.call(this)
      }

      // save the table, don't forget!
      await table.save()

      return events
    }


    async handleCardplay(cardplay) {
      let events = []

      let table = await Table.findOne({ table_id: cardplay.table })
      if (!table) return // TODO: give some error or something

      console.log(`${cardplay.user} wants to play ${cardplay.rank}${cardplay.suit}`)
      // table, user, side, type, value
      let userIndex = table.userIndex(cardplay.user)
      // check if active player
      if (
        userIndex == -1 ||
        table.occupants[userIndex].status != 'active' ||
        table.occupants[userIndex].role != 'player'
        ) {
        console.error(`${cardplay.user} is not an active player`)
        cardplay.error = {type: 'auth', text: `${cardplay.user} only active players can play`}
        cardplay.to = cardplay.user
        cardplay.event = 'cardplay'
        events.push(cardplay)
        return events
      }

      // player canaot play stage
      if (table.stage != 'play') {
        console.error(`${cardplay.table} you cannot play cards at this stage`)
        cardplay.error = {text: `${cardplay.table} you cannot play cards at this stage`}
        cardplay.to = cardplay.user
        cardplay.event = 'cardplay'
        events.push(cardplay)
        return events
      }

      // player cannot play because claim is in progress
      if(table.hasClaimRequest()) {
        console.error(`${cardplay.table} you cannot play cards because claim is in progress`)
        cardplay.error = {text: `${cardplay.table} you cannot play cards because claim is in progress`}
        cardplay.to = cardplay.user
        cardplay.event = 'cardplay'
        events.push(cardplay)
        return events
      }

      //dummy is not allowed
      if (
        table.contract &&
        table.contract.declarer &&
        table.getPartner(table.contract.declarer) == cardplay.side &&
        table.occupants[userIndex].seat == cardplay.side
      ) {
        console.error(`${cardplay.table} Dummy is not allow to play`)
        cardplay.error = {text: `${cardplay.table} Dummy is not allowed to play`}
        cardplay.to = cardplay.user
        cardplay.event = 'cardplay'
        events.push(cardplay)
        return events
      }

      // check user turn
      let side
      if (table.turn == table.occupants[userIndex].seat) {
        side = table.turn
      } else {
        // check if you're playing for the dummy
        if (
          table.occupants[userIndex].seat == table.contract.declarer &&
          table.getPartner(table.contract.declarer) == table.turn
        ) {
          side = table.getPartner(table.occupants[userIndex].seat)
        } else {
          console.error(`It's not your turn`)
          cardplay.error = {text: `It's not your turn!`}
          cardplay.to = cardplay.user
          cardplay.event = 'cardplay'
          events.push(cardplay)
          return events
        }
      }

      // check if requested side matches user's side
      if (!side || (cardplay.side && cardplay.side != side)) {
        console.error(`${cardplay.user} cannot play for ${cardplay.side} at this point.`)
        cardplay.error = {text: `${cardplay.user} cannot play for ${cardplay.side} at this point.`}
        cardplay.to = cardplay.user
        cardplay.event = 'cardplay'
        events.push(cardplay)
        return events
      }

    // set up the trick if it doesn't exist
      if (table.tricks.length < 1) {
      table.tricks.push({cards: []})
    }

    //check card in your hand
    let cardIndex = table.seats.get(cardplay.side).cards.findIndex(c =>
      c.rank == cardplay.rank && c.suit == cardplay.suit)

     if (cardIndex < 0) {
          console.error(`${cardplay.user} you do not have that card to play`)
          cardplay.error = {text: `Card not in hand`}
          cardplay.to = cardplay.user
          cardplay.event = 'cardplay'
          events.push(cardplay)
          return events
     }

        //following suit
        if (table.tricks[table.tricks.length - 1].cards.length > 0) {

          if(cardplay.suit!=table.tricks[table.tricks.length - 1].cards[0].card.suit){
            console.log('diff suit')
            if(table.seats.get(side).cards.some(card => card.suit == table.tricks[table.tricks.length - 1].cards[0].card.suit)){

              console.error(`${cardplay.user}must play suit`,table.tricks[table.tricks.length - 1].cards[0].card.suit )
              cardplay.error = {text: `Card does not follow suit`}
              cardplay.to = cardplay.user
              cardplay.event = 'cardplay'
              events.push(cardplay)
              return events
            }

          }
        }

      //actually playcard
      let s = table.seats.get(cardplay.side)
      s.cards.splice(cardIndex, 1)
      table.seats.set(cardplay.side, s)

      table.tricks[table.tricks.length - 1].cards.push({
          side: cardplay.side,
          card: new Card(`${cardplay.suit}${cardplay.rank}`),
        })

        table.incrementTurn()

        // tell everyone about it
        for (let occupant of table.occupants.filter(
          o => { return o.status == 'active' }
          )) {
          events.push({
            event: 'cardplay',
            to: occupant.jid,
            table: cardplay.table,
            user: cardplay.user,
            side: cardplay.side,
            rank: cardplay.rank,
            suit: cardplay.suit,
          })
        }

        //check round is over
        if (table.tricks[table.tricks.length-1].cards.length >= 4) {

          //calculate winning card
          let highestCard

          if(table.contract.suit!='NT'){


                let  trumpCards=table.tricks[table.tricks.length-1].cards.filter( (x) => { return x.card.suit == table.contract.suit })
            if(trumpCards.length>0){
              let highestRank = Math.max.apply(Math, trumpCards.map(c => [...RANKS.keys()].indexOf(c.card.rank)))
              highestCard = trumpCards.filter(
                t => t.card.rank == [...RANKS.keys()][highestRank]
              )[0].card


          }
        }
     // if not found yet, try with normal cards
     if(!highestCard){

       let suitCards =table.tricks[table.tricks.length - 1].cards.filter(x => x.card.suit == table.tricks[table.tricks.length-1].cards[0].card.suit)

       let highestRank = Math.max.apply(Math, suitCards.map(c => [...RANKS.keys()].indexOf(c.card.rank)))
       highestCard = suitCards.filter(c => c.card.rank == [...RANKS.keys()][highestRank])[0].card
    }

       table.tricks[table.tricks.length-1].winner = (
         table.tricks[table.tricks.length-1].cards
         .findIndex(h => h.card.rank == highestCard.rank && h.card.suit == highestCard.suit)
        )

       let lastTrick = table.tricks[table.tricks.length-1]
       console.info(`${lastTrick.cards[lastTrick.winner].side} wins the tricks ${cardplay.table} info `)

       // calculate and set trick score
       let trickScore = table.trickScore()

       //set up next trick
       table.turn = lastTrick.cards[lastTrick.winner].side
       table.tricks.push({cards: []})

       // tell everyone about it
        for (let occupant of table.occupants.filter(
          o => { return o.status == 'active' }
        )) {
          events.push({
            event: 'state',
            to: occupant.jid,
            table: cardplay.table,
            status: 'active',
            score: {
              trick: [
                { side: 'NS', value: trickScore['NS'] },
                { side: 'EW', value: trickScore['EW'] },
              ],
            },
            turn: table.tricks.length < 14 ? table.turn : null
          })
        }
     }

     // reveal dummy when 1st card is played
     if( table.tricks.length == 1 && table.tricks[0].cards.length == 1){
      let dummy=table.getPartner(table.contract.declarer)

      // get the player details
      let dummyPlayer = table.seats.get(dummy)

      for (let occupant of table.occupants.filter( o => o.status == 'active')) {
        events.push({
          event: 'state',
          to: occupant.jid,
          table: cardplay.table,
          status: 'active',
          hands: [{
            side: dummy,
            cards: dummyPlayer.cards.slice().reverse(),
          }],
        })
      }

      if(table.openSides && !table.openSides.get(dummy))
      {
        // Get player cards and expose it to everyone
          table.openSides.set(dummy,{
            side: dummy,
            cards: dummyPlayer.cards.slice().reverse(),
          })
      }

      // also show declarer players to dummy
      events.push({
        event: 'state',
        to: dummyPlayer.user,
        table: cardplay.table,
        status: 'active',
        hands: [{
          side: table.contract.declarer,
          cards: table.seats.get(table.contract.declarer).cards.slice().reverse()
        }],
      })
     }


     //handle end game

     if(table.tricks.length==14 && table.tricks[table.tricks.length-2].cards.length==4){
       // surprised about the '14'? don't forget we created an extra trick at
       // the end earlier on!

         // we're naming this differently because the previously computed
         // trickScore is not showing for some time, but we wanted to name
         // it differently anyway in case the 'let's clash sometime. No
         // idea why, shoganai ¯\_(ツ)_/¯
         let finalTrickScore = table.trickScore()
          let dealScore =table.hoolScore(table.contract,finalTrickScore['NS'],finalTrickScore['EW'])

      //update running score
      table.runningScore.set('NS', table.runningScore.get('NS') + dealScore['NS'])
      table.runningScore.set('EW', table.runningScore.get('EW') + dealScore['EW'])

      console.log("Game Over")
      console.log("runningScore",table.runningScore)
      console.log("DealScore",table.dealScore)
      let allHands = []
      // inform all the players of all cards
      for (let [side, s] of table.gameHistory[table.gameHistory.length-1].seats.entries()) {
        allHands.push({
          side: side,
          cards: s.cards.slice().reverse(),
        })
      }
      // tell everyone about it
      for (let occupant of table.occupants.filter(
        o => { return o.status == 'active' }
      )) {
        events.push({
          event: 'state',
          to: occupant.jid,
          table: cardplay.table,
          status: 'active',
          score: {
            deal: [
              { side: 'NS', value: dealScore['NS'] },
              { side: 'EW', value: dealScore['EW'] },
            ],
            running: [
              { side: 'NS', value: table.runningScore.get('NS') },
              { side: 'EW', value: table.runningScore.get('EW') },
            ],
          },
          hands:allHands
        })
      }

      // set stage
      table.stage = 'scoreboard' // FIXME: make that 'scoreboard'
      table.readyToPlay = []
      // set dealer and turn (both clockwise from current dealer)
      // TODO: move this to scoreboard function later
      table.turn = table.dealer
      table.incrementTurn()
      table.dealer = table.turn
      }

      // don't forget to save at the end!
      await table.save()
      return events
    }

    async handleKickOutUser (kickOutUser) {
      let events = []
      let table = await Table.findOne({ table_id: kickOutUser.table })
      if (!table) return // TODO: give some error or something
      console.log(`${kickOutUser.user} I want to kick out ${kickOutUser.kickuser} `)
      //check if host
      let userIndex = table.userIndex(kickOutUser.user)
      if(table.occupants[userIndex].host == false ){
        console.error(`${kickOutUser.user} only host can kick out people`)
        kickOutUser.error = {text: `only host can kick out people`}
        kickOutUser.to = kickOutUser.user
        kickOutUser.event = 'kickOutUser'
        events.push(kickOutUser)
        return events
      }
      else{
        //remove user
        let newHost = table.clearUser(kickOutUser.kickuser)

        // tell everyone about it
        for (let occupant of table.occupants.filter(
          o => { return o.status == 'active' }
          )) {
          events.push({
            event: 'kickoutuser',
            to: occupant.jid,
            table: kickOutUser.table,
            user: kickOutUser.user,
            kickuser: kickOutUser.kickuser,
          })
        }
      }

      // save it!
      await table.save()
      return events
    }

    async handleChangeDeal(changeDeal) {
      let events = []
      let table = await Table.findOne({ table_id: changeDeal.table })
      if (!table) return // TODO: give some error or something

      console.log(`${changeDeal.user} wants to Change Deal on ${changeDeal.table}`)
      // table, user, side, type, value
      let userIndex = table.userIndex(changeDeal.user)
      let side = changeDeal.side

      // check if user can redeal at this stage
      if (
        table.stage == 'scoreboard' &&
        changeDeal.type == 'redeal'
        ) {
        console.error(`${changeDeal.table} you cannot change deal at this stage`)
        changeDeal.error = {type: 'changeDeal', text: `${changeDeal.user} you cannot change deal at this stage`}
        changeDeal.to = changeDeal.user
        changeDeal.event = 'changeDeal'
        events.push(changeDeal)
        return events
      }

      // check if active player
      if (
        userIndex == -1 ||
        (table.occupants[userIndex].status != 'active' && table.occupants[userIndex].host )
        ) {
        console.error(`${changeDeal.user} is not an active player`)
        changeDeal.error = {text: `${changeDeal.user} only active players can raise undo request`}
        changeDeal.to = changeDeal.user
        changeDeal.event = 'changeDeal'
        events.push(changeDeal)
        return events
      }

      // check if requestee already in change deal votes
      if(table.changeDealVotes.filter(
        o => { return o.requestee == changeDeal.user }
      ).length != 0 ){
        console.error(`${changeDeal.user} requestee already in change deal votes`)
        changeDeal.error = {text: `${changeDeal.requestee} requestee already in change deal votes`}
        changeDeal.to = changeDeal.user
        changeDeal.event = 'changeDeal'
        events.push(changeDeal)
        return events
      }
      else{// add requestee to change deal votes
        table.changeDealVotes.push({
          requestee: changeDeal.user,
          type: changeDeal.type,
        })
        events.push({
          event: 'changeDeal',
          user: changeDeal.user,
          to: changeDeal.user,
          table: changeDeal.table,
          status: 'active',
          type: changeDeal.type,
          side: changeDeal.side
        })
      }

      //If this the first request then inform everyone for confirmation
      if(table.changeDealVotes.length == 1)
      {
        for (let occupant of table.occupants.filter( o => o.status == 'active')) {
          events.push({
            event: 'changeDeal',
            user: changeDeal.user,
            to: occupant.jid,
            table: changeDeal.table,
            status: 'request',
            requestee: changeDeal.user,
            type: changeDeal.type,
            side: changeDeal.side
          })
        }
      }

      let resetDone = false;
      let resetCards = 'none';
      if(table.changeDealVotes.length == 4 && table.changeDealVotes.filter(
        o => { return o.type == 'redeal' }
      ).length == 4){
        //Apply ReDeal
        await table.tableRedeal();
        resetDone = true;
        resetCards = 'reset';
      }
      if(table.changeDealVotes.length == 4 && table.changeDealVotes.filter(
        o => { return o.type == 'replay' }
      ).length == 4){
        //Apply Reset
        await table.tableResetDeal();
        resetDone = true;
        resetCards = 'replay';
      }
      if(table.changeDealVotes.length != 0 && table.changeDealVotes.filter(
        o => { return o.type == 'no' }
      ).length > 0){
        console.error(`${changeDeal.user} players rejected the change deal`)
        // changeDeal.error = {text: `${changeDeal.requestee} players rejected the change deal`}
        for (let occupant of table.occupants.filter(
          o => { return o.status == 'active'}
        )) {
          events.push({
            event: 'changeDeal',
            user: changeDeal.user,
            to: occupant.jid,
            table: changeDeal.table,
            status: 'reject',
            requestee: changeDeal.user,
            type: changeDeal.type,
            side: changeDeal.side
          })
        }

        table.claimRequest = []
        table.changeDealVotes = []
        table.undoRequest = []
      }
      await table.save()
      if(resetDone){
        let seatsEmpty = [...table.seats.values()].some(s => !s.user)

        if (!seatsEmpty && table.stage == 'inactive') {
          table.stage = 'infosharing'

          if(resetCards == 'reset') {
            if ([...table.seats.values()].every(s => s.cards.length == 0)) {
              table.dealCards()
            }
          }
          else if(resetCards == 'replay') {
            table.putBackCardsFromTrick()
          }

          //Ask everyone for catchup
          for (let occupant of table.occupants.filter(
            o => { return o.status == 'active'}
          )) {
            events.push({
              event: 'changeDeal',
              user: changeDeal.user,
              to: occupant.jid,
              table: changeDeal.table,
              status: 'catchup',
              type: changeDeal.type,
              side: changeDeal.side
            })
          }
        }

      }
      // don't forget to save at the end!
      await table.save()
      return events
    }

    async handleUndoRequest(undoRequest) {
      let events = []

      let table = await Table.findOne({ table_id: undoRequest.table })
      if (!table) return // TODO: give some error or something
      console.log(`${undoRequest.user} wants to undo ${undoRequest.side} -> ${undoRequest.suit} ${undoRequest.rank}`)
      // table, user, side, type, value
      let userIndex = table.userIndex(undoRequest.user)

      let side = undoRequest.side

      // check if active player
      if (
        userIndex == -1 ||
        table.occupants[userIndex].status != 'active'
        ) {
        console.error(`${undoRequest.user} is not an active player`)
        undoRequest.error = {type: 'auth', text: `${undoRequest.user} only active players can raise undo request`}
        undoRequest.to = undoRequest.user
        undoRequest.event = 'undo'
        events.push(undoRequest)
        return events
      }

      // player canaot play stage
      if (table.stage != 'play') {
        console.error(`${undoRequest.table} you cannot play cards at this stage`)
        undoRequest.error = {text: `${undoRequest.table} you cannot play cards at this stage`}
        undoRequest.to = undoRequest.user
        undoRequest.event = 'undo'
        events.push(undoRequest)
        return events
      }

      // player cannot play because claim is in progress
      if(table.hasClaimRequest()) {
        console.error(`${undoRequest.table} you cannot play cards because claim is in progress`)
        undoRequest.error = {text: `${undoRequest.table} you cannot play cards because claim is in progress`}
        undoRequest.to = undoRequest.user
        undoRequest.event = 'undo'
        events.push(undoRequest)
        return events
      }

      //dummy is not allowed
      if (
        table.contract &&
        table.contract.declarer &&
        table.getPartner(table.contract.declarer) == undoRequest.side &&
        table.occupants[userIndex].seat == undoRequest.side
      ) {
        console.error(`${undoRequest.table} Dummy is not allow to undo`)
        undoRequest.error = {text: `${undoRequest.table} Dummy is not allowed to undo`}
        undoRequest.to = undoRequest.user
        undoRequest.event = 'undo'
        events.push(undoRequest)
        return events
      }

      // Send the error the trick if it doesn't exist
      if (table.tricks.length < 1) {
        console.error(`${undoRequest.user} cannot request undo at this point.`)
        undoRequest.error = {text: `${undoRequest.user} cannot request undo at this point.`}
        undoRequest.to = undoRequest.user
        undoRequest.event = 'undo'
        events.push(undoRequest)
        return events
      }

      // if(side!=table.occupants[userIndex].seat){
      //   if(table.occupants[userIndex].seat == table.contract.declarer &&
      //     table.getPartner(table.contract.declarer) == side){
      //     table.undoRequest = undoRequest
      //     events.push(undoRequest)
      //     console.log("undo request saved for declarer")
      //   }
      //   else
      //     console.log("undo request not saved")
      // }
      // else{
        //Check for the last card Side
        let lastTrick = table.tricks.length - 1
        if (table.tricks[lastTrick].cards.length > 0) {
          if(table.tricks[lastTrick].cards[table.tricks[lastTrick].cards.length-1].side != side){
            console.error(`${undoRequest.user} cannot take back: Next Player already made a move`)
            undoRequest.error = {text: `${undoRequest.user} cannot take back: Next Player already made a move`}
            undoRequest.to = undoRequest.user
            undoRequest.event = 'undo'
            events.push(undoRequest)
            return events
          }
        }
        //Get the current player seat to get undoCount
        let s = table.seats.get(side)
        if(s.undoCount >= s.maxUndoCount){
          console.error(`${undoRequest.user} Undos are not allowed this point, reached max undo`)
          undoRequest.error = {text: `${undoRequest.user} Undos are not allowed this point, reached max undo`}
          undoRequest.to = undoRequest.user
          undoRequest.event = 'undo'
          events.push(undoRequest)
          return events
        }
        s.undoCount = s.undoCount+1
        table.seats.set(side, s)
        table.undoRequest = undoRequest
        table.undoRequest.requestees = []
        //calculate requestees
        for(let opponent of table.getOpponents(side)){
          if(opponent != table.getPartner(table.contract.declarer)){
            table.undoRequest.requestees.push({side:opponent})
          }
        }
        // tell everyone about it
        for (let occupant of table.occupants.filter(
          o => { return o.status == 'active' }
        )) {
          events.push({
            table: undoRequest.table,
            user: undoRequest.user,
            side: undoRequest.side,
            suit: undoRequest.suit,
            rank: undoRequest.rank,
            mode: undoRequest.mode,
            event: 'undo',
            to: occupant.jid,
          })
        }
      // }

      // don't forget to save at the end!
      await table.save()

      return events
    }

    async handleUndoResponse(undoResponse) {
      let events = []

      let table = await Table.findOne({ table_id: undoResponse.table })
      if (!table) return // TODO: give some error or something

      console.log(`${undoResponse.user} wants to undo ${undoResponse.mode}`)
      // table, user, side, type, value
      let userIndex = table.userIndex(undoResponse.user)

      let userSide = table.occupants[userIndex].seat

      // check if active player
      if (
        userIndex == -1 ||
        table.occupants[userIndex].status != 'active'
        ) {
        console.error(`${undoResponse.user} is not an active player`)
        undoResponse.error = {type: 'auth', text: `${undoResponse.user} only active players can ${undoResponse.mode} undo request`}
        undoResponse.to = undoResponse.user
        undoResponse.event = 'undo'
        events.push(undoResponse)
        return events
      }

      // player canaot play stage
      if (table.stage != 'play') {
        console.error(`${undoResponse.table} Takebacks only allowed during card play`)
        undoResponse.error = {text: `${undoResponse.table} Takebacks only allowed during card play`}
        undoResponse.to = undoResponse.user
        undoResponse.event = 'undo'
        events.push(undoResponse)
        return events
      }

      // player cannot play because claim is in progress
      if(table.hasClaimRequest()) {
        console.error(`${undoResponse.table} you cannot perform action because claim is in progress`)
        undoResponse.error = {text: `${undoResponse.table} you cannot perform action because claim is in progress`}
        undoResponse.to = undoResponse.user
        undoResponse.event = 'undo'
        events.push(undoResponse)
        return events
      }

      //dummy is not allowed
      if (
        table.contract &&
        table.contract.declarer &&
        table.getPartner(table.contract.declarer) == userSide &&
        table.occupants[userIndex].seat == userSide
      ) {
        console.error(`${undoResponse.table} Dummy is not allow to ${undoResponse.mode}`)
        undoResponse.error = {text: `${undoResponse.table} Dummy is not allowed to ${undoResponse.mode}`}
        undoResponse.to = undoResponse.user
        undoResponse.event = 'undo'
        events.push(undoResponse)
        return events
      }

      // Send the error the trick if it doesn't exist
      if (table.tricks.length < 1) {
        console.error(`${undoResponse.user} cannot request undo at this point.`)
        undoResponse.error = {text: `${undoResponse.user} cannot request undo at this point.`}
        undoResponse.to = undoResponse.user
        undoResponse.event = 'undo'
        events.push(undoResponse)
        return events
      }

      // Send the error the undo request if it doesn't exist
      if (!table.undoRequest || !table.undoRequest.side) {
        console.error(`${undoResponse.user} No Undo Request to accept`)
        undoResponse.error = {text: `${undoResponse.user} No Undo Request to accept`}
        undoResponse.to = undoResponse.user
        undoResponse.event = 'undo'
        events.push(undoResponse)
        return events
      }

      if(table.undoRequest && table.undoRequest.side!= userSide){
        if(undoResponse.mode ==  "accept")
        {
          //Mark acceptance
          await table.undoRequest.requestees.forEach((r,k) => {
            if(r.side == table.occupants[userIndex].seat)
            {
              r.response = true
              table.undoRequest.requestees.set(k,r)
            }
          })

          //apply if all user accepted
          if(table.undoRequest.requestees.filter( r => { return r.response }).length == table.undoRequest.requestees.length){
            //Code to accept to undo the card play
            let response = await this.updateAcceptUndoResponseData(table, undoResponse, events)
            if(response.events.length !=0 )
              return events
            table = response.table
            undoResponse.status = "accepted"
            undoResponse.cardsInTrick = response.cardsInTrick
          }
          else{
            //partial acceptance
            console.error(`${undoResponse.table} undo has been partially accepted by ${undoResponse.user}`)
            undoResponse.status = "partially_accepted"
          }
        }
        else { //Undo Response Reject
          //Code to reject to undo the card play
          let response = await this.updateRejectUndoResponseData(table, undoResponse, events)
          if(response.events.length !=0 )
            return events
          table = response.table
        }
        // tell everyone about it
        for (let occupant of table.occupants.filter(
          o => { return o.status == 'active' }
        )) {
          events.push({
            table: undoResponse.table,
            user: undoResponse.user,
            side: table.undoRequest.side,
            suit: table.undoRequest.suit,
            rank: table.undoRequest.rank,
            status: undoResponse.status,
            mode: undoResponse.mode,
            event: 'undo',
            to: occupant.jid,
            cardsintrick: undoResponse.cardsInTrick
          })
        }
        if((undoResponse.mode == "accept" && undoResponse.status == "accepted") || undoResponse.mode == "reject")
        {
          //Reset the undoRequest
          table.undoRequest = {}
        }
      }
      else{
        console.error(`${undoResponse.user} Cannot to accept undo response for this side`)
        undoResponse.error = {text: `${undoResponse.user} Cannot to accept undo response for this side`}
        undoResponse.to = undoResponse.user
        undoResponse.event = 'undo'
        events.push(undoResponse)
        return events
      }

      // don't forget to save at the end!
      await table.save()

      return events
    }

    async updateAcceptUndoResponseData(table, undoResponse, events){
      let currentTrick = table.tricks[table.tricks.length - 1]
      let lastCard = null
      let lastCardIndex = null
      if (currentTrick.cards.length > 0) {
        lastCardIndex = currentTrick.cards.length-1
        lastCard = currentTrick.cards[lastCardIndex]
      }else{
        let prevTrick = table.tricks[table.tricks.length - 2]
        lastCardIndex = prevTrick.cards.length-1
        lastCard = prevTrick.cards[lastCardIndex]
      }
      if(lastCard.card.rank != table.undoRequest.rank || lastCard.card.suit != table.undoRequest.suit)
      {
        console.error(`${undoResponse.user} Undo Request has already been cancelled`)
        undoResponse.error = {text: `${undoResponse.user} Undo Request has already been cancelled`}
        undoResponse.to = undoResponse.user
        undoResponse.event = 'undo'
        events.push(undoResponse)
        return {table, undoResponse, events}
      }

      let s = table.seats.get(table.undoRequest.side)
      s.cards.push(`${lastCard.card.suit}${lastCard.card.rank}`)
      table.seats.set(table.undoRequest.side, s)

      table.turn = table.undoRequest.side
      let cardsInTrick = []
      if(currentTrick.cards.length>0){
        table.tricks[table.tricks.length - 1].cards.splice(lastCardIndex,1)
        cardsInTrick = table.tricks[table.tricks.length - 1].cards
      }
      else{
        table.tricks[table.tricks.length - 2].cards.splice(lastCardIndex,1)
        cardsInTrick = table.tricks[table.tricks.length - 2].cards
        table.tricks.splice(table.tricks.length - 1,1)
      }

      return {table, undoResponse, events, cardsInTrick}
    }

    async updateRejectUndoResponseData(table, undoResponse, events){
      let currentTrick = table.tricks[table.tricks.length - 1]
      let lastCard = null
      let lastCardIndex = null
      if (currentTrick.cards.length > 0) {
        lastCardIndex = currentTrick.cards.length-1
        lastCard = currentTrick.cards[lastCardIndex]
      }else{
        let prevTrick = table.tricks[table.tricks.length - 2]
        lastCardIndex = prevTrick.cards.length-1
        lastCard = prevTrick.cards[lastCardIndex]
      }

      if(lastCard.card.rank != table.undoRequest.rank || lastCard.card.suit != table.undoRequest.suit)
      {
        console.error(`${undoResponse.user} Undo Request has already been cancelled`)
        undoResponse.error = {text: `${undoResponse.user} Undo Request has already been cancelled`}
        undoResponse.to = undoResponse.user
        undoResponse.event = 'undo'
        events.push(undoResponse)
        return {table, undoResponse, events}
      }

      table.undoRequest.rejected = true
      return {table, undoResponse, events}
    }

    async handleUndoCancel(undoCancel) {
      let events = []

      let table = await Table.findOne({ table_id: undoCancel.table })
      if (!table) return // TODO: give some error or something

      console.log(`${undoCancel.user} wants to cancel undo ${undoCancel.side}`)
      // table, user, side, type, value
      let userIndex = table.userIndex(undoCancel.user)

      let userSide = table.occupants[userIndex].seat

      // check if active player
      if (
        userIndex == -1 ||
        table.occupants[userIndex].status != 'active'
        ) {
        console.error(`${undoCancel.user} is not an active player`)
        undoCancel.error = {type: 'auth', text: `${undoCancel.user} only active players can ${undoCancel.status} undo request`}
        undoCancel.to = undoCancel.user
        undoCancel.event = 'undo'
        events.push(undoCancel)
        return events
      }

      // player canaot play stage
      if (table.stage != 'play') {
        console.error(`${undoCancel.table} Takebacks only allowed during card play`)
        undoCancel.error = {text: `${undoCancel.table} Takebacks only allowed during card play`}
        undoCancel.to = undoCancel.user
        undoCancel.event = 'undo'
        events.push(undoCancel)
        return events
      }

      // player cannot play because claim is in progress
      if(table.hasClaimRequest()) {
        console.error(`${undoCancel.table} you cannot perform action because claim is in progress`)
        undoCancel.error = {text: `${undoCancel.table} you cannot perform action because claim is in progress`}
        undoCancel.to = undoCancel.user
        undoCancel.event = 'undo'
        events.push(undoCancel)
        return events
      }

      // dummy is not allowed
      if (
        table.contract &&
        table.contract.declarer &&
        table.getPartner(table.contract.declarer) == userSide &&
        table.occupants[userIndex].seat == userSide
      ) {
        console.error(`${undoCancel.table} Dummy is not allow to play`)
        undoCancel.error = {text: `${undoCancel.table} Dummy is not allowed to play`}
        undoCancel.to = undoCancel.user
        undoCancel.event = 'undo'
        events.push(undoCancel)
        return events
      }

      // Send the error the Undo Request if it doesn't exist
      if (!table.undoRequest || !table.undoRequest.side) {
        console.error(`${undoCancel.user} No Undo Request to cancel`)
        undoCancel.error = {text: `${undoCancel.user} No Undo Request to cancel`}
        undoCancel.to = undoCancel.user
        undoCancel.event = 'undo'
        events.push(undoCancel)
        return events
      }
      let response = null
      if(table.undoRequest && table.undoRequest.side != userSide){
        if(userSide == table.contract.declarer ||
          table.getPartner(table.contract.declarer) == userSide){
          //Code to accept to undo the card play
          response = await this.updateUndoCancelData(table, table.getPartner(table.contract.declarer), events)
          if(response.events.length !=0 )
            return events

        }
        else{
          console.error(`${undoCancel.user} Cannot cancel undo request for this side`)
          undoCancel.error = {text: `${undoCancel.user}Cannot cancel undo request for this side`}
          undoCancel.to = undoCancel.user
          undoCancel.event = 'undo'
          events.push(undoCancel)
          return events
        }
      }
      else if(table.undoRequest && table.undoRequest.side == userSide){
        //Code to accept to undo the card play
        response = await this.updateUndoCancelData(table, userSide, events)
        if(response.events.length !=0 )
          return events
      }
      else{
        console.error(`${undoCancel.user} Cannot cancel undo request for this side`)
        undoCancel.error = {text: `${undoCancel.user} Cannot cancel undo request for this side`}
        undoCancel.to = undoCancel.user
        undoCancel.event = 'undo'
        events.push(undoCancel)
        return events
      }
      if(response != null)
      {
        table = response.table
        // tell everyone about it
        for (let occupant of table.occupants.filter(
          o => { return o.status == 'active' }
        )) {
          events.push({
            table: undoCancel.table,
            user: undoCancel.user,
            status: 'cancelled',
            mode: undoCancel.mode,
            side: undoCancel.side,
            event: 'undo',
            to: occupant.jid,
          })
        }
      }

      // don't forget to save at the end!
      await table.save()

      return events
    }

    async updateUndoCancelData(table, userSide, events){
      table.undoRequest = {}
      let s = table.seats.get(userSide)
      if(s.undoCount > 0)
        s.undoCount = s.undoCount-1
      table.seats.set(userSide, s)
      return {table, events}
    }

    async handleClaimRequest(claimRequest) {
      let events = []
      //let openSides = []
      let table = await Table.findOne({ table_id: claimRequest.table })
      if (!table) return // TODO: give some error or something

      console.log(`${claimRequest.user} wants to claim amount on ${claimRequest.amount}`)
      // table, user, side, type, value
      let userIndex = table.userIndex(claimRequest.user)
      let side = claimRequest.side
      let amount = claimRequest.amount


      // check if active player
      if (
        userIndex == -1 ||
        table.occupants[userIndex].status != 'active' ||
        table.occupants[userIndex].role != 'player'
        ) {
        console.error(`${claimRequest.user} is not an active player`)
        claimRequest.error = {text: `${claimRequest.user} only active players can claim amount`}
        claimRequest.to = claimRequest.user
        claimRequest.event = 'claim'
        events.push(claimRequest)
        return events
      }

      //Check if game in cardplay stage
      if (table.stage != 'play') {
        console.error(`${claimRequest.table} is not in cardplay stage right now`)
        claimRequest.error = {text: `${claimRequest.table} is not in cardplay stage right now`}
        claimRequest.to = claimRequest.user
        claimRequest.event = 'claim'
        events.push(claimRequest)
        return events
      }

      //dummy is not allowed to claim
      if (
        table.contract &&
        table.contract.declarer &&
        table.getPartner(table.contract.declarer) == claimRequest.side &&
        table.occupants[userIndex].seat == claimRequest.side
      ) {
        console.error(`${claimRequest.table} Only non dummy active player can make claim`)
        claimRequest.error = {text: `${claimRequest.table} Only non dummy active player can make claim`}
        claimRequest.to = claimRequest.user
        claimRequest.event = 'claim'
        events.push(claimRequest)
        return events
      }
      //check for conflicting claim
      if(table.hasClaimRequest()){
        if(table.claimRequest.side == side  &&
        table.claimRequest.amount == amount)
        {
          //inform user has claimed amount
          console.error(`${claimRequest.user} has claimend ${claimRequest.amount} tricks for ${claimRequest.side}`)
          claimRequest.error = {text: `${claimRequest.user} has claimend ${claimRequest.amount} tricks for ${claimRequest.side}`}
          claimRequest.to = claimRequest.user
          claimRequest.event = 'claim'
          events.push(claimRequest)
          return events
        } else{
          console.error(`${claimRequest.table} claim request is already in progress`)
          claimRequest.error = {text: `${claimRequest.table} claim request is already in progress`}
          claimRequest.to = claimRequest.user
          claimRequest.event = 'claim'
          events.push(claimRequest)
          return events
        }
      }

      // Count remaining tricks
      let trickCounter = 0
      let remainingTricks = 0
      for (let trick of table.tricks){
        if(trick.cards.length == table.seats.length){
          trickCounter += 1
        }
      }
      remainingTricks = (SUITS.size * RANKS.size/SIDES.size) - trickCounter

      //Check claim amount does not exceed remaining tricks
      if (amount > remainingTricks){
        console.error(`${claimRequest.user} tricks are not available to claim`)
        claimRequest.error = {text: `${claimRequest.user}  tricks are not available to claim`}
        claimRequest.to = claimRequest.user
        claimRequest.event = 'claim'
        events.push(claimRequest)
        return events
      }

      //save and send
      table.claimRequest = {}
      table.claimRequest.side = side
      table.claimRequest.amount = amount
      table.claimRequest.requestees = []
      //calculate requestees
      for(let opponent of table.getOpponents(side)){
        if(opponent != table.getPartner(table.contract.declarer)){
          table.claimRequest.requestees.push({side:opponent})
        }
      }

      let openHands = []
      // expose card
      if(table.openSides && !table.openSides.get(side))
      {
        // Get player cards and expose it to everyone
        for (let [seatSide, s] of table.seats.entries()) {
          if(seatSide == side)
          {
            openHands.push({
              side: seatSide,
              cards: s.cards.slice().reverse(),
            })
            table.openSides.set(seatSide,{
              side: seatSide,
              cards: s.cards.slice().reverse(),
            })
          }
        }
        // inform players of their claim requester cards
        for (let [seatSide, s] of table.seats.entries()) {
          if (seatSide != side) {
            events.push({
              event: 'state',
              to: s.user,
              table: claimRequest.table,
              hands: openHands
            })
          }
        }
      }


      // also inform all the players
      for (let occupant of table.occupants.filter(
        o => { return o.status == 'active' && o.role == 'player' }
      )) {
        events.push({
          event: 'claim',
          mode: claimRequest.mode,
          to: occupant.jid,
          table: claimRequest.table,
          amount: claimRequest.amount,
          side: claimRequest.side,
          user: claimRequest.user,
          hands: openHands,
        })
      }

      // don't forget to save at the end!
      await table.save()
      return events
    }

    async handleClaimResponse(claimResponse) {
      let events = []
      let table = await Table.findOne({ table_id: claimResponse.table })
      if (!table) return // TODO: give some error or something
      console.log(`${claimResponse.user} responding to claim amount on ${claimResponse.amount} ${claimResponse.side}`)
      // table, user, side, type, value
      let userIndex = table.userIndex(claimResponse.user)
      let user = claimResponse.user
      let side = claimResponse.side
      let amount = table.claimRequest.amount
      claimResponse.status = false

      // check if active player
      if (
        userIndex == -1 ||
        table.occupants[userIndex].status != 'active' ||
        table.occupants[userIndex].role != 'player'
        ) {
        console.error(`${claimResponse.user} is not an active player`)
        claimResponse.error = {text: `${claimResponse.user} only active players can claim amount`}
        claimResponse.to = claimResponse.user
        claimResponse.event = 'claim'
        events.push(claimResponse)
        return events
      }

      //Check stage
      if (table.stage != 'play') {
        console.error(`${claimResponse.table} is not in cardplay stage right now`)
        claimResponse.error = {text: `${claimResponse.table} is not in cardplay stage right now`}
        claimResponse.to = claimResponse.user
        claimResponse.event = 'claim'
        events.push(claimResponse)
        return events
      }
      //Check the claim matches
      if( (claimResponse.mode == 'withdraw' && side != table.claimRequest.side) ||
          (claimResponse.mode != 'withdraw' && table.claimRequest.requestees.filter( r => { return r.side == table.occupants[userIndex].seat }).length == 0 ))
      {
        console.error(`${claimResponse.table} claim has already expired`)
        claimResponse.error = {text: `${claimResponse.table} claim has already expired`}
        claimResponse.to = claimResponse.user
        claimResponse.event = 'claim'
        events.push(claimResponse)
        return events
      }

      if(claimResponse.mode == 'accept'){
        //Mark acceptance
        await table.claimRequest.requestees.forEach((r,k) => {
          if(r.side == table.occupants[userIndex].seat)
          {
            r.response = true
            table.claimRequest.requestees.set(k,r)
          }
        })

        //apply if all user accepted
        if(table.claimRequest.requestees.filter( r => { return r.response }).length == table.claimRequest.requestees.length){

          let trickCounter = 0;
          let remainingTricks = 0;
          // Count remaining tricks
          for (let trick of table.tricks){
            if(trick.cards.length == SIDES.size){
              trickCounter += 1
            }
          }
          remainingTricks = (SUITS.size * RANKS.size/SIDES.size) - trickCounter

          //save tricks
          if(['N','S'].includes(table.claimRequest.side)){
            table.claimedTricks.NS = table.claimRequest.amount
            table.claimedTricks.EW = remainingTricks - table.claimRequest.amount
          }else{
            table.claimedTricks.EW = table.claimRequest.amount
            table.claimedTricks.NS = remainingTricks - table.claimRequest.amount
          }
          table.claimedTricks.claimant = table.claimRequest.side
          table.claimRequest = {}
          //initiate end game
          let trickScore = table.trickScore()
          trickScore['NS'] += table.claimedTricks.NS
          trickScore['EW'] += table.claimedTricks.EW
          let dealScore = table.hoolScore(table.contract,trickScore['NS'],trickScore['EW'])
          //update running score
          table.runningScore.set('NS', table.runningScore.get('NS') + dealScore['NS'])
          table.runningScore.set('EW', table.runningScore.get('EW') + dealScore['EW'])
          console.log("Game End")

          //broadcast info
          for (let occupant of table.occupants.filter(
            o => { return o.status == 'active' }
          )) {
            events.push({
              event: 'claim',
              mode: claimResponse.mode,
              to: occupant.jid,
              user: claimResponse.user,
              table: claimResponse.table,
              side: claimResponse.side,
              claimant : table.claimedTricks.claimant,
              amount : claimResponse.amount,
              status: 'accepted',
            })
          }
          console.log("Game Over")
          console.log("runningScore",table.runningScore)
          console.log("DealScore",dealScore)
          let allHands = []
          // inform all the players of all cards
          for (let [side, s] of table.gameHistory[table.gameHistory.length-1].seats.entries()) {
            allHands.push({
              side: side,
              cards: s.cards.slice().reverse(),
            })
          }
          // tell everyone about it
          for (let occupant of table.occupants.filter(
            o => { return o.status == 'active' }
          )) {
            events.push({
              event: 'state',
              to: occupant.jid,
              user: claimResponse.user,
              table: claimResponse.table,
              status: 'active',
              score: {
                trick: [
                  { side: 'NS', value: trickScore['NS'] },
                  { side: 'EW', value: trickScore['EW'] },
                ],
                deal: [
                  { side: 'NS', value: dealScore['NS'] },
                  { side: 'EW', value: dealScore['EW'] },
                ],
                running: [
                  { side: 'NS', value: table.runningScore.get('NS') },
                  { side: 'EW', value: table.runningScore.get('EW') },
                ],
              },
              hands: allHands
            })
          }

          // set stage
          table.stage = 'scoreboard' // FIXME: make that 'scoreboard'
          table.readyToPlay = []
          // set dealer and turn (both clockwise from current dealer)
          // TODO: move this to scoreboard function later
          table.turn = table.dealer
          table.incrementTurn()
          table.dealer = table.turn
        }
        else{
          //partial acceptance
          console.error(`${claimResponse.table} claim has been accepted by ${claimResponse.user}`)
          claimResponse.message = `${claimResponse.amount} claim has been accepted by you`
          claimResponse.to = claimResponse.user
          claimResponse.event = 'claim'
          claimResponse.status = 'partially_accepted'
          events.push(claimResponse)
        }
      }
      else if(claimResponse.mode == 'reject'){
          //Mark Rejectance
          table.claimRequest = {}
          //inform everyone abount claim rejection
          for (let occupant of table.occupants.filter(
            o => { return o.status == 'active' }
          )) {
            events.push({
              event: 'claim',
              mode: claimResponse.mode,
              to: occupant.jid,
              table: claimResponse.table,
              user: claimResponse.user,
              side: claimResponse.side,
              claimant : table.claimedTricks.claimant,
              amount : claimResponse.amount,
              status: 'rejected',
            })
          }
      }
      else {
        table.claimRequest = {}
        //inform everyone abount claim rejection
        for (let occupant of table.occupants.filter(
          o => { return o.status == 'active' }
        )) {
          events.push({
            event: 'claim',
            mode: claimResponse.mode,
            to: occupant.jid,
            table: claimResponse.table,
            user: claimResponse.user,
            side: claimResponse.side,
            amount: claimResponse.amount,
            status: 'withdraw',
          })
        }
      }
      // don't forget to save at the end!
      await table.save()
      return events
    }

    async handleScoreboardResponse(scoreboard) {
      let events = []
      let table = await Table.findOne({ table_id: scoreboard.table })
      if (!table) return // TODO: give some error or something
      console.log(`${scoreboard.user} ready to play`)
      // table, user, side, type, value
      let userIndex = table.userIndex(scoreboard.user)

      // check if active player
      if (
        userIndex == -1 ||
        table.occupants[userIndex].status != 'active' ||
        table.occupants[userIndex].status == 'unavailable'||
        table.occupants[userIndex].role != 'player'
        ) {
        console.error(`${scoreboard.user} is not an active player`)
        scoreboard.error = {text: `${scoreboard.user} only active players can play`}
        scoreboard.to = scoreboard.user
        scoreboard.event = 'scoreboard'
        events.push(scoreboard)
        return events
      }

      //Check stage
      if (table.stage == 'inactive') {
        console.error(`${scoreboard.table} is not in active stage right now`)
        scoreboard.error = {text: `${scoreboard.table} is not in active stage right now`}
        scoreboard.to = scoreboard.user
        scoreboard.event = 'scoreboard'
        events.push(scoreboard)
        return events
      }
      let newDeal = false;
      if(scoreboard.mode=="ready")
      {
        //new deal
        newDeal = table.markReady(scoreboard.side)
      }
      else{
        //add side to Not ready to play
        table.notReady.push(this.side)
      }
      //broadcast info
      for (let occupant of table.occupants.filter(
        o => { return o.status == 'active' }
        )) {
          events.push({
            event: 'scoreboard',
            mode: scoreboard.mode,
            to: occupant.jid,
            user: scoreboard.user,
            table: scoreboard.table,
            side: scoreboard.side,
          })
      }

      if(newDeal){
        let allHands = []
        // inform players of their own cards
        for (let [side, s] of table.seats.entries()) {
          events.push({
            event: 'state',
            to: s.user,
            table: scoreboard.table,
            status: 'inactive',
            hands: [{
              side: side,
              cards: s.cards.slice().reverse(),
            }],
            tableinfo: {stage:table.stage, turn:table.turn, dealer:table.dealer},
          })

          allHands.push({
            side: side,
            cards: s.cards.slice().reverse(),
          })
        }

        // also inform all the kibitzers
        for (let occupant of table.occupants.filter(
          o => { return o.status == 'active' && o.role == 'kibitzer' }
        )) {
          events.push({
            event: 'state',
            to: occupant.jid,
            table: scoreboard.table,
            status: 'active',
            hands: allHands,
            tableinfo: {stage:table.stage, turn:table.turn, dealer:table.dealer},
          })
        }
      }
      // don't forget to save at the end!
      await table.save()
      return events
    }

    async handleCatchupInfoResponse(catchup) {
      let events = []
      let table = await Table.findOne({ table_id: catchup.table })
      if (!table) return // TODO: give some error or something
      console.log(`${catchup.user} wants catchup info`)
      // table, user, side, type, value
      let userIndex = table.userIndex(catchup.user)
      if (
        userIndex == -1 ||
        table.occupants[userIndex].status != 'active'
        ) {
        console.error(`${catchup.user} is not an active player`)
        catchup.error = {text: `${catchup.user} only active players can fetch catchup`}
        catchup.to = catchup.user
        catchup.event = 'catchup'
        events.push(catchup)
        return events
      }
      let role = table.occupants[userIndex].role
      events = await this.catchupInfo(table, catchup, role, catchup.side, events)
      return events
    }

    listen(transmitter) {
      this.transmitter = transmitter

      // set up hool IQ response functions
      // (i.e. xmpp queries that always require a response)


      // respond to incoming events
      this.transmitter.on('tableJoin', async (request) => {
        let events = await this.handleTableJoin(request)

        // send out response
        for (let event of events) {
          this.emit(event.event, event)
        }
      })

      this.transmitter.on('tableLeave', async (request) => {
        let events = await this.handleTableLeave(request)

        // send out response
        for (let event of events) {
          this.emit(event.event, event)
        }
      })

      this.transmitter.on('tableExit', async (request) => {
        let events = await this.handleTableExit(request)

        // send out response
        for (let event of events) {
          this.emit(event.event, event)
        }
      })

      this.transmitter.on('infoshare', async (infoshare) => {
        let events = await this.handleInfoshare(infoshare)

        // send out response
        for (let event of events) {
          this.emit(event.event, event)
        }

      })

      this.transmitter.on('bid', async (bid) => {
        let events = await this.handleBid(bid)

        // send out response
        for (let event of events) {
          this.emit(event.event, event)
        }
      })

      this.transmitter.on('cardplay', async (cardplay) => {
        let events = await this.handleCardplay(cardplay)

        // send out response
        for (let event of events) {
          this.emit(event.event, event)
        }
      })

      // respond to undo events
      this.transmitter.on('undo', async (undo) => {
        let events = []
        if(undo.mode == 'request')
          events = await this.handleUndoRequest(undo)
        else if(undo.mode == 'accept' || undo.mode == 'reject')
          events = await this.handleUndoResponse(undo)
        else if(undo.mode == 'cancel')
          events = await this.handleUndoCancel(undo)
        // send out response
        for (let event of events) {
          this.emit(event.event, event)
        }
      })

      this.transmitter.on('claim', async (claim) => {
        let events = []
        if(claim.mode == 'request')
          events = await this.handleClaimRequest(claim)
        else
          events = await this.handleClaimResponse(claim)

        // send out response
        for (let event of events) {
          this.emit(event.event, event)
        }
      })

      this.transmitter.on('kickoutuser', async (kickOutUser) => {
        let events = await this.handleKickOutUser(kickOutUser)

        // send out response
        for (let event of events) {
          this.emit(event.event, event)
        }
      })

      this.transmitter.on('scoreboard', async (scoreboard) => {
        let events = []
        events = await this.handleScoreboardResponse(scoreboard)

        // send out response
        for (let event of events) {
          this.emit(event.event, event)
        }
      })

      this.transmitter.on('catchup', async (catchup) => {
        let events = []
        events = await this.handleCatchupInfoResponse(catchup)

        // send out response
        for (let event of events) {
          this.emit(event.event, event)
        }
      })

      // respond to changeDeal events
      this.transmitter.on('changedeal', async (changeDeal) => {
        let events = await this.handleChangeDeal(changeDeal)

        // send out response
        for (let event of events) {
          this.emit(event.event, event)
        }
      })
    }
  }

module.exports = {
  Card,
  HoolEngine,
  Table,
  User,
}
