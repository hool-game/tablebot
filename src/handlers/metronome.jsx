const telnet = require('telnet-client')
const events = require('events')

/**
 * Metronome
 *
 * Provides a generic interface to Metronome-specific commands
 * This class mainly uses Metronome's admin_telnet interface, so
 * be sure to enable that in your metronome.cfg.lua as described
 * in the Metronome docs:
 *
 * https://metronome.im/documentation/administration-console
 *
 * If you want to add support for more backends, create a copy of
 * this file with the name of the other backend, and modify the
 * code here accordingly. This will be helpful if we want to move
 * to a more heavy-duty server surch as ejabberd or MongooseXMPP
 * in future.
 */

class MetronomeHandler {
  constructor(hostname='localhost', port=5582, shellPrompt='') {
    this.hostname = hostname
    this.port = 5582
    this.shellPrompt = shellPrompt
  }

  async changeUserPassword(jid, password) {
    let response = await runCommand.call(this, `user:password("${jid.toString()}", "${password}")`)

    if (response.status == 'error') {
      // TODO: maybe parse the error more here?

      return {
        status: 'error',
        error: response.error || 'Something went wrong while running command',
      }
    }

    return response
  }

  async createUser(jid, password) {
    return await runCommand.call(this, `user:create("${jid.toString()}", "${password}")`)
  }

  async deleteUser(jid) {
    return await runCommand.call(this, `user:delete("${jid.toString()}")`)
  }

}

/**
 * runCommand
 *
 * Private function to run a command through telnet and return the
 * output. Will be called from within the main class via .call(this)
 */

async function runCommand(command) {
  let client = new telnet()

  // first, let's see if we can ever connect!
  try {
    await client.connect({
      host: this.host,
      port: this.port,
      shellPrompt: this.shellPrompt,
    })
  } catch(error) {
    // no we can't! see? what did i tell you?
    return {
      status: 'error',
      error: 'Unable to finish telnet transaction',
    }
  }

  // oh great that worked! guess we'll have to do some *real work* then
  let waiting = events.once(client, 'data')
  await client.send(command)

  // wait till the response comes (it's not transactional for some reason)
  let response = await waiting
  response = '' + response.map(r => r.asciiSlice())

  // clean up before we go
  client.destroy()

  let success = response.match(/^\|\ OK\:\ (?<message>.*)/)

  if (!success) {
    // check if there's an explanation given
    if (response.match(/^\|\ Command\ completed\ with\ a\ problem/)) {
      let lines = response.split('\r\n')
      if (lines.length > 1) {
        let message = lines[1].match(/^\|\ Message\:\ (?<message>.*)/)
        if (message && message.groups.message) {
          message = message.groups.message
          return {
            status: 'error',
            error: message,
          }
        }
      }
    }

    return {
      status: 'error',
      error: "An unknown error occurred and we don't know how to handle it :(",
    }
  }

  return {
    status: 'ok',
    message: success.groups.message || 'Operation completed, apparently',
  }
}

module.exports = {
  MetronomeHandler
}
