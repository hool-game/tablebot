const mongoose = require('mongoose')
const uuid = require('uuid')
const { jid } = require('@xmpp/component')
const { HoolTransmitter } = require('./xmpp')
const { HoolEngine } = require('./engine')
const { HoolMailer } = require('./email')

// user registration endpoint
const { HoolHTTPServer } = require('./server')

// Metronome-specific handler, for managing users
const { MetronomeHandler } = require('./handlers/metronome')

// load config from .env file (if exists)
require("dotenv").config()
const TABLEBOT = process.env.TABLEBOT_DOMAIN
const TABLEBOT_JID = jid(`bot@${TABLEBOT}`)

const TABLEBOT_COMPONENT_TITLE = process.env.TABLEBOT_COMPONENT_TITLE || 'Hool Tables'

function start() {
  // set up xmpp client
  const transmitter = new HoolTransmitter({
      service: process.env.TABLEBOT_SERVICE,
      domain: process.env.TABLEBOT_DOMAIN,
      password: process.env.TABLEBOT_PASSWORD,
    })

  // set up hool engine
  const hool = new HoolEngine({
    secret_key: process.env.TABLEBOT_SECRET_KEY,
    http_domain: process.env.TABLEBOT_HTTP_DOMAIN,
  })

  // set up database hook
  mongoose.connection.once('open', async function() {
    console.log('connected to database.')
    await transmitter.start().catch(console.error)
  })

  // set up http server
  const server = new HoolHTTPServer({
    port: process.env.TABLEBOT_HTTP_PORT || 5000,
    defaultDomain: process.env.TABLEBOT_ACCOUNT_DOMAIN || process.env.TABLEBOT_DOMAIN
  })

  // set up email sender if configured
  let mailer
  if (
    !!process.env.TABLEBOT_EMAIL_HOST &&
    !!process.env.TABLEBOT_EMAIL_USERNAME &&
    !!process.env.TABLEBOT_EMAIL_PASSWORD
  ) {
    mailer = new HoolMailer({
      host: process.env.TABLEBOT_EMAIL_HOST,
      username: process.env.TABLEBOT_EMAIL_USERNAME,
      password: process.env.TABLEBOT_EMAIL_PASSWORD,
      port: process.env.TABLEBOT_EMAIL_PORT,
      secure: process.env.TABLEBOT_EMAIL_SECURE == 'false' ? false : true,
      defaultFrom: process.env.TABLEBOT_EMAIL_DEFAULT_SENDER,
    })
  } else {
    console.warn('Email delivery is not set up. Email content will be printed to the console instead.')
    mailer = new HoolMailer({ dummy: true })
  }

  // link everything together
  hool.setDB(mongoose.connection) // set the database
  hool.setMailer(mailer) // set the email handler
  hool.setServerHandler(new MetronomeHandler()) // set the XMPP server handle (for user management)
  hool.listen(transmitter) // listens for incoming XMPP messages
  transmitter.listen(hool) // listens for outgoing XMPP broadcasts
  server.listen(hool) // listens for API calls and passes on to engine

  // connect and start the whole process
  mongoose.connect(process.env.TABLEBOT_MONGODB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })

  // handle termination
  process.once('SIGINT', async (code) => {
    console.log("Off we go")

    await server.stop()
    console.log('http server disconnected.')

    await mongoose.connection.close()
    console.log('database disconnected.')

    await transmitter.stop()
    console.log('component disconnected.')
  })
}

// run if main script
if (!module.parent) {
  start()
}
