const express = require("express")
const bodyParser = require("body-parser")
const jid = require("@xmpp/jid")
const xml = require("@xmpp/xml")


class HoolHTTPServer {
  constructor (options = {}) {

    if (!!options.engine) {
      this.engine = options.engine
    }

    if (!!options.port) {
      this.port = options.port
    } else {
      this.port = 5000
    }

    if (!!options.defaultDomain) {
      this.defaultDomain = options.defaultDomain
    }

    this.app = express()

    // ALlow URLencoded API
    this.app.use(express.urlencoded({
       extended: false
    }))

    // Allow JSON API
    this.app.use(express.json())
  }

  /**
   * Generic handlers
   *
   * These handlers are called upon by the router to do the main logic
   * work. The router can then return a response in HTML or JSON as
   * appropriate.
   */

   /**
    * Account registration
    */
  async registerAccount(req, res) {
    if (!req.body.username) {
      return {
        status: "invalid",
        message: "Please specify a username",
      }
    }

    if (!req.body.password) {
      return {
        status: "invalid",
        message: "Please specify a password",
      }
    }

    // Attempt to create account
    let answer = await this.engine.handleRegistration({
      username: req.body.username,
      password: req.body.password,
      email: req.body.email,
      name: req.body.name,
      location: req.body.location,
    }, false, this.defaultDomain)

    if (!!answer.error) {
      return {
        status: answer.error.tag || "error",
        message: answer.error.text,
      }
    }

    return {
      status: "ok",
      username: answer.username,
      email: answer.email,
      name: answer.name,
      location: answer.location,
      jid: answer.jid,
    }
  }


  /**
   * Email verification
   */

  async verifyEmail(req, res) {

    // first check that key and jid are provided
    if (!req.query.key) {
      return {
        status: "invalid",
        message: "Please specify a key",
      }
    }

    if (!req.query.jid) {
      return {
        status: "invalid",
        message: "Please specify a JID",
      }
    }

    // decode key (unless it's timed out/expired)
    let serialiser = this.engine.serialiser(this.engine.secret_key, req.query.jid)
    let email

    try {
      email = serialiser.loads(req.query.key, 100000) // 20 ms
    } catch(e) {
      if (e.name == 'SignatureExpired') {
        return{
          status: "error",
          message: "This link has expired. Please generate a new one."
        }
      } else {
        return {
          status: "error",
          message: "An unknown error occurred. Please generate a new link and try again."
        }
      }
    }

    // pass it on to the engine function for processing
    let answer = await this.engine.handleEmailVerification({
      jid: req.query.jid,
      email: email,
    })

    if (!!answer.error) {
      return {
        status: "error",
        message: answer.error.text
      }
    }

    if (!answer.verified) {
      return {
        status: "error",
        message: "Something weird happened: the transaction went through but your email still isn't marked as verified :("
      }
    }

    return {
      status: "ok",
      message: answer.message || "Your email has been verified!",
      verified: true
    }
  }


  /**
   * Password reset
   */

  async requestPasswordReset(req, res, useJSON = false) {
    // first check that jid is specified
    if (!req.body.jid) {
      return {
        status: "invalid",
        message: "Please specify a JID",
      }
    }

    // retrieve user info
    let u = await this.engine.getUserInfo(req.body.jid)

    if (!!u && !!u.email) {

      // check that email has been verified
      if (!u.email.verified) {
        return {
          status: "error",
          message: "Your email has not been verified, so you can't use it to reset your password",
        }
      }

      // generate verification link
      let serialiser = new this.engine.serialiser(this.engine.secret_key, u.email.address)
      let verification_link = `${this.engine.http_domain||'https://' + this.engine.transmitter.domain}/${ useJSON ? 'api/' : ''}reset_password/verify?email=${encodeURIComponent(u.email.address)}&key=${encodeURIComponent(serialiser.dumps(u.jid.toString()))}`

      // send verification email
      if (!!this.engine.mailer) {
        // send email
        this.engine.mailer.sendMail({
          to: u.email.address,
          subject: "Reset your Hool password",
          text: `Hello ${u.jid.local},\n\nSomeone (hopefully you) recently requested a password reset. You can do so by clicking the following link:\n\n${verification_link}\n\nIf you didn't send this email, don't worry: you can safely ignore it and the link will soon expire. Don't forward it to anyone in the meantime, though!\n\nCheers 🙂\nThe Hool Team`, // TODO: include how long it'll take to expire
        })
      } else {
        // otherwise, print to console
        console.log('Email sending is not set up. Please direct users to this link:', verification_link)
      }
    }

    return {
      status: "ok",
      message: "If the account exists, you will find an email with password reset instructions in your inbox.",
    }
  }


  async getPasswordReset(req, res) {
    // check for key and email

    if (!req.query.key || !req.query.email) {
      return {
        status: "invalid",
        message: "Please specify a key and email",
      }
    }

    // decode key (unless it's timed out/expired)
    let serialiser = new this.engine.serialiser(this.engine.secret_key, req.query.email)
    let userJID

    try {
      userJID = jid(serialiser.loads(req.query.key, 100000)) // 20 ms
    } catch(e) {
      if (e.name == 'SignatureExpired') {
        return {
          status: "error",
          message: "This link has expired. Please generate a new one."
        }
      } else {
        return {
          status: "error",
          message: "An unknown error occurred. Please generate a new link and try again."
        }
      }
    }

    return {
      status: "ok",
      jid: userJID.toString(),
      message: "Please enter a new password and send it in",
    }
  }


  async setPasswordReset(req, res) {
    if (!req.body.key || !req.body.email || !req.body.password) {
      return {
        status: "invalid",
        message: "Please provide your email, (requested new) password, and (verification) key.",
      }
    }

    // decode key (unless it's timed out/expired)
    let serialiser = this.engine.serialiser(this.engine.secret_key, req.body.email)
    let userJID

    try {
      userJID = jid(serialiser.loads(req.body.key, 100000)) // 20 ms
    } catch(e) {
      if (e.name == 'SignatureExpired') {
        return {
          status: "error",
          message: "This link has expired. Please generate a new one."
        }
      } else {
        return {
          status: "error",
          message: "An unknown error occurred. Please generate a new link and try again."
        }
      }
    }

    // pass it on to the engine function for processing
    let answer = await this.engine.handlePasswordReset({
      jid: userJID,
      email: req.body.email,
      password: req.body.password
    })

    if (!!answer.error) {
      return{
        status: "error",
        message: answer.error.text
      }
    }

    return {
      status: "ok",
      jid: userJID.toString(),
      message: answer.message || "Your password has been changed!",
    }
  }


  /**
   * HTML helper
   *
   * This function helps to render HTML. In future, we should start
   * using some kind of templating system for this instead.
   */

  renderHTML(options={}) {

    // set default options
    if (!options.title) options.title = 'Welcome to Hool!'
    if (!options.message) options.message = (
      "There's nothing here though: maybe you want to go to the app?"
    )
    if (!options.error) options.error = false

    // render the result
    return ((
      <html>
        <head>
          <title>Hool | {options.title}</title>
        </head>
        <body>
          <h1>{options.title}</h1>
          <p style={ options.error ? 'color: red;' : '' }>{options.message}</p>
          { options.extra || ''}
        </body>
      </html>
    ).toString())
  }


  listen(engine = this.engine) {
    if (!engine) throw "No engine specified! Please specify a HoolEngine"
    this.engine = engine

    /*
     * JSON API endpoints
     *
     * These endpoints expose our backend's JSON API (to be accessed
     * behind the scenes by the app itself, for example)
     */

    this.app.get("/", (req, res) => {
      res.json({ status: "ok", message: "welcome, hooligan!"})
    })

    // Register account
    this.app.post("/api/register", async (req, res) => {
      res.json(await this.registerAccount(req, res))
    })

    // Verify email
    this.app.get("/api/verify_email", async (req, res) => {
      res.json(await this.verifyEmail(req, res))
    })

    // Reset password
    this.app.put("/api/reset_password", async (req, res) => {
      res.json(await this.requestPasswordReset(req, res, true)) // true means "use JSON"
    })

    this.app.get("/api/reset_password/verify", async (req, res) => {
      res.json(await this.getPasswordReset(req, res))
    })

    this.app.put("/api/reset_password/verify", async (req, res) => {
      res.json(await this.setPasswordReset(req, res))
    })

    /*
     * HTTP API endpoints
     *
     * These are the browser-friendly versions of the above JSON
     * API, which people can access from their web browsers. These
     * will be used when (for example) people click on verification
     * or reset links received by email. We don't want to show fancy
     * code in curly braces to *those* people!
     */

    this.app.get("/verify_email", async (req, res) => {
      let response = await this.verifyEmail(req, res)

      res.send(this.renderHTML({
        title: (response.status == "ok"
          ? 'Email verified'
          : 'Email verification failed'
        ),
        message: response.message,
        error: response.status != "ok",
      }))
    })

    this.app.get("/reset_password", async (req, res) => {
      res.send(this.renderHTML({
        title: 'Reset Password',
        message: "Forgot your password? No worries: enter your username/JID and we'll send a reset link to your inbox",
        extra: (
          <form action="" method="post">
            <label for="jid">Username</label>
            <input
              type="text"
              name="jid"
              placeholder="chewbacca@hool.org"
            />
            <input
              type="submit"
              value="Get email"
            />
          </form>
        )
      }))
    })

    this.app.post("/reset_password", async (req, res) => {
      if (!req.body.jid) {
        res.send(this.renderHTML({
          title: 'Reset Password',
          message: "Forgot your password? No worries: enter your username/JID and we'll send a reset link to your inbox",
          extra: (
            <form action="" method="post">
              <label for="jid">Username</label>
              <input
                type="text"
                name="jid"
                placeholder="chewbacca@hool.org"
              />
              <input
                type="submit"
                value="Get email"
              />
            </form>
          )
        }))
        return
      }

      // Add default hostname if necessary
      // For example: chewbacca -> chewbacca@hool.org
      let userJID = jid(req.body.jid)
      if (!userJID.local) {
        userJID.local = userJID.domain
        userJID.domain = this.defaultDomain || this.engine.transmitter.domain
        req.body.jid = userJID.toString()
      }

      let response = await this.requestPasswordReset(req, res)

      res.send(this.renderHTML({
        title: 'Reset Password',
        message: response.message,
        error: response.status != "ok",
      }))
    })

    this.app.get("/reset_password/verify", async (req, res) => {
      let response = await this.getPasswordReset(req, res)

      res.send(this.renderHTML({
        title: 'Reset Password',
        message: response.status == "ok" ? `Changing password for ${response.jid}` : response.message,
        extra: (
          <form action="" method="post">
            <input type="hidden" name="jid" value={response.jid}/>
            <input type="hidden" name="email" value={req.query.email}/>
            <input type="hidden" name="key" value={req.query.key}/>
            <label for="password">New password</label>
            <input
              type="password"
              name="password"
              placeholder="******"
            />
            <input
              type="submit"
              value="Change Password"
            />
          </form>
        ),
        error: response.status != "ok"
      }))
    })

    this.app.post("/reset_password/verify", async (req, res) => {
      let response = await this.setPasswordReset(req, res)

      if (response.status == "invalid") {
        res.send(this.renderHTML({
          title: 'Reset Password',
          message: response.status == "ok" ? `Changing password for ${response.jid}` : response.message,
          extra: (
            <form action="" method="post">
              <input type="hidden" name="jid" value={response.jid}/>
              <input type="hidden" name="email" value={req.query.email}/>
              <input type="hidden" name="key" value={req.query.key}/>
              <label for="password">New password</label>
              <input
                type="password"
                name="password"
                placeholder="******"
              />
              <input
                type="submit"
                value="Change Password"
              />
            </form>
          ),
          error: response.status != "ok"
        }))
        return
      }

      res.send(this.renderHTML({
        title: 'Reset Password',
        message: response.status == "ok" ? `Password changed for ${response.jid}. You can now return to the app to sign in with the new password.` : response.message,
        error: response.status != "ok",
      }))
    })

    // Start the server!

    this.server = this.app.listen(this.port, () => {
      console.log(`HTTP server is running on port ${this.port}.`)
    })
  }

  stop() {
    if (!!this.server) {
      return this.server.close()
    }
  }

}

module.exports = {
  HoolHTTPServer,
}
