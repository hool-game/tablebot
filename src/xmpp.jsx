const { component, xml, jid } = require("@xmpp/component")
const debug = require("@xmpp/debug")

const { once, EventEmitter } = require('events')

class HoolTransmitter extends EventEmitter {

    // xmpp helper functions

    constructor(options,...args) {
      // initial settings go here
      super(...args)

      if (!options) throw 'No options defined'
      if (!options.domain) throw 'Please specify a domain'
      if (!options.service) throw 'Please specify an XMPP service'
      if (!options.password) throw 'Please specify a password!'

      this.domain = options.domain
      this.service = options.service
      this.componentTitle = options.componentTitle || 'Hool Tables'

      this.xmpp = component({
        service: this.service,
        domain: this.domain,
        password: options.password,
      })

      console.log(`the service is ${this.service} and domain is ${this.domain}`)

      // test data
      // TODO get rid of this, and soon!

      // dummy table lists
      // TODO: replace with actual database calls in the function!

      this.DUMMY_TABLES = [
        {
          jid: 'bamboo@' + this.domain,
          name: 'Woody grass gaming',
          time: -1,
          history: true,
          players: 3,
          kibitzers: 1,
          host: 'bombay@hool.org'
        },
        {
          jid: 'banyan@' + this.domain,
          name: 'Hanging out with Hool',
          time: -1,
          history: true,
          players: 4,
          kibitzers: 19,
          host: 'calcutta@hool.org'
        },
        {
          jid: 'coconut@' + this.domain,
          name: 'Polished games with lots of refreshment',
          time: -1,
          players: 3,
          kibitzers: 1,
          host: 'madras@hool.org'
        },
        {
          jid: 'hopea@' + this.domain,
          name: 'A termite-proof game that can sail at a pinch',
          time: 1,
          history: true,
          players: 4,
          kibitzers: 12,
          host: 'bangalore@hool.org'
        },
        {
          jid: 'pine@' + this.domain,
          name: 'A hard and tough game where you may meet your match',
          time: -1,
          history: true,
          players: 3,
          kibitzers: 1,
          host: 'hyderabad@hool.org'
        },
      ]

      // setup event catching

      this.xmpp.on("error", (err) => {
        console.error(err)
      })

      this.xmpp.on("offline", () => {
        console.log("offline")
      })

      // handle incoming messages

      this.xmpp.on("stanza", async (stanza) => {
        // ignore IQs, since they're handled separately
        if (stanza.is("iq")) return

        // ignore own messages
        var from = stanza.attrs.from ? jid(stanza.attrs.from) : undefined
        if (
          from &&
          from == this.domain
        ) {
          console.log(`ignoring own message: ${stanza}`)
          return
        }

        console.log(`got stanza: ${stanza}`)

        // incoming presence (implicit)
        if (stanza.is("presence")) {
          // don't respond to presences to main domain
          if (stanza.attrs.to == this.domain) return

          // handle "unavailable" stanzas: tableLeave
          if (stanza.attrs.type == 'unavailable') {
            // check if it's a permanent leaving
            let permanent = false
            if (
              stanza.getChildren("game").length &&
              stanza.getChild("game").getChildren("item").length &&
              stanza.getChild("game").getChild("item").attrs.affiliation == 'none'
            ) {
              permanent = true
            }

            if (permanent) {
              this.emit('tableExit', {
                table: stanza.attrs.to,
                user: stanza.attrs.from,
              })
            } else {
              this.emit('tableLeave', {
                table: stanza.attrs.to,
                user: stanza.attrs.from,
              })
            }
            return
          }

          // parameters
          var role = 'kibitzer'

          // check for additional details
          let game = stanza.getChildren("game")
          if (game.length) {

            // check that we want to play hool
            if (game[0].attrs.var != 'http://hool.org/protocol/mug/hool') {
              console.log(`Ignoring stanza: ${stanza}`)
              return // not supported for this server!
            }

            // get values
            let options = game[0].getChildren('item')
            if (options) {
              console.log(`selected role: ${options[0].attrs.role}`)
              // check if side is selected
              if (
                (['N', 'E', 'S', 'W']
                    .some(side => options[0].attrs.role == side))
              ) {
                role = options[0].attrs.role
              } else if (options[0].attrs.role == 'any') {
                role = 'any'
              } else if (options[0].attrs.role == 'currentPresence') {
                role = 'currentPresence'
              }
              // otherwise, assume kibitzer
            }
          }

          this.emit('tableJoin', {
            table: stanza.attrs.to,
            user: stanza.attrs.from,
            role: role,
          })

        }

        // incoming message
        if (stanza.is("message")) {

          // handle turns (moves made by user)
          if (stanza.getChildren("turn").length) {
            let turn = stanza.getChild("turn")

            // infoshare
            let infoshare = turn.getChild("infoshare")
            if (
              infoshare &&
              infoshare.attrs.xmlns == 'http://hool.org/protocol/mug/hool'
            ) {
              // TODO: reject malformed stanza
              this.emit('infoshare', {
                table: stanza.attrs.to,
                user: stanza.attrs.from,
                side: infoshare.attrs.side,
                type: infoshare.attrs.type,
                value: infoshare.attrs.value,
              })
            }

            // bid
            let bid = turn.getChild("bid")
            if (
              bid &&
              bid.attrs.xmlns == 'http://hool.org/protocol/mug/hool'
            ) {
              // TODO: reject malformed stanza
              this.emit('bid', {
                table: stanza.attrs.to,
                user: stanza.attrs.from,
                side: bid.attrs.side,
                type: bid.attrs.type,
                level: bid.attrs.level,
                suit: bid.attrs.suit,
              })
            }

            // card play
            let cardplay = turn.getChild("cardplay")
            if (
              cardplay &&
              cardplay.attrs.xmlns == 'http://hool.org/protocol/mug/hool'
              ) {
                this.emit('cardplay', {
                table: stanza.attrs.to,
                user: stanza.attrs.from,
                side: cardplay.attrs.side,
                rank: cardplay.attrs.rank,
                suit: cardplay.attrs.suit,
              })
            }

            // undo request
            let undo = turn.getChild("undo")
            if (
              undo &&
              undo.attrs.xmlns == 'http://hool.org/protocol/mug/hool'
              ) {
                this.emit('undo', {
                table: stanza.attrs.to,
                user: stanza.attrs.from,
                mode: undo.attrs.mode,
                side: undo.attrs.side,
                rank: undo.attrs.rank,
                suit: undo.attrs.suit,
              })
            }

            //claim Request
            let claim = turn.getChild("claim")
            if (
              claim &&
              claim.attrs.xmlns == 'http://hool.org/protocol/mug/hool'
              ) {
                this.emit('claim', {
                table: stanza.attrs.to,
                user: stanza.attrs.from,
                mode: claim.attrs.mode,
                side: claim.attrs.side,
                amount: claim.attrs.amount,
              })
            }

            // kickoutuser
            let kickoutuser = turn.getChild("kickoutuser")
            if (
              kickoutuser &&
              kickoutuser.attrs.xmlns == 'http://hool.org/protocol/mug/hool'
            ) {
              // TODO: reject malformed stanza
              this.emit('kickoutuser', {
                table: stanza.attrs.to,
                user: stanza.attrs.from,
                kickuser: kickoutuser.attrs.kickuser,
              })
            }

            // scoreboard request
            let scoreboard = turn.getChild("scoreboard")
            if (
              scoreboard &&
              scoreboard.attrs.xmlns == 'http://hool.org/protocol/mug/hool'
              ) {
                this.emit('scoreboard', {
                table: stanza.attrs.to,
                user: stanza.attrs.from,
                mode: scoreboard.attrs.mode,
                side: scoreboard.attrs.side,
              })
            }

            // catchup request
            let catchup = turn.getChild("catchup")
            if (
              catchup &&
              catchup.attrs.xmlns == 'http://hool.org/protocol/mug/hool'
              ) {
                this.emit('catchup', {
                table: stanza.attrs.to,
                user: stanza.attrs.from,
                mode: catchup.attrs.mode,
                side: catchup.attrs.side,
              })
            }

            // change deal
            let changedeal = turn.getChild("changedeal");
            if (
              changedeal &&
              changedeal.attrs.xmlns == "http://hool.org/protocol/mug/hool"
            ) {
              // TODO: reject malformed stanza
              this.emit("changedeal", {
                table: stanza.attrs.to,
                user: stanza.attrs.from,
                type: changedeal.attrs.type,
                side: changedeal.attrs.side,
              });
            }
        }
      }
   })

      this.xmpp.on("online", async (address) => {
        // Makes itself available
        console.log("online as", address.toString())

        // send chat message to self
        await this.xmpp.send((
          <message type="chat" to={address}>
            <body>hello world!</body>
          </message>
        ))
      })

      // handle incoming IQs

      // express support for the hool game
      this.xmpp.iqCallee.get("http://jabber.org/protocol/disco#info",
        "query",
        async (ctx) => {
          // check if it's the root list
          if (ctx.stanza.attrs.to == this.domain) {
            console.log(`returning room list`)
            return (
              <query xmlns="http://jabber.org/protocol/disco#info">
                <identity
                  category="game"
                  name={this.componentTitle}
                  type="multi-user"/>
                <feature var="http://jabber.org/protocol/disco#items"/>
                <feature var="jabber:iq:search"/>
                <feature var="jabber:iq:register"/>
                <feature var="http://jabber.org/protocol/mug"/>
                <feature var="http://hool.org/protocol/mug/hool"/>
              </query>
            )
          } else {
            let result = await this.engine.getTableInfo(ctx.stanza.attrs.to)
            if(!result) {  // if no result, return blank response
              return (<query xmlns="http://jabber.org/protocol/disco#items"/>)
            } else {
              // process info
              let playerCount = result.occupants.filter(o => o.role == 'player').length
              let kibitzerCount = result.occupants.filter(o => o.role == 'kibitzer').length
              let hostName = result.occupants.find(o => o.host == true).jid
              let timeCat = result.time || 'none'
              let historyEnabled = result.history == false ? 'off' : 'on'
              let cheatsheetEnabled = result.cheatSheet == false ? 'off' : 'on'
              let variant = 'hool' // TODO: update when two-player version happens

              // return info
              return(
                <query xmlns="http://jabber.org/protocol/disco#items">
                  <feature var="http://jabber.org/protocol/disco#info"/>
                  <feature var="http://jabber.org/protocol/mug"/>
                  <feature var="http://hool.org/protocol/mug/hool"/>
                  <x xmlns="jabber:x:data" type="result">
                    <field var="FORM_TYPE" type="hidden">
                      <value>http://jabber.org/protocol/mug#matchinfo</value>
                    </field>
                    <field var="mug#match_description" label="Description">
                      <value>{result.name}</value>
                    </field>
                    <field var="hool#host" type="jid" label="Host">
                      <value>{hostName}</value>
                    </field>
                    <field var="hool#variant" label="Variant">
                      <value>{variant}</value>
                    </field>
                    <field var="hool#time" label="Time">
                      <value>{timeCat}</value>
                    </field>
                    <field var="hool#player_count" label="Players">
                      <value>{playerCount}</value>
                    </field>
                    <field var="hool#kibitzer_count" label="Kibitzers">
                      <value>{kibitzerCount}</value>
                    </field>
                    <field var="hool#history_switch" label="History">
                      <value>{historyEnabled}</value>
                    </field>
                    <field var="hool#cheatsheet_switch" label="Cheat Sheet">
                      <value>{cheatsheetEnabled}</value>
                    </field>
                  </x>
                </query>
              )
            }
          }
        }
      )

      // list active public tables
      this.xmpp.iqCallee.get("http://jabber.org/protocol/disco#items",
        "query",
        async (ctx) => {
          // check if it's the root list
          if (ctx.stanza.attrs.to == this.domain) {
            console.log("listing public tables")
            let results = await this.engine.getTableList()
              return (
                <query xmlns="http://jabber.org/protocol/disco#items">
                  {
                    results.map((table)=>{
                      let playerCount = table.occupants.filter(o => o.role == 'player').length
                      let kibitzerCount = table.occupants.filter(o => o.role == 'kibitzer').length
                      let hostName = table.occupants.find(o => o.host == true).jid
                      let timeCat = table.time || 'none'
                      let historyEnabled = table.history == false ? 'off' : 'on'
                      let cheatsheetEnabled = table.cheatSheet == false ? 'off' : 'on'
                      let variant = 'hool' // TODO: update when two-player version happens
                      return xml('item', {
                        jid: table.table_id,
                        name: table.name,
                        playerCount,
                        kibitzerCount,
                        hostName,
                        timeCat,
                        historyEnabled,
                        cheatsheetEnabled,
                        variant
                      })
                    })
                  }
                </query>
              )
          } else {
            // TODO: list stuff for that particular room
            return (
              <query xmlns="http://jabber.org/protocol/disco#items" />
            )
          }


        }
      )

      // return search form
      this.xmpp.iqCallee.get("jabber:iq:search",
        "query",
        (ctx) => {
          console.log("returning search form")
          return (
            <query xmlns="jabber:iq:search">
              <instructions>
                Hi there, please enter what you want to search for
              </instructions>
              <x xmlns="jabber:x:data" type="form">
                <title>Table Search</title>
                <field type="hidden" var="FORM_TYPE">
                  <value>jabber:iq:search</value>
                </field>
                <field type="text-single"
                  label="Table Name"
                  var="mug#roomsearch_name"/>
                <field type="hidden" var="mug#roomsearch_game">
                  <value>http://hool.org/protocol/mug#hool</value>
                </field>
              </x>
            </query>
          )
        }
      )

      // process search form
      this.xmpp.iqCallee.set("jabber:iq:search",
        "query",
        (ctx) => {
          if (ctx.stanza.children) {
            // return results
            let x = ctx.stanza.getChild("query").getChild("x")
            if (x && x.attrs.xmlns == "jabber:x:data") {
              // define params
              var params = {}

              // insert values into params
              var fields = x.getChildren("field")
              for (let i=0;i<fields.length;i++) {
                params[fields[i].attrs.var] = fields[i].getChildText("value")
              }

              // validate form type and filters
              if (params['FORM_TYPE'] != 'jabber:iq:search') return
              if (params['mug#roomsearch_game'] != 'http://hool.org/protocol/mug#hool') {
                return (<query xmlns="jabber:iq:search"/>) // no non-hool results
              }

              this.engine.getTableList().then(results => {
                return (
                  <query xmlns="jabber:iq:search">
                    <x xmlns="jabber:x:data" type="result">
                      <field type="hidden" var="FORM_TYPE">
                        <value>{params['FORM_TYPE']}</value>
                      </field>
                      <reported>
                        <field var="time" label="Time" type="text-single"/>
                        <field var="host" label="Host" type="jid-single"/>
                        <field var="players" label="Players" type="text-single"/>
                        <field var="history" label="History" type="boolean"/>
                        <field var="kibbitzers" label="Kibbitzers" type="boolean"/>
                        <field var="jid" label="JID" type="jid-single"/>
                      </reported>
                      {
                        results.map((table)=>{
                          return (
                            <item>
                              <field var="jid"><value>{table.table_id}</value></field>
                              <field var="time"><value>{table.time}</value></field>
                              <field var="host"><value>{table.host}</value></field>
                              <field var="players"><value>{table.players}</value></field>
                              <field var="history"><value>{table.history}</value></field>
                              <field var="kibbitzers"><value>{table.kibbitzers}</value></field>
                            </item>
                          )
                        })
                      }
                    </x>
                  </query>
                )
              })
            }
          }
        }
      )


      // return registration form
      this.xmpp.iqCallee.get("jabber:iq:register",
        "query",
        async (iq) => {
          let userInfo = await this.engine.getUserInfo(iq.from)
          if (userInfo) {
            return (
              <query xmlns="jabber:iq:register">
                <registered/>
                <x xmlns="jabber:x:data" type="result">
                  <field type="hidden" var="FORM_TYPE">
                    <value>jabber:iq:register</value>
                  </field>
                  <field type="text-single" label="Username" var="username">
                    <value>{userInfo.jid.local}</value>
                  </field>
                  <field type="text-private" label="Email" var="email" />
                  <field type="text-single" label="Location" var="location">
                    <value>{userInfo.location}</value>
                  </field>
                  <field type="text-single" label="Name" var="name">
                    <value>{userInfo.name}</value>
                  </field>
                  <field type="text-single" label="Account Status" var="status">
                    <value>{userInfo.status}</value>
                  </field>
                </x>
              </query>
            )
          } else {
            return(
              <query xmlns="jabber:iq:register">
                <instructions>
                  Use the enclosed form to register. If your Jabber client
                  does not support Data Forms, visit the Hool website to
                  register via your web browser.
                </instructions>
                <x xmlns="jabber:x:data" type="form">
                  <title>Hool Registration</title>
                  <instructions>
                    Please enter your registration details. Your email
                    will have to be verified before you can start using
                    this service.
                  </instructions>
                  <field type="hidden" var="FORM_TYPE">
                    <value>jabber:iq:register</value>
                  </field>
                  <field type="text-single" label="Username" var="username">
                    <required />
                  </field>
                  <field type="text-single" label="Email" var="email">
                    <required />
                  </field>
                  <field type="text-single" label="Name" var="name" />
                  <field type="text-single" label="Location" var="location" />
                </x>
              </query>
            )
          }
        }
      )

      // process registration form
      this.xmpp.iqCallee.set("jabber:iq:register",
        "query",
        async (iq) => {
          if (iq.stanza.attrs.to == this.domain) {
            if (
              iq.stanza.getChildren("query").length &&
              iq.stanza.getChild("query").attrs.xmlns == "jabber:iq:register" &&
              iq.stanza.getChild("query").getChildren("x").length
            ) {
              // handle account creation
              let xData = iq.stanza.getChild("query").getChild("x")

              let registration = {
                type: 'register',
                from: iq.from,
              }

              for (let field of xData.getChildren("field")) {
                if (field.getChildren("value").length) {
                  registration[field.attrs.var] = field.getChild("value").text()
                }
              }

              return await this.renderRegistration(
                await this.engine.handleRegistration(registration))
            }
          }
        }
      )

      // process room creation
      this.xmpp.iqCallee.set("http://jabber.org/protocol/mug#owner",
        "query",
        (ctx) => {
          console.log(ctx)
          // stanza will be directed to a certain room
          if (iq.stanza.attrs.to != this.domain) {
            let table_id = jid(iq.stanza.attrs.to)

            // check if user wants form returned
            if (
              iq.stanza.getChild("query") &&
              iq.stanza.getChild("query").getChild("options") &&
              iq.stanza.getChild("query").getChild("options").children.length == 0
              ) {
                return(
                  <query xmlns="http://jabber.org/protocol/mug#owner">
                    <options>
                      <x xmlns="jabber:x:data" type="form">
                        <title>Configuration for  Hool room</title>
                        <instructions>
                          Your room {table_id} has been created!
                          The default configuration is as follows:
                            - Unlisted: no
                            - Table lock: unlocked
                            - Kibbitzers: allowed
                            - Time limit: off
                            - Scoring type: basic
                          To accept the default configuration, click OK. To
                          select a different configuration option, please
                          complete this form.
                        </instructions>
                        <field type="hidden" var="FORM_TYPE">
                          <value>http://jabber.org/protocol/mug#matchconfig</value>
                        </field>
                        <field
                          label="Include this room in the public lists?"
                          type="boolean"
                          var="mug#roomconfig_publicroom">
                          <value>1</value>
                        </field>
                      </x>
                      <options xmlns="http://hool.org/protocol/mug/hool">
                        <x xmlns="jabber:x:data" type="form">
                          <title>Game settings</title>
                          <field var="FORM_TYPE">
                            <val>http://hool.org/protocol/mug/hool#hoolconfig</val>
                          </field>
                          <field label="Lock table (must be invited to sit)"
                            type="boolean"
                            var="mug/hool#config_tablelock">
                            <value>0</value>
                          </field>
                          <field label="Lock kibbitzers (must be invited to sit)"
                            type="boolean"
                            var="mug/hool#config_kibbitzerlock">
                            <value>0</value>
                          </field>
                          <field label="Lock kibbitzers (must be invited to sit)"
                            type="boolean"
                            var="mug/hool#config_scoringtype">
                            <value>basic</value>
                            <option label="Basic scoring"><value>basic</value></option>
                          </field>
                        </x>
                      </options>
                    </options>
                  </query>
                )
            }

          }
        }
      )
    }

    start() {
      if (process.env.NODE_ENV == 'development') {
        debug(xmpp, true)
      }

      return this.xmpp.start()
    }

    stop() {
      return this.xmpp.stop()
    }

    /**
     * Template rendering functions: these take in a standard
     * JavaScript response/event object and convert it into the
     * corresponding XMPP-compatible XML.
     *
     * Note that this DOES NOT send any XMPP messages; it merely
     * returns the data for further use.
     */

    renderRegistration(registration) {
      if (!!registration.error) {
        if (!registration.error.type) throw 'Please specify error type'
        if (!registration.error.tag) throw 'Please specify error tag'
  
        // calculate error code if necessary
        if (!registration.error.code) {
          if (registration.error.type == 'cancel') {
            registration.error.code = '409'
          } else if (registration.error.type == 'modify') {
            registration.error.code = '406'
          } else {
            throw 'Error code could not be computed. Please specify it manually'
          }
        }
      }

      if (!!registration.error) {
        /* Note: query stanza will be prepended automatically.
         *
         * The XMPP standard requires messages to be returned in the
         * followig form when there is an error:
         *
         * <iq ...>
         *   <query ...>
         *     <.../>
         *   </query>
         *   <error ...>
         *     <.../>
         *   </error>
         *  </iq>
         *
         * where the enclosing <iq/> tag is added automatically. But
         * when we try to add two tags (<query/> and <error/>) side
         * by side, it doesn't work because iq expects only one tag,
         * not a list of tags. This needs to be fixed in the way
         * xmpp.js handles iq, as well as how it parses XML in general.
         * (Support for React's <> tag doesn't seem to be there, which
         * is what would make this operation so much simpler.
         *
         * Anyway, upon testing it seems that we can send *only* the
         * <error/> tag and the <query/> tag will be automatically
         * added on top! How cool is that?
         *
         * (Needs to be better documented though; it took a day of
         * searching before this exceedingly simple solution was
         * stumbled upon, to wit: don't worry about it and things will
         * be taken care of for you).
         */

        return(
          <error type={registration.error.type}>
            {xml(registration.error.tag, 'urn:ietf:params:xml:ns:xmpp-stanzas')}
            <text>{registration.error.text}</text>
          </error>
        )
      } else {
        return(
          <query xmlns="jabber:iq:register">
           { !!registration.registered ?
             <registered/>
           : null }
           <x xmlns="jabber:x:data" type="result">
             <field type="hidden" var="FORM_TYPE">
               <value>jabber:iq:register</value>
             </field>
             <field type="text-single" label="Username" var="username">
               <value>{registration.username}</value>
             </field>
             <field type="text-single" label="Email" var="email">
               <value>{registration.email}</value>
             </field>
             { !!registration.location ?
               <field type="text-single" label="Location" var="location">
                 <value>{registration.location.location}</value>
               </field>
             : null }
             { !!registration.status ?
               <field type="text-single" label="Account Status" var="status">
                 <value>{userInfo.status}</value>
               </field>
             : null }
           </x>
         </query>
        )
      }
    }

    /**
     * Listen and respond to incoming events from the Engine
     */

    listen(engine) {
      this.engine = engine

      this.engine.on('presence', (presence) => {
        if (!presence.to) throw 'Please specify message recipient'
        if (!presence.user) throw 'Please specify presence user'
        if (presence.role == 'kibitzer') presence.role = 'none'

        if (presence.table) {
          // TODO calculate local nickname more intelligently
          let user_jid = jid(presence.user)
          presence.from = `${presence.table}/${user_jid.local}`

          // Handle error presences
          if (presence.error) {
            if (!presence.error.tag) throw 'Please specify error tag'
            if (!presence.error.type) presence.error.type = 'cancel'
            this.xmpp.send(
              <presence
                from={presence.from}
                to={presence.to}
                type='error'>
                <game xmlns='http://jabber.org/protocol/mug'/>
                <error type={presence.error.type}>
                  {xml(presence.error.tag, 'urn:ietf:params:xml:ns:xmpp-stanzas')}
                  <text>{presence.error.text}</text>
                </error>
              </presence>
            )

            return
          }

          // handle "exit" presences
          if (presence.type == 'exit') {
            presence.type = 'unavailable'
            presence.affiliation = 'none'
            // now, ends up being processed as a "leaving" presence
          }

          // handle "leaving" presences
          if (presence.type == 'unavailable') {
            this.xmpp.send(
              <presence
                from={presence.from}
                to={presence.to}
                type='unavailable'>
                <game xmlns='http://jabber.org/protocol/mug'>
                  <item
                    affiliation={presence.affiliation || 'member'}
                    role='none'
                    jid={user_jid}
                  />
                </game>
              </presence>
            )

            return
          }

          this.xmpp.send(
            <presence
              from={presence.from}
              to={presence.to}>
              <game xmlns='http://jabber.org/protocol/mug'>
                <item
                  affiliation={presence.affiliation || 'member'}
                  role={presence.role || 'none'}
                  jid={user_jid}
                />
              </game>
            </presence>
          )
          return
        }
      })

      /**
       * Status update
       *
       * Updates the user on the state of the game (eg. cards dealt,
       * current position, etc).
      */

      this.engine.on('state', (presence) => {
        if (!presence.to) throw 'Please specify message recipient'
        if (!presence.table) throw Error('Please specify a table to send from!')

        this.xmpp.send(
          <presence
            from={presence.table}
            to={presence.to}>
            { presence.status ? <status>{presence.status}</status> : null }
            <game xmlns='http://jabber.org/protocol/mug'>
              <state xmlns='http://hool.org/protocol/mug/hool'>
                <x xmlns='jabber:x:data' type='submit'>
                  { presence.hands ?
                    <hands>
                      {presence.hands.map((hand) => (
                        <hand side={hand.side}>
                          <cards>
                          {hand.cards ? hand.cards.map((card) => (
                            <card suit={card.suit} rank={card.rank}/>
                          )): null}
                          </cards>
                          <infoshared>
                          {hand.infoshared ? hand.infoshared.map((info) => (
                            <infoshare type={info.infoType} value={info.infoValue}/>
                          )) : null}
                          </infoshared>
                        </hand>
                      ))}
                    </hands>
                    : null }

                  {/*
                   * FIXME: use <bids> stanza instead of <bidinfo>
                   *
                   * The format should be something like:
                   *
                   *   <bids>
                   *     <round number=1 winner={side}>
                   *       <bid side={} type={} level={} won={}/>
                   *       ...
                   *     </round>
                   *     <round number=2>
                   *       ...
                   *     </round>
                   *     ...
                   *   </bids>
                   *
                   */}
                  { presence.bids ?
                    <bidinfo>
                      {presence.bids.map((bid) => (
                        <bid
                          side={bid.side}
                          type={bid.type}
                          level={bid.level}
                          suit={bid.suit}
                          round={bid.round ? bid.round : null}
                          won={bid.won ? 'true' : null}/>
                      ))}
                    </bidinfo>
                    : null }
                  { presence.tableinfo ?
                  <tableinfo
                    stage={presence.tableinfo.stage}
                    turn={presence.tableinfo.turn}
                    dealer={presence.tableinfo.dealer}
                    host={presence.tableinfo.host}
                    />
                  : null }
                  {/*
                   * FIXME: use <contract/> stanza instead of
                   * <contractinfo/>. The rest of the format
                   * can remain the same.
                   */}
                  { presence.contract ?
                    <contractinfo
                      level={presence.contract.level}
                      suit={presence.contract.suit}
                      double={presence.contract.double}
                      redouble={presence.contract.redouble}
                      declarer={presence.contract.declarer}
                    />: null }
                  { presence.tricksinfo ?
                    <trickinfo>
                      {presence.tricksinfo.map((trickinfo) => (
                        <trick winner={trickinfo.winner}>
                          {trickinfo.cards.map((card) => (
                            <card
                              side={card.side}
                              rank={card.card.rank}
                              suit={card.card.suit}
                              />
                          ))}
                        </trick>
                      ))}
                    </trickinfo>
                    : null }
                  { presence.score && presence.score.trick ?
                    <score type='trick'>
                      {presence.score.trick.map((score) => (
                        <value side={score.side}>{score.value}</value>
                      ))}
                    </score>
                    : null }
                  { presence.score && presence.score.deal ?
                    <score type='deal'>
                      {presence.score.deal.map((score) => (
                        <value side={score.side}>{score.value}</value>
                      ))}
                    </score>
                    : null }
                    { presence.score && presence.score.running ?
                    <score type='running'>
                      {presence.score.running.map((score) => (
                        <value side={score.side}>{score.value}</value>
                      ))}
                    </score>
                    : null }
                  { presence.turn ? <turn side={presence.turn} /> : null }
                </x>
              </state>
            </game>
          </presence>
        )
      })


      /**
       * Info share
       *
       * This is when a user shares information about they hand
       * (suit count, high card points, or pattern)
      */


      this.engine.on('infoshare', (infoshare) => {
        if (!infoshare.to) throw 'Please specify an info recipient'
        if (!infoshare.table) throw 'Please specify a table'
        if (!infoshare.user) throw 'Please specify a user'
        if (!infoshare.side) throw 'Please specify the playing side'
        if (!infoshare.type) throw 'Please specify the info type'
        // if (!infoshare.error && !infoshare.value) throw 'Please specify the info value'

        // TODO calculate local nickname more intelligently
        let user_jid = jid(infoshare.user)
        infoshare.from = `${infoshare.table}/${user_jid.local}`

        this.xmpp.send(
          <message
            from={infoshare.from}
            to={infoshare.to}
            type='chat'>
            <turn xmlns='http://jabber.org/protocol/mug#user'>
              <infoshare xmlns='http://hool.org/protocol/mug/hool'
                side={infoshare.side}
                type={infoshare.type}
                value={infoshare.value}/>
            </turn>
            {infoshare.error ?
              <error type={ infoshare.error.type || 'cancel'}>
                { infoshare.error.type == 'auth' ?
                  <forbidden xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
                :
                  [
                    <undefined-condition xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>,
                    <invalid-turn xmlns='http://jabber.org/protocol/mug'/>,
                  ]
                }
                { infoshare.error.text ? <text>{ infoshare.error.text }</text> : '' }
              </error>
            :
              ''
            }
          </message>
        )
      })

      /**
       * Bid
       *
       * This  is when a user bids to win a particular number of tricks.
      */
      

      this.engine.on('bid', (bid) => {
        if (!bid.to) throw 'Please specify an info recipient'
        if (!bid.table) throw 'Please specify a table'
        if (!bid.user) throw 'Please specify a user'
        if (!bid.side) throw 'Please specify the playing side'

        /*
         * empty bid type is allowed, to inform a person that
         * someone has bid but not revealing what the bid was
         */
        if (!!bid.type) {

          /*
           * but if the  bid type is specified, we need to
           * make sure all the details are valid.
           */
          if (['level', 'redouble', 'double', 'pass'].indexOf(bid.type) < 0) {
            throw `Invalid bid type: ${bidType}`
          }

          if (bid.type == 'level') {
            if (!bid.level) throw 'Please specify level for level bid'
            if (!bid.suit) throw 'Please specify suit for level bid'
          }
        } else {
          /*
           * and if it's not specified, we clear the level and
           * suit because what does that even mean?
           */
           bid.level = null
           bid.suit = null
        }

        // TODO calculate local nickname more intelligently
        let user_jid = jid(bid.user)
        bid.from = `${bid.table}/${user_jid.local}`

        this.xmpp.send(
          <message
            from={bid.from}
            to={bid.to}
            type='chat'>
            <turn xmlns='http://jabber.org/protocol/mug#user'>
              <bid xmlns='http://hool.org/protocol/mug/hool'
                side={bid.side}
                type={bid.type}
                level={bid.level}
                suit={bid.suit}
                round={bid.round ? bid.round : null}
                won={bid.won ? 'true' : null}/>
            </turn>
            {/*
              * FIXME: handle errors more intelligenttly
              *
              * The XMPP standard specifies the following error tags:
              *
              * - undefined-condition+invalid-turn
              *   (for valid bid not alowed at this time, eg. too high)
              * - auth/forbidden
              *   (if kibitzer tries to play, or player for some other
              *   side)
              * - not-allowed
              *   (if trying to play in wrong stage, eg. bid during
              *   infoshare stage)
              *
              * These errors tags can use similar logic to that used
              * in <presence/> stanzas. Once ready, they should be
              * implemented in all bid functions in engine.jsx and then
              * enforced here (sending a bid error without specifying
              * tag type should throw an exception).
              */}

            {bid.error ?
              <error type={ bid.error.type || 'cancel'}>
                { bid.error.type == 'auth' ?
                  <forbidden xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
                :
                  [
                    <undefined-condition xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>,
                    <invalid-turn xmlns='http://jabber.org/protocol/mug'/>,
                  ]
                }
                { bid.error.text ? <text>{ bid.error.text }</text> : '' }
              </error>
            :
              ''
            }
          </message>
        )
      })

      this.engine.on('kickoutuser', (kickoutuser) => {
        if (!kickoutuser.kickuser) throw 'Please specify an info recipient'
        if (!kickoutuser.table) throw 'Please specify a table'
        
        // TODO calculate local nickname more intelligently
        let user_jid = jid(kickoutuser.user)
        kickoutuser.from = `${kickoutuser.table}/${user_jid.local}`

        this.xmpp.send(
          <message
            from={kickoutuser.from}
            to={kickoutuser.to}
            type='chat'>
            <turn xmlns='http://jabber.org/protocol/mug#user'>
              <kickoutuser xmlns='http://hool.org/protocol/mug/hool'
                kickuser={kickoutuser.kickuser}
                messsage={kickoutuser.message ? kickoutuser.message : null}/>
            </turn>
            {kickoutuser.error ?
              <error type={ kickoutuser.error.type || 'cancel'}>
                { kickoutuser.error.type == 'auth' ?
                  <forbidden xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
                :
                  [
                    <undefined-condition xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>,
                    <invalid-turn xmlns='http://jabber.org/protocol/mug'/>,
                  ]
                }
                { kickoutuser.error.text ? <text>{ kickoutuser.error.text }</text> : '' }
              </error>
            :
              ''
            }
          </message>
        )
      })

      this.engine.on('cardplay', (cardplay) => {
        if (!cardplay.to) throw 'Please specify an info recipient'
        if (!cardplay.table) throw 'Please specify a table'
        if (!cardplay.user) throw 'Please specify a user'
        if (!cardplay.side) throw 'Please specify the playing side'
        if (!cardplay.rank) throw 'Please specify the card rank'
        if (!cardplay.suit) throw 'Please specify the card suit'

        // TODO calculate local nickname more intelligently
        let user_jid = jid(cardplay.user)
        cardplay.from = `${cardplay.table}/${user_jid.local}`

        this.xmpp.send(
          <message
            from={cardplay.from}
            to={cardplay.to}
            type='chat'>
            <turn xmlns='http://jabber.org/protocol/mug#user'>
              <cardplay xmlns='http://hool.org/protocol/mug/hool'
                side={cardplay.side}
                rank={cardplay.rank}
                suit={cardplay.suit}/>
            </turn>
            {cardplay.error ?
              <error type={ cardplay.error.type || 'cancel'}>
                { cardplay.error.type == 'auth' ?
 
                  <forbidden xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
                :
                  [
                    <undefined-condition xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>,
                    <invalid-turn xmlns='http://jabber.org/protocol/mug'/>,
                  ]
                }

                { cardplay.error.text ? <text>{ cardplay.error.text }</text> : '' }
 
              </error>
            :
              ''
            }
          </message>
        )
      })

      this.engine.on('undo', (undo) => {
        if (!undo.to) throw 'Please specify an info recipient'
        if (!undo.table) throw 'Please specify a table'
        if (!undo.user) throw 'Please specify a user'
        if (!undo.side) throw 'Please specify the undo side'
        if (!undo.mode) throw 'Please specify mode of undo'

        // TODO calculate local nickname more intelligently
        let user_jid = jid(undo.user)
        undo.from = `${undo.table}/${user_jid.local}`

        this.xmpp.send(
          <message
            from={undo.from}
            to={undo.to}
            type='chat'>
            <turn xmlns='http://jabber.org/protocol/mug#user'>
              <undo xmlns='http://hool.org/protocol/mug/hool'
                mode={undo.mode}
                side={undo.side?undo.side:null}
                rank={undo.rank?undo.rank:null}
                suit={undo.suit?undo.suit:null}
                status={undo.status?undo.status:null}>
                  {undo.cardsintrick?undo.cardsintrick.map((card) => (
                    <card
                      side={card.side}
                      rank={card.card.rank}
                      suit={card.card.suit}
                      />
                  )):null}
              </undo>
            </turn>
            {undo.error ?
              <error type={ undo.error.type || 'cancel'}>
                { undo.error.type == 'auth' ?
 
                  <forbidden xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
                :
                  [
                    <undefined-condition xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>,
                    <invalid-turn xmlns='http://jabber.org/protocol/mug'/>,
                  ]
                }

                { undo.error.text ? <text>{ undo.error.text }</text> : '' }
 
              </error>
            :
              ''
            }
          </message>
        )
      })

      /**
       * Claim Amount
       * This is when a user wants to claim amount 
      */
       this.engine.on('claim', (claim) => {
        if (!claim.to) throw 'Please specify an info recipient'
        if (!claim.table) throw 'Please specify a table'
        if (!claim.user) throw 'Please specify a user'
        if (!claim.mode) throw 'Please specify mode'
        

        // TODO calculate local nickname more intelligently
        let user_jid = jid(claim.user)
        claim.from = `${claim.table}/${user_jid.local}`

        this.xmpp.send(
          <message
            from={claim.from}
            to={claim.to}
            type='chat'>
            <turn xmlns='http://jabber.org/protocol/mug#user'>
              <claim xmlns='http://hool.org/protocol/mug/hool'
                mode={claim.mode}
                side={claim.side ? claim.side : null}
                amount={claim.amount ? claim.amount : null}
                claimant = {claim.claimant ? claim.claimant : null}
                status={claim.status ? claim.status : null}/>
                {claim.hand?
                <hand side={claim.hand.side}>
                  {hand.cards.map((card) => (
                    <card suit={card.suit} rank={card.rank}/>
                  ))}
                </hand>
                :null}
            </turn>
            {claim.error ?
              <error type={ claim.error.type || 'cancel'}>
                { claim.error.type == 'auth' ?
                  <forbidden xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
                :
                  [
                    <undefined-condition xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>,
                    <invalid-turn xmlns='http://jabber.org/protocol/mug'/>,
                  ]
                }
                { claim.error.text ? <text>{ claim.error.text }</text> : '' }
              </error>
            :
              ''
            }
          </message>
        )
      })

      this.engine.on('scoreboard', (scoreboard) => {
        if (!scoreboard.to) throw 'Please specify an info recipient'
        if (!scoreboard.table) throw 'Please specify a table'
        if (!scoreboard.user) throw 'Please specify a user'
        if (!scoreboard.side) throw 'Please specify the scoreboard side'
        if (!scoreboard.mode) throw 'Please specify mode of scoreboard'

        // TODO calculate local nickname more intelligently
        let user_jid = jid(scoreboard.user)
        scoreboard.from = `${scoreboard.table}/${user_jid.local}`

        this.xmpp.send(
          <message
            from={scoreboard.from}
            to={scoreboard.to}
            type='chat'>
            <turn xmlns='http://jabber.org/protocol/mug#user'>
              <scoreboard xmlns='http://hool.org/protocol/mug/hool'
                mode={scoreboard.mode}
                side={scoreboard.side?scoreboard.side:null}/>
            </turn>
            {scoreboard.error ?
              <error type={ scoreboard.error.type || 'cancel'}>
                { scoreboard.error.type == 'auth' ?
 
                  <forbidden xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
                :
                  [
                    <undefined-condition xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>,
                    <invalid-turn xmlns='http://jabber.org/protocol/mug'/>,
                  ]
                }

                { scoreboard.error.text ? <text>{ scoreboard.error.text }</text> : '' }
 
              </error>
            :
              ''
            }
          </message>
        )
      })


      this.engine.on('catchup', (catchup) => {
        if (!catchup.to) throw 'Please specify an info recipient'
        if (!catchup.table) throw 'Please specify a table'
        if (!catchup.user) throw 'Please specify a user'
        if (!catchup.side) throw 'Please specify the catchup side'
        if (!catchup.mode) throw 'Please specify mode of catchup'

        // TODO calculate local nickname more intelligently
        let user_jid = jid(catchup.user)
        catchup.from = `${catchup.table}/${user_jid.local}`

        this.xmpp.send(
          <message
            from={catchup.from}
            to={catchup.to}
            type='chat'>
            <turn xmlns='http://jabber.org/protocol/mug#user'>
              <catchup xmlns='http://hool.org/protocol/mug/hool'
                mode={catchup.mode}
                side={catchup.side?catchup.side:null}/>
            </turn>
            {catchup.error ?
              <error type={ catchup.error.type || 'cancel'}>
                { catchup.error.type == 'auth' ?
 
                  <forbidden xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
                :
                  [
                    <undefined-condition xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>,
                    <invalid-turn xmlns='http://jabber.org/protocol/mug'/>,
                  ]
                }

                { catchup.error.text ? <text>{ catchup.error.text }</text> : '' }
 
              </error>
            :
              ''
            }
          </message>
        )
      })

      /**
       * Change Deal
       *
       * This  is when a user changes the deal.
       */

      this.engine.on("changeDeal", (changedeal) => {
        if (!changedeal.to) throw "Please specify an info recipient";
        if (!changedeal.table) throw "Please specify a table";

        if (!!changedeal.type) {
          if (["redeal", "replay", "no"].indexOf(changedeal.type) < 0) {
            throw `Invalid deal type: ${changedeal.type}`;
          }
        } else {
          throw `Invalid deal type: ${changedeal.type}`;
        }

        // TODO calculate local nickname more intelligently
        let user_jid = jid(changedeal.user);
        changedeal.from = `${changedeal.table}/${user_jid.local}`;

        this.xmpp.send(
          <message from={changedeal.from} to={changedeal.to} type="chat">
            <turn xmlns="http://jabber.org/protocol/mug#user">
              <changedeal
                xmlns="http://hool.org/protocol/mug/hool"
                type={changedeal.type}
                status={changedeal.status}
                requestee={changedeal.requestee ? changedeal.requestee : null}
                side={changedeal.side ? changedeal.side : null}
              />
            </turn>
            {changedeal.error ? (
              <error type={changedeal.error.type || "cancel"}>
                {changedeal.error.type == "auth" ? (
                  <forbidden xmlns="urn:ietf:params:xml:ns:xmpp-stanzas" />
                ) : (
                  [
                    <undefined-condition xmlns="urn:ietf:params:xml:ns:xmpp-stanzas" />,
                    <invalid-turn xmlns="http://jabber.org/protocol/mug" />,
                  ]
                )}
                {changedeal.error.text ? (
                  <text>{changedeal.error.text}</text>
                ) : (
                  ""
                )}
              </error>
            ) : (
              ""
            )}
          </message>
        );
      });

      this.engine.on('message', (message) => {
        if (!message.to) throw 'Please specify a message recipient'
        if (!message.text) throw 'Please specify the message text'

        this.xmpp.send(xml(
          'message',
          { type: 'chat', to: options.to, id: uuid.v4() },
          xml('body', {}, options.text),
        ))
      })

      
    }

    // temporary bot engine

    async botPlay(table, userJID) {
      // First, get the player
      var bot = table.userToPlayer(`${this.domain}`)
      console.log(table)

      if (bot && bot.hand.length > 0) {
        var botCard = bot.hand[Math.floor(Math.random()*bot.hand.length)]

        console.log(`playing: ${botCard}`)
        var { status, message, error } = await table.playCard(`${this.domain}`, botCard)

        if (error) {
          // TODO: intelligently handle error
          console.log(`bot: ${error}`)
          if (error == 'game-over') return // fail silently
          sendMessage(xmpp, userJID, `Bot got error: ${message}`)
          return
        }

        await table.save()
        sendMessage(xmpp, userJID, `Bot played: ${botCard}`)
      }
    }
}


module.exports = {
  HoolTransmitter,
}
