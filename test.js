const test = require('ava')
const mongoose = require('mongoose')

// load config if present
require('dotenv').config()

const { EventEmitter } = require('events')

const {
  Card,
  Table,
  HoolEngine,
} = require('./dist/engine')

// Helper functions for event-based testing
function wait(nanoseconds) {
  if (!nanoseconds) nanoseconds = 1000
  return new Promise(resolve => {
    setTimeout(resolve, nanoseconds)
  })
}

class HoolRunner extends EventEmitter {
  constructor (testFunc, ...args) {
    super()
    this.events = []
    this.test = testFunc
  }

  test() {
    // override this!
  }

  async run() {
    // first, set up Hool engine
    let hool = new HoolEngine()
    hool.setDB(mongoose.connection)
    hool.listen(this)

    hool.on('presence', (p) => {
      p.type = 'presence'
      this.events.push(p)
    })

    hool.on('state', (s) => {
      s.type = 'state'
      this.events.push(s)
    })

    // run our test function
    await this.test()

    // wait for a reasonable amount of time
    await wait(1000)

    return this.events
  }
}

/**
 * Runs a function within the Hool Engine and collects the outputted
 * events for further analysis.
 */
async function hoolRun(fn) {
  let r = new HoolRunner(fn)
  let events = r.run()
  return events
}

test.before('set up database', async t => {
  let mongoUrl = (
    process.env.TABLEBOT_MONGODB_TEST_URL ||
    process.env.TABLEBOT_MONGODB_URL
  )
  await mongoose.connect(mongoUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
})

test('foo ', t => {
  t.pass()
})

/**
 * Card object tests
 *
 * Check that the card object validates properly and that
 * cards can be sorted and compared with each other, both
 * as a separate object and in the database.
 */

test('card object works', t => {
  let c = new Card('C2')
  t.is(c.value, 'C2')
  t.is(c.suit, 'C')
  t.is(c.rank, '2')
})

test('card object allows non-integer ranks', t => {
  let c = new Card('CK')
  t.is(c.rank, 'K')
})

test('card object allows multi-character ranks', t => {
  let c = new Card('C10')
  t.is(c.rank, '10')
})

test('card object is case-insensitive', t => {
  let c = new Card('c2')
  t.is(c.suit, 'C')
})

test('card rejects invalid values', t =>{
  t.throws(() => {
    let c = new Card('X2')
  }, {instanceOf: TypeError})
})

test('card rejects malformed values', t =>{
  t.throws(() => {
    let c = new Card('2C') // should've been C2
  }, {instanceOf: TypeError})
})

test('deck creation', t => {
  let deck = Card.makeDeck()

  t.is(deck.length, 52)
  t.is(deck[0].suit, 'C')
  t.is(deck[0].rank, '2')
})

test('sorting cards', t => {
  let hand = [
    new Card('H4'),
    new Card('D4'),
    new Card('C2'),
    new Card('C3'),
  ]

  let sortedHand = Card.unshuffle(hand)

  t.is(sortedHand[0].suit, 'C')
  t.is(sortedHand[0].rank, '2')
  t.is(sortedHand[1].suit, 'C')
  t.is(sortedHand[1].rank, '3')
  t.is(sortedHand[2].suit, 'D')
  t.is(sortedHand[3].suit, 'H')
})

test('table creation and deletion', async t => {
  table = await Table.createNewTable('creation@hool.test')
  await table.save()

  fetchedTable = await Table.findOne({ table_id: 'creation@hool.test' })
  t.is(fetchedTable.table_id, 'creation@hool.test')

  await Table.deleteOne({ _id: fetchedTable._id })
})

test('card works in schema too', async t=> {
  table = await Table.createNewTable('cardschema@hool.test')

  // first, let's add some cards
  let east = table.seats.get('E')

  east.cards.push(new Card('C2'))
  east.cards.push(new Card('D4'))
  east.cards.push('HK')

  table.seats.set('E', east)

  await table.save()

  // now, fetch and check it
  let fetchedTable = await Table.findOne({
    table_id: 'cardschema@hool.test'
  })

  let newEast = fetchedTable.seats.get('E')

  t.is(newEast.cards[0].constructor, Card)
  t.is(newEast.cards[1].rank, '4')
  t.is(newEast.cards[2].suit, 'H')
})

test('dealing cards to players', async t => {
  let table = await Table.createNewTable('letsdeal@hool.test')
  table.dealCards()

  t.is(table.seats.get('N').cards.length, 13)
  t.is(table.seats.get('E').cards.length, 13)
  t.is(table.seats.get('S').cards.length, 13)
  t.is(table.seats.get('W').cards.length, 13)
})

/**
 * Table occupants tests
 *
 * Check if people can leave and join tables properly,
 * create new tables if necessary, and shift from one
 * seat to another (given the requisite permissions).
 */

test('adding user to occupants list', async t => {
  table = await Table.createNewTable('occupants@hool.test')
  table.occupants.push({
    jid: 'avaone@hool.test',
    role: 'kibitzer',
    seat: null,
    status: 'active',
    host: true,
  })
  await table.save()

  fetchedTable = await Table.findOne({ table_id: 'occupants@hool.test' })
  t.is(fetchedTable.occupants.length, 1)
  t.is(fetchedTable.occupants[0].jid, 'avaone@hool.test')

  await Table.deleteOne({ _id: fetchedTable._id })
})

test('changeSeat: moving occupant to seat', async t => {
  table = await Table.createNewTable('sitting@hool.test')
  table.occupants.push({
    jid: 'avatwo@hool.test',
    role: 'kibitzer',
    seat: null,
    status: 'active',
    host: true,
  })
  table.changeSeat(0, 'E')

  t.is(table.occupants[0].role, 'player')
  t.is(table.seats.get('E').user, 'avatwo@hool.test')
})

test('changeSeat: disallow sitting in an occupied seat', async t => {
  table = await Table.createNewTable('sitting@hool.test')
  table.occupants.push({
    jid: 'avathree@hool.test',
    role: 'kibitzer',
    seat: null,
    status: 'active',
    host: true,
  })
  table.occupants.push({
    jid: 'avafour@hool.test',
    role: 'kibitzer',
    seat: null,
    status: 'active',
    host: false,
  })
  table.changeSeat(0, 'E')

  t.throws(() => {
    table.changeSeat(1, 'E')
  }, {instanceOf: Error})
})

test('changeSeat: leaving a seat to kibitz', async t => {
  table = await Table.createNewTable('sitting@hool.test')
  table.occupants.push({
    jid: 'avafive@hool.test',
    role: 'kibitzer',
    seat: null,
    status: 'active',
    host: true,
  })
  table.changeSeat(0, 'N')
  table.changeSeat(0, null)

  t.is(table.seats.get('N').user, null)
  t.is(table.occupants[0].seat, null)
  t.is(table.occupants[0].role, 'kibitzer')
})

test('getPartner: check that correct partner is chosen', async t => {
  table = await Table.createNewTable('partner@hool.test')
  t.is(table.getPartner('N'), 'S')
})

/**
 * Handler test
 *
 * This is to test out the handler functions (such
 * as `bidHandler`) directly, without having to jump
 * through hoops to create an EventRunner and collect
 * events from it.
 */
test('handler test', async t => {
  let hool = new HoolEngine({db: mongoose.connection})

  let events = await hool.handleTableJoin({
    table: 'handlers@tables.hool.test',
    user: 'handlerone@hool.test',
    role: 'kibitzer',
  })

  t.is(events.length, 1)
  t.is(events[0].table, 'handlers@tables.hool.test')

  await Table.deleteOne({ table_id: 'handlerone@hool.test' })
})

test('handler: multiple rapid events', async t => {
  let hool = new HoolEngine({db: mongoose.connection})

  for (let u of [...Array(50).keys()].slice()) {
    await hool.handleTableJoin({
      table: 'kibitzersonly@tables.hool.test',
      user: `kib${u}@hool.test`,
      role: 'kibitzer',
    })
  }

  let table = await Table.findOne({
    table_id: 'kibitzersonly@tables.hool.test'
  })

  t.is(table.occupants.length, 50)
})

/**
 * Event runner test
 *
 * This is to test the now obsolete event runner.
 * This event runner is going to be removed soon so
 * please don't use it!
 */

test('event runner test', async t => {
  // set up our event collector
  let events = await hoolRun(function() {
    this.emit('tableJoin', {
        table: 'eventrunner@tables.hool.test',
        user: 'avasix@hool.test',
        role: 'kibitzer',
      })
  })

  await wait(1000)

  t.is(events.length, 1)
  t.is(events[0].table, 'eventrunner@tables.hool.test')

  await Table.deleteOne({ table_id: 'eventrunner@tables.hool.test' })
})

test('automatically set host', async t => {
  let hool = new HoolEngine({ db: mongoose.connection })

  // let some people join
  await hool.handleTableJoin({
    table: 'autohost@tables.hool.test',
    user: 'avaseven@hool.test',
    role: 'N',
  })

  await hool.handleTableJoin({
    table: 'autohost@tables.hool.test',
    user: 'avaeight@hool.test',
    role: 'S',
  })

  // check table
  let table = await Table.getTableById('autohost@tables.hool.test')

  // get occupants
  let u1 = table.userIndex('avaseven@hool.test')
  let u2 = table.userIndex('avaeight@hool.test')

  // check that host is set properly
  t.is(table.occupants[u1].host, true)
  t.is(table.occupants[u2].host, false)

  await Table.deleteOne({ _id: table._id })
})

test('deny occupation of non-empty seat', async t => {
  let hool = new HoolEngine({ db: mongoose.connection })

  let events = await hool.handleTableJoin({
    table: 'denynonempty@tables.hool.test',
    user: 'avanine@hool.test',
    role: 'N',
  })

  events = events.concat(await hool.handleTableJoin({
    table: 'denynonempty@tables.hool.test',
    user: 'avaten@hool.test',
    role: 'N',
  }))

  // check events
  t.is(!!events[events.length-1].error, true)
  t.is(events[events.length-1].error.tag, 'conflict')

  // check table
  let table = await Table.getTableById('denynonempty@tables.hool.test')

  // get occupants
  let u1 = table.userIndex('avanine@hool.test')
  let u2 = table.userIndex('avaten@hool.test')

  // check that host is set properly
  t.is(table.occupants[u1].seat, 'N')
  t.is(u2, -1)

  await Table.deleteOne({ _id: table._id })
})

test('seat empties on role change', async t => {
  let hool = new HoolEngine({ db: mongoose.connection })

  let events = await hool.handleTableJoin({
    table: 'emptyseat@tables.hool.test',
    user: 'ava11@hool.test',
    role: 'N',
  })

  events = events.concat(await hool.handleTableJoin({
    table: 'emptyseat@tables.hool.test',
    user: 'ava11@hool.test',
    role: 'none',
  }))

  // check events
  t.is(!!events[events.length-1].error, false)
  t.is(events[events.length-1].role, 'kibitzer')

  // check table
  let table = await Table.getTableById('emptyseat@tables.hool.test')

  // get occupant
  let u = table.userIndex('ava11@hool.test')

  // check that seat is empty
  t.is(table.occupants[u].seat, null)
  t.is(table.seats.get('N').user, null)
  t.is(table.occupants[u].role, 'kibitzer')

  await Table.deleteOne({ _id: table._id })
})

test('game starts when four players sit', async t => {
  let hool = new HoolEngine({ db: mongoose.connection })

  let events = await hool.handleTableJoin({
    table: 'table4four@tables.hool.test',
    user: 'ava12@hool.test',
    role: 'N',
  })

  events = events.concat(await hool.handleTableJoin({
    table: 'table4four@tables.hool.test',
    user: 'ava13@hool.test',
    role: 'E',
  }))

  events = events.concat(await hool.handleTableJoin({
    table: 'table4four@tables.hool.test',
    user: 'ava14@hool.test',
    role: 'S',
  }))

  events = events.concat(await hool.handleTableJoin({
    table: 'table4four@tables.hool.test',
    user: 'ava15@hool.test',
    role: 'W',
  }))

  // check events
  t.is(events[events.length-1].event, 'state')
  t.is(events[events.length-1].table, 'table4four@tables.hool.test')
  t.is(events[events.length-1].status, 'active')
  t.is(events[events.length-1].hands.length, 1)
  t.is(events[events.length-1].hands[0].side, 'W')
  t.is(events[events.length-1].hands[0].cards.length, 13)

  // check table
  let table = await Table.getTableById('table4four@tables.hool.test')

  // check that hands are full
  for (let [k,v] of table.seats.entries()) {
    t.is(v.cards.length, 13)
  }

  // check that game has started
  t.is(table.stage, 'infosharing')

  await Table.deleteOne({ _id: table._id })
})

test('kibitzer see cards on game start', async t => {
  let hool = new HoolEngine({ db: mongoose.connection })

  let events = await hool.handleTableJoin({
    table: 'table4kibitzers@tables.hool.test',
    user: 'ava21@hool.test',
    role: 'kibitzer',
  })

  events = events.concat(await hool.handleTableJoin({
    table: 'table4kibitzers@tables.hool.test',
    user: 'ava22@hool.test',
    role: 'any',
  }))

  events = events.concat(await hool.handleTableJoin({
    table: 'table4kibitzers@tables.hool.test',
    user: 'ava23@hool.test',
    role: 'any',
  }))

  events = events.concat(await hool.handleTableJoin({
    table: 'table4kibitzers@tables.hool.test',
    user: 'ava24@hool.test',
    role: 'any',
  }))

  events = events.concat(await hool.handleTableJoin({
    table: 'table4kibitzers@tables.hool.test',
    user: 'ava25@hool.test',
    role: 'any',
  }))

  let stateEvent = events.find(e => (
    e.event == 'state' && e.hands.length == 4
  ))

  t.is(!!stateEvent, true)
  for (let hand of stateEvent.hands) {
    t.is(hand.cards.length, 13)
  }

  await Table.deleteOne({ _id: table._id })
})

test('cards remain if fourth player sits mid-game', async t => {
  let hool = new HoolEngine({ db: mongoose.connection })

  // set up table
  let table = await Table.getTableById('latecomers@tables.hool.test')

  // add users
  table.occupants.push({
    jid: 'ava16@hool.test',
    role: 'kibitzer',
    seat: null,
    status: 'active',
    host: true,
  })

  table.occupants.push({
    jid: 'ava17@hool.test',
    role: 'kibitzer',
    seat: null,
    status: 'active',
    host: false,
  })

  table.occupants.push({
    jid: 'ava18@hool.test',
    role: 'kibitzer',
    seat: null,
    status: 'active',
    host: false,
  })

  table.occupants.push({
    jid: 'ava19@hool.test',
    role: 'kibitzer',
    seat: null,
    status: 'active',
    host: false,
  })

  // set seats
  table.changeSeat(0, 'N')
  table.changeSeat(1, 'E')
  table.changeSeat(2, 'S')

  await table.save()

  // use Hool Engine for last change
  await hool.handleTableJoin({
    table: 'latecomers@tables.hool.test',
    user: 'ava19@hool.test',
    role: 'W',
  })

  // cards should have come by now
  table = await Table.findOne({table_id: 'latecomers@tables.hool.test'})
  t.is(table.seats.get('W').cards.length, 13)
  t.is(table.seats.get('E').cards.length, 13)
  let westFirst = table.seats.get('W').cards[0].value

  // W becomes kibitzer
  await hool.handleTableJoin({
    table: 'latecomers@tables.hool.test',
    user: 'ava19@hool.test',
    role: 'kibitzer',
  })

  // reset table stage
  table = await Table.findOne({_id: table._id})
  table.stage = 'inactive'
  table.save()

  // now, let another player join
  await hool.handleTableJoin({
    table: 'latecomers@tables.hool.test',
    user: 'ava20@hool.test',
    role: 'W',
  })

  // check that hand size is still same
  table = await Table.findOne({_id: table._id})
  t.is(table.seats.get('W').cards[0].value, westFirst) // same as last time?

  // check that hands are full (and not overfull!)
  for (let [k,v] of table.seats.entries()) {
    t.is(v.cards.length, 13)
  }

  await Table.deleteOne({ _id: table._id })
})

/**
 * Infoshare tests
 *
 * Check that the various kinds of shared info are
 * calculated properly.
 */

test('hand info calculation: HCP', t => {
  let hand = [
    new Card('CA'),
    new Card('C2'),
    new Card('C3'),
    new Card('D2'),
    new Card('DK'),
    new Card('DQ'),
    new Card('H3'),
    new Card('H4'),
    new Card('S4'),
    new Card('S10'),
    new Card('SJ'),
    new Card('H2'),
    new Card('H5'),
  ]

  let table = new Table()
  t.is(table.getHandInfo('HCP', hand), 10)
})

test('hand info calculation: pattern', t => {
  let hand = [
    new Card('CA'),
    new Card('C2'),
    new Card('C3'),
    new Card('D2'),
    new Card('DK'),
    new Card('DQ'),
    new Card('H3'),
    new Card('H4'),
    new Card('S4'),
    new Card('S10'),
    new Card('SJ'),
    new Card('H2'),
    new Card('H5'),
  ]

  let pattern = new Table().getHandInfo('pattern', hand)
  t.is(pattern.length, 4)
  t.is(pattern[0], 3)
  t.is(pattern[1], 3)
  t.is(pattern[2], 3)
  t.is(pattern[3], 4)
})

test('hand info calculation: count', t => {
  let hand = [
    new Card('CA'),
    new Card('C2'),
    new Card('C3'),
    new Card('D2'),
    new Card('DK'),
    new Card('DQ'),
    new Card('H3'),
    new Card('H4'),
    new Card('S4'),
    new Card('S10'),
    new Card('SJ'),
    new Card('H2'),
    new Card('H5'),
  ]

  let hearts = new Table().getHandInfo('H', hand)
  t.is(hearts, 4)
})

// TODO: add some handleInfoshare tests to see that sequence is
// followed, no double-shares, stage change when two rounds are
// over, etc.

/**
 * Bidding tests
 *
 * Test that highest bid, winning bid, and final contract
 * are calculated properly.
 */

test('bid winner calculation #1', async t => {
  let table = await Table.getTableById('bidwinner1@tables.hool.test')
  table.bids.push({
    bids: [
      { side: 'N', bidType: 'level', suit: 'S', level: '1' },
      { side: 'E', bidType: 'level', suit: 'C', level: '2' },
      { side: 'S', bidType: 'pass' },
      { side: 'W', bidType: 'level', suit: 'C', level: '2' },
    ],
  })

  let winningBid = table.bids[0].getWinningBid('N')

  t.is(winningBid.side, 'E')
  t.is(winningBid.level, 2)
  t.is(winningBid.suit, 'C')
})

test('bid winner calculation #2', async t => {
  let table = await Table.getTableById('bidwinner2@tables.hool.test')
  table.bids.push({
    bids: [
      { side: 'N', bidType: 'pass' },
      { side: 'E', bidType: 'pass' },
      { side: 'S', bidType: 'pass' },
      { side: 'W', bidType: 'pass' },
    ],
  })

  let winningBid = table.bids[0].getWinningBid('N')

  t.is(winningBid.bidType, 'pass')
  // can't check winner here: not specified
})

test('bid winner calculation #3', async t => {
  let table = await Table.getTableById('bidwinner3@tables.hool.test')
  table.bids.push({
    bids: [
      { side: 'N', bidType: 'pass' },
      { side: 'S', bidType: 'level', suit: 'NT', level: '3' },
    ],
  })

  let winningBid = table.bids[0].getWinningBid('N')

  t.is(winningBid.side, 'S')
  t.is(winningBid.level, 3)
  t.is(winningBid.suit, 'NT')
})

test('final contract calculation #1', async t => {
  let table = await Table.getTableById('finalcontract1@tables.hool.test')
  table.bids.push({ bids: [{ side: 'S', bidType: 'level', level: 1, suit: 'S' }] })
  table.bids.push({ bids: [{ side: 'E', bidType: 'pass'}] })

  let contract = table.getContract()

  t.is(contract.declarer, 'S')
  t.is(contract.level, 1)
  t.is(contract.suit, 'S')
})

test('final contract calculation #2', async t => {
  let table = await Table.getTableById('finalcontract2@tables.hool.test')
  table.bids.push({ bids: [{ side: 'W', bidType: 'level', level: 1, suit: 'S' }] })
  table.bids.push({ bids: [{ side: 'N', bidType: 'level', level: 2, suit: 'H' }] })
  table.bids.push({ bids: [{ side: 'E', bidType: 'double'}] })
  table.bids.push({ bids: [{ side: 'N', bidType: 'pass'}] })

  let contract = table.getContract()

  t.is(contract.declarer, 'N')
  t.is(contract.level, 2)
  t.is(contract.suit, 'H')
  t.is(contract.double, true)
  t.is(contract.redouble, false)

})

test('final contract calculation #3', async t => {
  let table = await Table.getTableById('finalcontract3@tables.hool.test')
  table.bids.push({ bids: [{ side: 'N', bidType: 'level', level: 1, suit: 'NT' }] })
  table.bids.push({ bids: [{ side: 'E', bidType: 'level', level: 3, suit: 'S' }] })
  table.bids.push({ bids: [{ side: 'S', bidType: 'double'}] })
  table.bids.push({ bids: [{ side: 'E', bidType: 'redouble'}] })

  let contract = table.getContract()

  t.is(contract.declarer, 'E')
  t.is(contract.level, 3)
  t.is(contract.suit, 'S')
  t.is(contract.double, true)
  t.is(contract.redouble, true)

})

test('final contract calculation #4', async t => {
  let table = await Table.getTableById('finalcontract4@tables.hool.test')
  table.bids.push({ bids: [{ side: 'E', bidType: 'level', level: 5, suit: 'C' }] })
  table.bids.push({ bids: [{ side: 'S', bidType: 'level', level: 5, suit: 'S' }] })
  table.bids.push({ bids: [{ side: 'W', bidType: 'level', level: 6, suit: 'C' }] })
  table.bids.push({ bids: [{ side: 'N', bidType: 'double'}] })
  table.bids.push({ bids: [{ side: 'W', bidType: 'pass'}] })

  let contract = table.getContract()

  t.is(contract.declarer, 'W')
  t.is(contract.level, 6)
  t.is(contract.suit, 'C')
  t.is(contract.double, true)
  t.is(contract.redouble, false)

})

test('final contract calculation #5', async t => {
  let table = await Table.getTableById('finalcontract5@tables.hool.test')
  table.bids.push({ bids: [{ side: 'N', bidType: 'level', level: 6, suit: 'S' }] })
  table.bids.push({ bids: [{ side: 'E', bidType: 'level', level: 6, suit: 'NT' }] })
  table.bids.push({ bids: [{ side: 'S', bidType: 'level', level: 7, suit: 'S' }] })
  table.bids.push({ bids: [{ side: 'W', bidType: 'double'}] })
  table.bids.push({ bids: [{ side: 'S', bidType: 'redouble'}] })

  let contract = table.getContract()

  t.is(contract.declarer, 'S')
  t.is(contract.level, 7)
  t.is(contract.suit, 'S')
  t.is(contract.double, true)
  t.is(contract.redouble, true)

})

test('final contract calculation #6', async t => {
  let table = await Table.getTableById('finalcontract6@tables.hool.test')
  table.bids.push({ bids: [{ side: 'S', bidType: 'pass'}] })

  let contract = table.getContract()

  t.is(contract.level, undefined)
  t.is(contract.suit, undefined)
  t.is(contract.double, undefined)
  t.is(contract.redouble, undefined)

})

test('bidding: round 1: hidden bids', async t=> {
  /**
   * This is the first of our bid tests, and a simple one: all it does
   * is to make sure that the first bid is broadcast, and that other
   * players can see that a bid was made but not what exactly the bid
   * was.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'starter'
  let table = generateTable('bid-round-1a', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'
  await table.save()

  // let the games begin!
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}S@hool.test`,
    side: 'S',
    type: 'level',
    level: 3,
    suit: 'NT',
  })

  // everyone should have got a bid notification...
  t.is(events.length, 4)

  // ...but, apart from S, they shouldn't know what the bid was
  t.assert(events.every(e => e.to == `${userPrefix}S@hool.test` || !e.type))

  // S should, though, so they can sanity-check
  let s_event = events.find(e => e.to == `${userPrefix}S@hool.test`)
  t.assert(s_event.type, 'level')
  t.assert(s_event.level, '3')
  t.assert(s_event.suit, 'NT')

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 1: bids revealed to kibitzer', async t=> {
  /**
   * From the previous test we know that bids are hidden from players:
   * but are they revealed to the kibitzers? That's what we'll find
   * out this time.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bidder'
  let table = generateTable('bid-round-1b', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'
  await table.save()

  // here comes the kibitzer
  let events = await hool.handleTableJoin({
    table: ourTable,
    user: `${userPrefix}-kib1@hool.test`,
    role: 'kibitzer',
  })

  // let the games begin!
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}S@hool.test`,
    side: 'S',
    type: 'level',
    level: 3,
    suit: 'NT',
  })

  // again, everyone should have got a bid notification...
  t.is(events.length, 5)

  // ...but the kibitzer should also know what the bid was!
  t.assert(events.every(e =>
    e.to == `${userPrefix}S@hool.test` ||
    e.to == `${userPrefix}-kib1@hool.test` ||
    !e.type))

  let kib_event = events.find(e => e.to == `${userPrefix}-kib1@hool.test`)
  t.assert(kib_event.type, 'level')
  t.assert(kib_event.level, '3')
  t.assert(kib_event.suit, 'NT')

  // does S still get the details too? that shouldn't be messed up
  let s_event = events.find(e => e.to == `${userPrefix}S@hool.test`)
  t.assert(s_event.type, 'level')
  t.assert(s_event.level, '3')
  t.assert(s_event.suit, 'NT')

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 1: bids revealed to multiple full kibitzers', async t=> {
  /**
   * Again, we check that bids are hidden from players and revealed to
   * kibitzers—but with multiple kibitzers this time!
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  // this time, we'll use a fresh table to control user join order
  let userPrefix = 'bidder'
  let ourTable = 'bid-round-1c@tables.hool.test'

  // here's who will be in the room...
  players = (['N', 'E', 'S', 'W']
    .map(n => `${userPrefix}${n}@hool.test`))
  kibitzers = (['-kib1', '-kib2', '-kib3', '-kib4']
    .map(n => `${userPrefix}${n}@hool.test`))

  // ... but they'll join in a weird order
  // (now you know why we used a fresh table)
  joinees = [
    { suffix: '-kib1', role: 'kibitzer' },
    { suffix: 'N', role: 'N' },
    { suffix: 'W', role: 'W' },
    { suffix: '-kib2', role: 'kibitzer' },
    { suffix: '-kib3', role: 'kibitzer' },
    { suffix: 'E', role: 'E' },
    { suffix: 'S', role: 'S' },
    { suffix: '-kib4', role: 'kibitzer' },
  ]

  let events, tab

  // wave a magic wand to join them all!
  for (let joinee of joinees) {
    events = await hool.handleTableJoin({
      table: ourTable,
      user: `${userPrefix}${joinee.suffix}@hool.test`,
      role: joinee.role,
    })
  }

  // set the table stage
  // (we couldn't have done it earlier, or they wouldn't
  // have been allowed to join)
  table = await Table.getTableById(ourTable)
  table.stage = 'bidding'
  await table.save()

  // let the games begin!
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}S@hool.test`,
    side: 'S',
    type: 'level',
    level: 3,
    suit: 'NT',
  })

  // as always, everyone should have got a bid notification...
  t.is(events.length, 8)

  // ...but the kibitzers should also know what the bid was!
  t.assert(events.every(e =>
    e.to == `${userPrefix}S@hool.test` ||
    kibitzers.includes(e.to) ||
    !e.type
  ))

  // now check that every kibitzer got the full details
  let kib_events = events.filter(e => kibitzers.includes(e.to))
  kib_events.forEach(e => {
    t.assert(e.type, 'level')
    t.assert(e.level, '3')
    t.assert(e.suit, 'NT')
  })

  // and, of course, we still check for S
  let s_event = events.find(e => e.to == `${userPrefix}S@hool.test`)
  t.assert(s_event.type, 'level')
  t.assert(s_event.level, '3')
  t.assert(s_event.suit, 'NT')

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 1: end of round', async t=> {
  /**
   * Now that we've extensively tested the first bid, I think we can
   * be pretty sure that the subsequent bids will also work. Where
   * things change is at the end of the round, where all bids are
   * revealed. That's what we'll be testing next.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'rounded'
  let table = generateTable('bid-round-1d', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'
  await table.save()

  // let the games begin!
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}S@hool.test`,
    side: 'S',
    type: 'level',
    level: 3,
    suit: 'NT',
  })

  // everyone should have got the bid notification but nothing else
  t.is(events.length, 4)

  // next bid comes in
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}N@hool.test`,
    side: 'N',
    type: 'level',
    level: 2,
    suit: 'H',
  })

  t.is(events.length, 4)

  // and the next
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}E@hool.test`,
    side: 'E',
    type: 'pass',
  })

  t.is(events.length, 4)

  // here comes the final bid!
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'level',
    level: 1,
    suit: 'NT',
  })

  /*
   * if you were wondering why we said "nothing else" earlier,
   * well, here comes the "something else!"
   *
   * this time, everyone should get two events: one saying that the
   * final bid has been played, and then one state update with the
   * full bid round and details of the winner.
   */
  t.is(events.length, 8)

  // first four should be 'bid' events
  t.assert(events.slice(0,4).every(e => e.event == 'bid'))

  // next four should state updates with bid info inside
  t.assert(events.slice(4,8).every(e => e.event == 'state' && !!e.bids))

  // inspect the last event, just to make sure
  let bids = events[events.length-1].bids

  // bids are usually returned serially in the order they happened
  t.is(bids[0].side, 'S')
  t.is(bids[0].level, 3)
  t.assert(bids[0].won)

  t.is(bids[1].side, 'N')
  t.is(bids[1].suit, 'H')

  t.is(bids[2].side, 'E')
  t.is(bids[2].type, 'pass')

  t.is(bids[3].side, 'W')
  t.is(bids[3].suit, 'NT')
  t.assert(!bids[3].won)

  // cool, so that worked!
  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 1: end of round: with kibitzers', async t=> {
  /**
   * Same as the previous test, but with kibitzers included.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'monitored-round-'
  let table = generateTable('bid-round-1e', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'
  await table.save()

  // now wait! let a kibitzer come and join
  await hool.handleTableJoin({
    table: ourTable,
    user: `${userPrefix}kib1@hool.test`,
    role: 'kibitzer',
  })

  // let the games begin!
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}S@hool.test`,
    side: 'S',
    type: 'level',
    level: 3,
    suit: 'NT',
  })

  // everyone should have got the bid notification but nothing else
  t.is(events.length, 5)

  // next bid comes in
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}N@hool.test`,
    side: 'N',
    type: 'level',
    level: 2,
    suit: 'H',
  })

  t.is(events.length, 5)

  /*
   * pause here and check that the kibitzer got a revealed bid
   * (namely, by specifying the bid type in the event) even though
   * the round isn't over
   */
  t.assert(events
    .find(e => e.to==`${userPrefix}kib1@hool.test`)
    .type)

  // and the next
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}E@hool.test`,
    side: 'E',
    type: 'pass',
  })

  t.is(events.length, 5)

  // just checking the kibitzer thing for the third bid also
  t.assert(events
    .find(e => e.to==`${userPrefix}kib1@hool.test`)
    .type)

  // here comes the final bid!
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'level',
    level: 1,
    suit: 'NT',
  })

  /*
   * if you were wondering why we said "nothing else" earlier,
   * well, here comes the "something else!"
   *
   * this time, everyone should get two events: one saying that the
   * final bid has been played, and then one state update with the
   * full bid round and details of the winner.
   */
  t.is(events.length, 10)

  // first four should be 'bid' events
  t.assert(events.slice(0,5).every(e => e.event == 'bid'))

  // next four should state updates with bid info inside
  t.assert(events.slice(5,10).every(e => e.event == 'state' && !!e.bids))

  // inspect the last event, just to make sure
  let bids = events[events.length-1].bids

  // bids are usually returned serially in the order they happened
  t.is(bids[0].side, 'S')
  t.is(bids[0].level, 3)
  t.assert(bids[0].won)

  t.is(bids[1].side, 'N')
  t.is(bids[1].suit, 'H')

  t.is(bids[2].side, 'E')
  t.is(bids[2].type, 'pass')

  t.is(bids[3].side, 'W')
  t.is(bids[3].suit, 'NT')
  t.assert(!bids[3].won)

  // cool, so that worked!
  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 1: all pass', async t=> {
  /**
   * Check that "all pass" first bids are handled by ending the game!
   * When this happens, a scoreboard should be returned immediately.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'pastor'
  let table = generateTable('bid-round-1e', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'
  await table.save()

  // let the games begin!
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}S@hool.test`,
    side: 'S',
    type: 'pass',
  })

  // everyone should have got the bid notification but nothing else
  t.is(events.length, 4)

  // next bid comes in
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}N@hool.test`,
    side: 'N',
    type: 'pass',
  })

  t.is(events.length, 4)

  // and the next
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}E@hool.test`,
    side: 'E',
    type: 'pass',
  })

  t.is(events.length, 4)

  // here comes the final bid!
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'pass',
  })

  /*
   * if you were wondering why we said "nothing else" earlier,
   * well, here comes the "something else!"
   *
   * this time, everyone should get two events:
   * - move saying that the final bid has been played
   * - a state update with the full bid round and details of the
   *   winner, as well as the scoreboard details (these used to
   *   come as two separate state updates but are now bundled into
   *   one).
   */

  t.is(events.length, 8)

  // first four should be 'bid' events
  t.assert(events.slice(0,4).every(e => e.event == 'bid'))

  // next four should be state updates with bid info inside
  t.assert(events.slice(4,8).every(e => e.event == 'state' && !!e.bids))

  // inspect the 5th event, just to make sure
  let bids = events[4].bids

  // bids are usually returned serially in the order they happened
  t.assert(bids.every(b => b.type == 'pass'))

  // these should also have endgame and scoreboard
  t.assert(events.slice(4,8).every(e => e.event == 'state' && !!e.score))

  // inspect the score (should be 0-0)
  let score = events[events.length-1].score

  t.is(score.deal.length, 2)
  t.is(score.running.length, 2)

  t.assert(score.deal.every(s => s.value == 0))
  t.assert(score.running.every(s => s.value == 0))

  // cool, so that worked!
  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 2: no out-of-turn bids', async t => {
  /**
   * Round 2 is when the filtering logic kicks in, because only
   * some specific people are allowed to play. This test checks
   * for out-of-turn bids and makes sure they're blocked with an
   * appropriate error message.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid2-disallowed-'
  let table = generateTable('bid-round-2a', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st rounds of bids, to test the 2nd
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null}
  ]
  await table.save()

  // S won the 1st round, so S should not be allowed to play
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}S@hool.test`,
    side: 'S',
    type: 'pass',
  })

  // only one event should come: the error
  t.is(events.length, 1)
  t.assert(events[0].error)
  // TODO: check error tag, once it's implemented

  // S won the 1st round, so N should not be allowed to play either
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}N@hool.test`,
    side: 'N',
    type: 'pass',
  })

  // only one event should come: the error
  t.is(events.length, 1)
  t.assert(events[0].error)
  // TODO: check error tag, once it's implemented

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 2: no shady bids', async t => {
  /**
   * Round 2 is when the filtering logic kicks in, because only
   * some specific people are allowed to play. This test checks
   * for shady ways for players to circumvent bidding rules:
   * S trying to play as E, whose turn it is, or E, whose turn
   * it is, trying to make a bid for S.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid2-shady-'
  let table = generateTable('bid-round-2b', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st rounds of bids, to test the 2nd
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null}
  ]
  await table.save()

  // S can't play for E even though it's E's turn
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}S@hool.test`,
    side: 'E',
    type: 'pass',
  })

  // E can't play for S even though it's E's turn
  t.is(events.length, 1)
  t.assert(events[0].error)
  // TODO: check error tag, once it's implemented

  // S can't play for E even though we are expecting a bid from E
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}E@hool.test`,
    side: 'S',
    type: 'pass',
  })

  // only one event should come: the error
  t.is(events.length, 1)
  t.assert(events[0].error)
  // TODO: check error tag, once it's implemented

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 2: end of round: pass (end bidding)', async t => {
  /**
   * Confirm that the bidding ends when both defenders pass in the
   * second round.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid2-end-'
  let table = generateTable('bid-round-2c', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st rounds of bids, to test the 2nd
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null}
  ]
  await table.save()

  // W passes
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'pass',
  })

  // we get 4 events: one for each player
  t.is(events.length, 4)

  // actual bid should only be shared to one player (that is, W)
  t.is(events.filter(e => !!e.type).length, 1)

  // E passes
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}E@hool.test`,
    side: 'E',
    type: 'pass',
  })

  // each player gets 2 events: E's bid, and full round
  t.is(events.length, 8)

  // first four should be 'bid' events
  t.assert(events.slice(0,4).every(e => e.event == 'bid'))

  // next four should be state updates with bid info inside
  t.assert(events.slice(4,8).every(e => e.event == 'state' && !!e.bids))

  // check that contract is also passed along with bids
  t.assert(events.slice(4,8).every(e => !!e.contract))

  let contract = events[7].contract
  t.is(contract.declarer, 'S')
  t.is(contract.level, 3)
  t.is(contract.suit, 'NT')


  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 2: end of round: double', async t => {
  /**
   * Let one side double in the second round; this should go
   * through properly.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid2-double-'
  let table = generateTable('bid-round-2d', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st rounds of bids, to test the 2nd
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null}
  ]
  await table.save()

  // W doubles
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'double',
  })

  // we get 4 events: one for each player
  t.is(events.length, 4)

  // actual bid should only be shared to one player (that is, W)
  t.is(events.filter(e => !!e.type).length, 1)

  // E passes
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}E@hool.test`,
    side: 'E',
    type: 'pass',
  })

  // each player gets 2 events: E's bid, and full round
  t.is(events.length, 8)

  // first four should be 'bid' events
  t.assert(events.slice(0,4).every(e => e.event == 'bid'))

  // next four should be state updates with bid info inside
  t.assert(events.slice(4,8).every(e => e.event == 'state' && !!e.bids))

  // take one, for inspection
  let event = events[4]
  t.is(event.bids[0].side, 'W')
  t.assert(event.bids[0].won)
  t.is(event.bids[1].side, 'E')
  t.assert(!event.bids[1].won)


  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 2: end of round: raise', async t => {
  /**
   * Let one side double in the second round; this should go
   * through properly.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid2-raise-'
  let table = generateTable('bid-round-2e', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st rounds of bids, to test the 2nd
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null}
  ]
  await table.save()

  // W raises to 4NT
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'level',
    level: 4,
    suit: 'NT',
  })

  // we get 4 events: one for each player
  t.is(events.length, 4)

  // actual bid should only be shared to one player (that is, W)
  t.is(events.filter(e => !!e.type).length, 1)

  // E doubles
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}E@hool.test`,
    side: 'E',
    type: 'double',
  })

  // each player gets 2 events: E's bid, and full round
  t.is(events.length, 8)

  // first four should be 'bid' events
  t.assert(events.slice(0,4).every(e => e.event == 'bid'))

  // next four should be state updates with bid info inside
  t.assert(events.slice(4,8).every(e => e.event == 'state' && !!e.bids))

  // take one, for inspection
  let event = events[4]

  // first check W's...
  t.is(event.bids[0].side, 'W')
  t.is(event.bids[0].type, 'level')
  t.is(event.bids[0].level, 4)
  t.is(event.bids[0].suit, 'NT')
  t.assert(event.bids[0].won)

  // ...and then check E's
  t.is(event.bids[1].side, 'E')
  t.assert(!event.bids[1].won)

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 2: end of round: illegal raise: too low', async t => {
  /**
   * Let one side double in the second round; this should go
   * through properly.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid2-badraise-'
  let table = generateTable('bid-round-2f', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st rounds of bids, to test the 2nd

  /*
   *   _             _   _
   *  | |__  _   _  | |_| |__   ___  __      ____ _ _   _
   *  | '_ \| | | | | __| '_ \ / _ \ \ \ /\ / / _` | | | |
   *  | |_) | |_| | | |_| | | |  __/  \ V  V / (_| | |_| |_ _ _
   *  |_.__/ \__, |  \__|_| |_|\___|   \_/\_/ \__,_|\__, (_|_|_)
   *         |___/                                  |___/
   *
   *
   * South's bid in this round is different from that in most of
   * the other tests! This is because we want to check that W is
   * allowed to raise to a higher suit within the same level. If
   * the default bid is NT, we can't test it because there will be
   * no higher suit to bid on.
   */
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'D'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null}
  ]
  await table.save()

  // W tries to raise to 2S
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'level',
    level: 2,
    suit: 'S',
  })

  // we get only one event: the error
  t.is(events.length, 1)

  // check that it is, indeed, an error
  t.assert(events[0].error)
  // TODO: check error tag, once it's implemented


  // now, W tries to raise to 3D
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'level',
    level: 3,
    suit: 'D',
  })

  // we get only one event: the error
  t.is(events.length, 1)

  // check that it is, indeed, an error
  t.assert(events[0].error)
  // TODO: check error tag, once it's implemented


  // finally, W raises to 3H, which should work
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'level',
    level: 3,
    suit: 'H',
  })

  // we should get four events if the bid is successful
  t.is(events.length, 4)

  // one of them should have the bid details (the others will be empty
  t.is(events.filter(e => !!e.type).length, 1)

  let bid = events.find(e => !!e.type)
  t.is(bid.side, 'W')
  t.is(bid.type, 'level')
  t.is(bid.level, 3)
  t.is(bid.suit, 'H')

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 3: declarer pass (end bidding)', async t => {
  /**
   * Now we start on round 3 tests, which rounds 1 and 2 already
   * pre-decided. In this test, we check that the declarer is
   * allowed to pass in round 3 if the opponents have doubled in
   * round 2.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid3-declarer-pass-'
  let table = generateTable('bid-round-3b', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st rounds of bids, to test the 2nd
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'pass'},
      {side: 'E', bidType: 'double'},
    ], winnerIndex: 1},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // S passes
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}S@hool.test`,
    side: 'S',
    type: 'pass',
  })

  // there should be eight events in total: that is...
  t.is(events.length, 8)

  // ...four for the bid, and ...
  t.assert(events.slice(0,4).every(e => e.event == 'bid'))

  // ...the other four for the round
  t.assert(events.slice(5,8).every(e => e.event == 'state' && !!e.bids))

  // those should also have the contract, by the way
  t.assert(events.slice(5,8).every(e => !!e.contract))

  // let's inspect the last one, just to make sure
  let contract = events[events.length-1].contract
  t.is(contract.declarer, 'S')
  t.is(contract.level, 3)
  t.is(contract.suit, 'NT')
  t.assert(contract.double)
  t.assert(!contract.redouble)

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 3: declarer redouble (end bidding)', async t => {
  /**
   * Now we start on round 3 tests, which rounds 1 and 2 already
   * pre-decided. In this test, we check that the declarer is
   * allowed to redouble in round 3 if the opponents have doubled in
   * round 2.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid3-redouble-'
  let table = generateTable('bid-round-3c', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st rounds of bids, to test the 2nd
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'pass'},
      {side: 'E', bidType: 'double'},
    ], winnerIndex: 1},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // S redoubles
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}S@hool.test`,
    side: 'S',
    type: 'redouble',
  })

  // there should be eight events in total: that is...
  t.is(events.length, 8)

  // ...four for the bid, and ...
  t.assert(events.slice(0,4).every(e => e.event == 'bid'))

  // ...the other four for the round
  t.assert(events.slice(5,8).every(e => e.event == 'state' && !!e.bids))

  // those should also have the contract, by the way
  t.assert(events.slice(5,8).every(e => !!e.contract))

  // let's inspect the last one, just to make sure
  let contract = events[events.length-1].contract
  t.is(contract.declarer, 'S')
  t.is(contract.level, 3)
  t.is(contract.suit, 'NT')
  t.assert(contract.double)
  t.assert(contract.redouble)

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 3: end of round: illegal declarer raise', async t => {
  /**
   * When the defenders have doubled or passed in round 2 (i.e. there
   * was no raise) then the declarer should not be allowed to raise in
   * round 3. That's the scenario that's tested out here.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid3-declarer-badraise-'
  let table = generateTable('bid-round-3d', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st rounds of bids, to test the 2nd
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'pass'},
      {side: 'E', bidType: 'double'},
    ], winnerIndex: 1},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // S tries to raise
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}S@hool.test`,
    side: 'S',
    type: 'level',
    level: 4,
    suit: 'NT',
  })

  // there should be only one event: the error
  t.is(events.length, 1)
  t.assert(!!events[0].error)
  // TODO: check exact error tag when implemented

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 3: end of round: out-of-turn', async t => {
  /**
   * When the defenders have doubled or passed in round 2 (i.e. there
   * was no raise) then only the declarer is allowed to bid: bids from
   * all other players should be rejected.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid3-nondeclarer'
  let table = generateTable('bid-round-3e', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st rounds of bids, to test the 2nd
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'pass'},
      {side: 'E', bidType: 'double'},
    ], winnerIndex: 1},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // N tries to pass
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}N@hool.test`,
    side: 'N',
    type: 'pass',
  })

  // there should be only one event: the error
  t.is(events.length, 1)
  t.assert(!!events[0].error)
  // TODO: check exact error tag when implemented


  // E tries to pass
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}E@hool.test`,
    side: 'E',
    type: 'pass',
  })

  // there should be only one event: the error
  t.is(events.length, 1)
  t.assert(!!events[0].error)
  // TODO: check exact error tag when implemented


  // W tries to pass
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'pass',
  })

  // there should be only one event: the error
  t.is(events.length, 1)
  t.assert(!!events[0].error)
  // TODO: check exact error tag when implemented

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 3: end of round: pass (end bidding)', async t => {
  /**
   * Here is a situation where E and W have raised in round 2, so
   * N and S have the option of raising, doubling, or passing in
   * round 3. In this case, we check that they're allowed to pass.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid3-passerby'
  let table = generateTable('bid-round-3f', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st rounds of bids, to test the 2nd
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'level', level: 4, suit: 'S'},
      {side: 'E', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // N passes
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}N@hool.test`,
    side: 'N',
    type: 'pass',
  })

  // there should be 4 events: one for each player
  t.is(events.length, 4)

  // inspect the event that goes to N
  let event = events.find(e => e.to == `${userPrefix}N@hool.test`)
  t.is(event.side, 'N')
  t.is(event.type, 'pass')


  // S passes as well
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}S@hool.test`,
    side: 'S',
    type: 'pass',
  })

  // each player gets 2 events: S's bid, and the state update
  t.is(events.length, 8)

  // first four should be 'bid' events
  t.assert(events.slice(0,4).every(e => e.event == 'bid'))

  // next four should be state updates with bid info inside
  t.assert(events.slice(4,8).every(e => e.event == 'state' && !!e.bids))

  // check that contract is also passed along with bids
  t.assert(events.slice(4,8).every(e => !!e.contract))

  let contract = events[events.length-1].contract
  t.is(contract.declarer, 'W')
  t.is(contract.level, 4)
  t.is(contract.suit, 'S')

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 3: end of round: double', async t => {
  /**
   * Here is a situation where E and W have raised in round 2, so
   * N and S have the option of raising, doubling, or passing in
   * round 3. In this case, we check that they're allowed to double.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid3-doubler'
  let table = generateTable('bid-round-3g', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st rounds of bids, to test the 2nd
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'level', level: 4, suit: 'S'},
      {side: 'E', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // N doubles
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}N@hool.test`,
    side: 'N',
    type: 'double',
  })

  // there should be 4 events: one for each player
  t.is(events.length, 4)

  // inspect the event that goes to N
  let event = events.find(e => e.to == `${userPrefix}N@hool.test`)
  t.is(event.side, 'N')
  t.is(event.type, 'double')


  // S doubles as well
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}S@hool.test`,
    side: 'S',
    type: 'double',
  })

  // each player gets 2 events: S's bid, and the state update
  t.is(events.length, 8)

  // first four should be 'bid' events
  t.assert(events.slice(0,4).every(e => e.event == 'bid'))

  // next four should be state updates with bid info inside
  t.assert(events.slice(4,8).every(e => e.event == 'state' && !!e.bids))

  // but no contract, since the bidding isn't over!
  t.assert(events.slice(4,8).every(e => !e.contract))

  // inspect one of the events
  let bids = events[events.length-2].bids

  // both N and S should have doubled
  t.assert(bids.some(b => b.side == 'N' && b.type == 'double'))
  t.assert(bids.some(b => b.side == 'S' && b.type == 'double'))

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 3: end of round: out-of-turn moves', async t => {
  /**
   * Here is a situation where E and W have raised in round 2, so
   * N and S have the option of raising, doubling, or passing in
   * round 3. E and W don't have an option to play anything at all,
   * but, mischieveously enough, that's exactly what they try to do.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid3-shady-doubler-'
  let table = generateTable('bid-round-3h', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st rounds of bids, to test the 2nd
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'level', level: 4, suit: 'S'},
      {side: 'E', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // E tries, illegally, to double
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}E@hool.test`,
    side: 'E',
    type: 'double',
  })

  // there should be only one event: the error
  t.is(events.length, 1)
  t.assert(!!events[0].error)
  // TODO: check exact error tag when implemented


  // failing to learn from their partner, W also tries to double
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'double',
  })

  // still only one event: the error
  t.is(events.length, 1)
  t.assert(!!events[0].error)
  // TODO: check exact error tag when implemented


  // E tries, illegally, to raise
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}E@hool.test`,
    side: 'E',
    type: 'level',
    level: 5,
    suit: 'NT',
  })

  // there should be only one event: the error
  t.is(events.length, 1)
  t.assert(!!events[0].error)
  // TODO: check exact error tag when implemented

  // W is evidently a copycat
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'level',
    level: 5,
    suit: 'NT',
  })

  // there should be only one event: the error
  t.is(events.length, 1)
  t.assert(!!events[0].error)
  // TODO: check exact error tag when implemented

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 3: end of round: re-raise', async t => {
  /**
   * Here is a situation where E and W have raised in round 2, so
   * N and S have the option of raising, doubling, or passing in
   * round 3. This time, one of them decides to raise.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid3-risin-'
  let table = generateTable('bid-round-3i', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st rounds of bids, to test the 2nd
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'level', level: 4, suit: 'S'},
      {side: 'E', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // this time, N is up to the challenge and decides to
  // answer a raise with a raise
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}N@hool.test`,
    side: 'N',
    type: 'level',
    level: 5,
    suit: 'H',
  })

  // there should be 4 events: one for each player
  t.is(events.length, 4)

  // inspect the event that goes to N
  let event = events.find(e => e.to == `${userPrefix}N@hool.test`)
  t.is(event.side, 'N')
  t.is(event.type, 'level')
  t.is(event.level, 5)
  t.is(event.suit, 'H')

  // S is more timid, but still pushes for a double
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}S@hool.test`,
    side: 'S',
    type: 'double',
  })

  // each player gets 2 events: S's bid, and the state update
  t.is(events.length, 8)

  // first four should be 'bid' events
  t.assert(events.slice(0,4).every(e => e.event == 'bid' && e.side == 'S'))

  // next four should be state updates with bid info inside
  t.assert(events.slice(4,8).every(e => e.event == 'state' && !!e.bids))

  // but no contract, since the bidding isn't over!
  t.assert(events.slice(4,8).every(e => !e.contract))

  // inspect one of the events
  let bids = events[events.length-2].bids

  // N should have raised while S passed
  t.assert(bids.some(b => b.side == 'N' && b.type == 'level' && b.level == 5))
  t.assert(bids.some(b => b.side == 'S' && b.type == 'double'))

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 3: end of round: illegal re-raise: too low', async t => {
  /**
   * Here is a situation where E and W have raised in round 2, so
   * N and S have the option of raising, doubling, or passing in
   * round 3. This time, N decides to raise but ends up bidding too
   * low, so the bid is rejected. The same goes for S, after which N
   * eventually makes a successful bid.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid3-settin-'
  let table = generateTable('bid-round-3j', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st rounds of bids, to test the 2nd
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'level', level: 4, suit: 'S'},
      {side: 'E', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // this time, N is up to the challenge and decides to
  // answer a raise with a raise
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}N@hool.test`,
    side: 'N',
    type: 'level',
    level: 3,
    suit: 'C',
  })

  // only one event should come: the error
  t.is(events.length, 1)
  t.assert(events[0].error)
  // TODO: check error tag, once it's implemented


  // S gets the level right, but the suit is still too small
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}S@hool.test`,
    side: 'S',
    type: 'level',
    level: 4,
    suit: 'H',
  })

  // only one event should come: the error
  t.is(events.length, 1)
  t.assert(events[0].error)
  // TODO: check error tag, once it's implemented


  // finally, N raises to a reasonable level
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}N@hool.test`,
    side: 'N',
    type: 'level',
    level: 4,
    suit: 'NT',
  })

  // there should be 4 events: one for each player
  t.is(events.length, 4)

  // inspect the event that goes to N
  let event = events.find(e => e.to == `${userPrefix}N@hool.test`)
  t.is(event.side, 'N')
  t.is(event.type, 'level')
  t.is(event.level, 4)
  t.is(event.suit, 'NT')


  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 4: end of round: illegal raise (not allowed)', async t => {
  /**
   * After N re-raises in round 3, E and W try to raise yet again in
   * round 4—which should fail, because raises aren't allowed beyond
   * that point.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid4-raiser-'
  let table = generateTable('bid-round-4a', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st, 2nd and 3rd round, to test the 4th
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'level', level: 4, suit: 'S'},
      {side: 'E', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'S', bidType: 'pass'},
      {side: 'N', bidType: 'level', level: 4, suit: 'NT'},
    ], winnerIndex: 1},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // E tries to raise too low
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}E@hool.test`,
    side: 'E',
    type: 'level',
    level: 1,
    suit: 'C',
  })

  // only one event should come: the error
  t.is(events.length, 1)
  t.assert(events[0].error)
  // TODO: check error tag, once it's implemented


  // W tries to raise properly, but shouldn't be allowed to
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'level',
    level: 5,
    suit: 'NT',
  })

  // only one event should come: the error
  t.is(events.length, 1)
  t.assert(events[0].error)
  // TODO: check error tag, once it's implemented

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 4: end of round: double', async t => {
  /**
   * After N re-raises in round 3, E and W are allowed to double
   * or pass. In this test case, we check for both.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid4-doubler-'
  let table = generateTable('bid-round-4b', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st, 2nd and 3rd round, to test the 4th
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'level', level: 4, suit: 'S'},
      {side: 'E', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'S', bidType: 'pass'},
      {side: 'N', bidType: 'level', level: 4, suit: 'NT'},
    ], winnerIndex: 1},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // E doubles
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}E@hool.test`,
    side: 'E',
    type: 'double',
  })

  // inspect the event that goes to E
  let event = events.find(e => e.to == `${userPrefix}E@hool.test`)
  t.is(event.side, 'E')
  t.is(event.type, 'double')// there should be 4 events: one for each player
  t.is(events.length, 4)


  // W passes, which should also work
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'pass',
  })

  // each player gets 2 events: W's bid, and full round
  t.is(events.length, 8)

  // first four should be 'bid' events
  t.assert(events.slice(0,4).every(e => e.event == 'bid'))

  // next four should be state updates with bid info inside
  t.assert(events.slice(4,8).every(e => e.event == 'state' && !!e.bids))

  // take one, for inspection
  event = events[4]

  // first check E's...
  t.is(event.bids[0].side, 'E')
  t.is(event.bids[0].type, 'double')
  t.assert(event.bids[0].won)

  // ...and then check W's
  t.is(event.bids[1].side, 'W')
  t.is(event.bids[1].type, 'pass')
  t.assert(!event.bids[1].won)

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 4: end of round: pass (end bidding)', async t => {
  /**
   * After N re-raises in round 3, E and W can pass to end the bidding.
   * Here, we verify that this is indeed the case.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid4-passer-'
  let table = generateTable('bid-round-4c', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st, 2nd and 3rd round, to test the 4th
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'level', level: 4, suit: 'S'},
      {side: 'E', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'S', bidType: 'pass'},
      {side: 'N', bidType: 'level', level: 4, suit: 'NT'},
    ], winnerIndex: 1},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // E passes
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}E@hool.test`,
    side: 'E',
    type: 'pass',
  })

  // inspect the event that goes to E
  let event = events.find(e => e.to == `${userPrefix}E@hool.test`)
  t.is(event.side, 'E')
  t.is(event.type, 'pass')// there should be 4 events: one for each player
  t.is(events.length, 4)


  // W passes, which should also work
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'pass',
  })

  // each player gets 2 events: W's bid, and full round with contract
  t.is(events.length, 8)

  // first four should be 'bid' events
  t.assert(events.slice(0,4).every(e => e.event == 'bid'))

  // next four should be state updates with bid info inside
  t.assert(events.slice(4,8).every(e => e.event == 'state' && !!e.bids))

  // take one, for inspection
  event = events[4]

  // first check W's...
  t.is(event.bids[0].side, 'W')
  t.is(event.bids[0].type, 'pass')

  // ...and then check E's
  t.is(event.bids[1].side, 'E')
  t.is(event.bids[1].type, 'pass')

  // check that contract is also passed along with bids
  t.assert(events.slice(4,8).every(e => !!e.contract))

  let contract = events[events.length-1].contract
  t.is(contract.declarer, 'N')
  t.is(contract.level, 4)
  t.is(contract.suit, 'NT')
  t.assert(!contract.double)
  t.assert(!contract.redouble)

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 4: end of round: declarer raise (not allowed)', async t => {
  /**
   * After S doubles in round 3, the declarer W is allowed to redouble
   * or pass. Here, we see what happens when they try to do something
   * else instead.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid4-declarer-rising-'
  let table = generateTable('bid-round-4d', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st, 2nd and 3rd round, to test the 4th
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'level', level: 4, suit: 'S'},
      {side: 'E', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'S', bidType: 'double'},
      {side: 'N', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // W tries to raise, which isn't allowed
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'level',
    level: 5,
    suit: 'NT',
  })

  // only one event should come: the error
  t.is(events.length, 1)
  t.assert(events[0].error)
  // TODO: check error tag, once it's implemented

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 4: end of round: declarer pass (end bidding)', async t => {
  /**
   * After S doubles in round 3, the declarer W is allowed to redouble
   * or pass. Here, we check that the round is ended properly once
   * they pass.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid4-passing-declaration-'
  let table = generateTable('bid-round-4e', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st, 2nd and 3rd round, to test the 4th
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'level', level: 4, suit: 'S'},
      {side: 'E', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'S', bidType: 'double'},
      {side: 'N', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // W tries to raise, which isn't allowed
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'pass',
  })

  // each player gets 2 events: W's bid, and full round with contract
  t.is(events.length, 8)

  // first four should be 'bid' events
  t.assert(events.slice(0,4).every(e => e.event == 'bid'))

  // next four should be state updates with bid info inside
  t.assert(events.slice(4,8).every(e => e.event == 'state' && !!e.bids))

  // take one, for inspection
  event = events[4]

  // W passed
  t.is(event.bids[0].side, 'W')

  // check that contract is also passed along with bids
  t.assert(events.slice(4,8).every(e => !!e.contract))

  let contract = events[events.length-1].contract
  t.is(contract.declarer, 'W')
  t.is(contract.level, 4)
  t.is(contract.suit, 'S')
  t.assert(contract.double)
  t.assert(!contract.redouble)

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 4: end of round: declarer redouble (end bidding)', async t => {
  /**
   * After S doubles in round 3, the declarer W is allowed to redouble
   * or pass. Here, we check that the round is ended properly once
   * they redouble.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid4-redoubled-'
  let table = generateTable('bid-round-4f', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st, 2nd and 3rd round, to test the 4th
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'level', level: 4, suit: 'S'},
      {side: 'E', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'S', bidType: 'double'},
      {side: 'N', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // W tries to raise, which isn't allowed
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'redouble',
  })

  // each player gets 2 events: W's bid, and full round with contract
  t.is(events.length, 8)

  // first four should be 'bid' events
  t.assert(events.slice(0,4).every(e => e.event == 'bid'))

  // next four should be state updates with bid info inside
  t.assert(events.slice(4,8).every(e => e.event == 'state' && !!e.bids))

  // take one, for inspection
  event = events[4]

  // W passed
  t.is(event.bids[0].side, 'W')

  // check that contract is also passed along with bids
  t.assert(events.slice(4,8).every(e => !!e.contract))

  let contract = events[events.length-1].contract
  t.is(contract.declarer, 'W')
  t.is(contract.level, 4)
  t.is(contract.suit, 'S')
  t.assert(contract.double)
  t.assert(contract.redouble)

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 5: out-of-turn', async t => {
  /**
   * Test for round 5: only the declarer is allowed to play; here
   * we make sure that other people aren't able to.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid5-unriseable-'
  let table = generateTable('bid-round-5a', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st, 2nd and 3rd round, to test the 4th
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'level', level: 4, suit: 'S'},
      {side: 'E', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'S', bidType: 'pass'},
      {side: 'N', bidType: 'level', level: 4, suit: 'NT'},
    ], winnerIndex: 1},
    {bids: [
      {side: 'E', bidType: 'double'},
      {side: 'W', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // S tries to pass (which shouldn't work)
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}S@hool.test`,
    side: 'S',
    type: 'pass',
  })

  // only one event should come: the error
  t.is(events.length, 1)
  t.assert(events[0].error)
  // TODO: check error tag, once it's implemented

  // E tries to pass (which *definitely* shouldn't work)
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}E@hool.test`,
    side: 'E',
    type: 'pass',
  })

  // only one event should come: the error
  t.is(events.length, 1)
  t.assert(events[0].error)
  // TODO: check error tag, once it's implemented

  // likewise for W
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}W@hool.test`,
    side: 'W',
    type: 'pass',
  })

  // only one event should come: the error
  t.is(events.length, 1)
  t.assert(events[0].error)
  // TODO: check error tag, once it's implemented

  // N does a successful pass, though
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}N@hool.test`,
    side: 'N',
    type: 'pass',
  })

  // so it ended up as pass anyway: all that bureaucracy for nothing!
  t.is(events.length, 8)

  // bureaucracy says the first four should be `bid` events
  t.assert(events.slice(0,4).every(e => e.event == 'bid'))

  // bureaucracy further states that the next four should be `state`
  t.assert(events.slice(4,8).every(e => e.event == 'state' && !!e.bids))

  // bureaucracy inspects one, just to make sure
  event = events[4]
  t.is(event.bids[0].side, 'N')

  // where there's bureaucracy, the contractors aren't far away ;)
  t.assert(events.slice(4,8).every(e => !!e.contract))

  let contract = events[events.length-1].contract
  t.is(contract.declarer, 'N')
  t.is(contract.level, 4)
  t.is(contract.suit, 'NT')
  t.assert(contract.double)
  t.assert(!contract.redouble)

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 5: end of round: illegal raise (not allowed)', async t => {
  /**
   * Test for round 5: the declarer should be able to either redouble
   * or pass (not raise, as they're trying now).
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid5-reachforthesky-'
  let table = generateTable('bid-round-5b', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st, 2nd and 3rd round, to test the 4th
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'level', level: 4, suit: 'S'},
      {side: 'E', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'S', bidType: 'pass'},
      {side: 'N', bidType: 'level', level: 4, suit: 'NT'},
    ], winnerIndex: 1},
    {bids: [
      {side: 'E', bidType: 'double'},
      {side: 'W', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // N tries to raise (which shouldn't work)
  let events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}N@hool.test`,
    side: 'N',
    type: 'level',
    level: 5,
    suit: 'NT',
  })

  // only one event should come: the error
  t.is(events.length, 1)
  t.assert(events[0].error)
  // TODO: check error tag, once it's implemented

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 5: end of round: declarer redouble (end bidding)', async t => {
  /**
   * Test for round 5: the declarer must be able to either redouble
   * or pass (in this case, they're super confident and decide to go
   * for a redouble).
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid5-overconfident-'
  let table = generateTable('bid-round-5c', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st, 2nd and 3rd round, to test the 4th
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'level', level: 4, suit: 'S'},
      {side: 'E', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'S', bidType: 'pass'},
      {side: 'N', bidType: 'level', level: 4, suit: 'NT'},
    ], winnerIndex: 1},
    {bids: [
      {side: 'E', bidType: 'double'},
      {side: 'W', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // N redoubles
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}N@hool.test`,
    side: 'N',
    type: 'redouble',
  })

  // so it ended up as pass anyway: all that bureaucracy for nothing!
  t.is(events.length, 8)

  // bureaucracy says the first four should be `bid` events
  t.assert(events.slice(0,4).every(e => e.event == 'bid'))

  // bureaucracy further states that the next four should be `state`
  t.assert(events.slice(4,8).every(e => e.event == 'state' && !!e.bids))

  // bureaucracy inspects one, just to make sure
  event = events[4]
  t.is(event.bids[0].side, 'N')

  // where there's bureaucracy, the contractors aren't far away ;)
  t.assert(events.slice(4,8).every(e => !!e.contract))

  let contract = events[events.length-1].contract
  t.is(contract.declarer, 'N')
  t.is(contract.level, 4)
  t.is(contract.suit, 'NT')
  t.assert(contract.double)
  t.assert(contract.redouble)

  await Table.deleteOne({ _id: table._id })
})

test('bidding: round 5: end of round: declarer pass (end bidding)', async t => {
  /**
   * Test for round 5: the declarer must be able to either redouble
   * or pass (in this case they're a bit wary and decide to pass).
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let userPrefix = 'bid5-underconfident-'
  let table = generateTable('bid-round-5d', userPrefix)
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st, 2nd and 3rd round, to test the 4th
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'W', bidType: 'level', level: 4, suit: 'S'},
      {side: 'E', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids: [
      {side: 'S', bidType: 'pass'},
      {side: 'N', bidType: 'level', level: 4, suit: 'NT'},
    ], winnerIndex: 1},
    {bids: [
      {side: 'E', bidType: 'double'},
      {side: 'W', bidType: 'pass'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null},
  ]
  await table.save()

  // N redoubles
  events = await hool.handleBid({
    table: ourTable,
    user: `${userPrefix}N@hool.test`,
    side: 'N',
    type: 'pass',
  })

  // so it ended up as pass anyway: all that bureaucracy for nothing!
  t.is(events.length, 8)

  // bureaucracy says the first four should be `bid` events
  t.assert(events.slice(0,4).every(e => e.event == 'bid'))

  // bureaucracy further states that the next four should be `state`
  t.assert(events.slice(4,8).every(e => e.event == 'state' && !!e.bids))

  // bureaucracy inspects one, just to make sure
  event = events[4]
  t.is(event.bids[0].side, 'N')

  // where there's bureaucracy, the contractors aren't far away ;)
  t.assert(events.slice(4,8).every(e => !!e.contract))

  let contract = events[events.length-1].contract
  t.is(contract.declarer, 'N')
  t.is(contract.level, 4)
  t.is(contract.suit, 'NT')
  t.assert(contract.double)
  t.assert(!contract.redouble)

  await Table.deleteOne({ _id: table._id })
})

/**
 * Cardplay tests
 *
 * Check that cardplay is working properly, including
 * trumps, trick-taking, following suit, etc.
 */

 let setHands = {
   N: [ 'S8', 'S4', 'S2', 'HK', 'HQ', 'H7', 'H3', 'DK', 'D9', 'D7', 'CQ', 'C8', 'C4' ],
   E: [ 'SA', 'S9', 'S5', 'H9', 'H8', 'H4', 'D10', 'D8', 'D3', 'CA', 'CK', 'C9', 'C5' ],
   S: [ 'SK', 'SQ', 'S10', 'HA', 'H10', 'H5', 'DA', 'DQ', 'DJ', 'D4', 'C10', 'C6', 'C2' ],
   W: [ 'SJ', 'S7', 'S6', 'S3', 'HJ', 'H6', 'H2', 'D6', 'D5', 'D2', 'CJ', 'C7', 'C3' ],
 }

function generateTable(tablePrefix, userPrefix) {
  return new Table({
    table_id: `${tablePrefix}@tables.hools.test`,
    occupants: [
      {
        jid: `${userPrefix}N@hool.test`,
        role: 'player',
        seat: 'N',
        host: true,
      },
      {
        jid: `${userPrefix}E@hool.test`,
        role: 'player',
        seat: 'E',
      },
      {
        jid: `${userPrefix}S@hool.test`,
        role: 'player',
        seat: 'S',
      },
      {
        jid: `${userPrefix}W@hool.test`,
        role: 'player',
        seat: 'W',
      },
    ],
    seats: new Map(['N', 'E', 'S', 'W'].map(side =>
      [side, {cards: setHands[side], user: `${userPrefix}${side}@hool.test`}]
    )),
    runningScore: new Map([ // TODO: make dynamic
      ['NS', 0],
      ['EW', 0],
    ]),
  })
}

test('opening lead: turn to play', async t=> {
  /**
   * Here, we simulate the final (in this case, second)
   * round of bidding to see that the opening leader
   * (first person who plays a card) is chosen properly.
   *
   * E and W both pass in the first round with this test data.
   * S wins the bid, which means the opening leader is W (who
   * sits clockwise from S). If other people try to play before
   * their turn, the play should be rejected.
   */
  let hool = new HoolEngine({ db: mongoose.connection })

  let table = generateTable('leadopen', 'leading')
  let ourTable = table.table_id

  // set up and save table
  table.stage = 'bidding'

  // we'll manually set the 1st rounds of bids, to test the 2nd
  table.bids = [
    {bids: [
      {side: 'S', bidType: 'level', level: 3, suit: 'NT'},
      {side: 'N', bidType: 'level', level: 2, suit: 'H'},
      {side: 'E', bidType: 'pass'},
      {side: 'W', bidType: 'level', level: 1, suit: 'NT'},
    ], winnerIndex: 0},
    {bids:[], winnerIndex: null}
  ]
  await table.save()

  // manually run 2nd round of bidding, to check logic
  let events = await hool.handleBid({
    table: ourTable,
    user: `leadingE@hool.test`,
    side: 'E',
    type: 'pass',
  })

  events = await hool.handleBid({
    table: ourTable,
    user: `leadingW@hool.test`,
    side: 'W',
    type: 'pass',
  })

  // try opening lead from wrong hand: it should fail
  events = await hool.handleCardplay({
    table: ourTable,
    user: 'leadingN@hool.test',
    side: 'N',
    rank: '4',
    suit: 'C',
  })

  table = await Table.findOne({ table_id: ourTable })

  t.is(events.length, 1)
  t.is(!!events[0].error, true)

  events = await hool.handleCardplay({
    table: ourTable,
    user: 'leadingS@hool.test',
    side: 'S',
    rank: 'Q',
    suit: 'D',
  })

  t.is(events.length, 1)
  t.is(!!events[0].error, true)

  events = await hool.handleCardplay({
    table: ourTable,
    user: 'leadingE@hool.test',
    side: 'E',
    rank: '3',
    suit: 'D',
  })

  t.is(events.length, 1)
  t.is(!!events[0].error, true)

  // this one should be successful

  events = await hool.handleCardplay({
    table: ourTable,
    user: 'leadingW@hool.test',
    side: 'W',
    rank: '7',
    suit: 'C',
  })

  // keep only cardplay events
  let cardplayEvents = events.filter(e => e.event == 'cardplay')

  t.is(cardplayEvents.length, 4)
  t.is(!!cardplayEvents[0].error, false)
  t.is(cardplayEvents[0].rank, '7')
  t.is(cardplayEvents[0].suit, 'C')

  await Table.deleteOne({ _id: table._id })
})

test('opening lead: dummy reveal', async t => {
  /**
   * Here, we check that the dummy's hand is revealed
   * to all non-dummy players via a 'hand' in the 'state'
   * event.
   *
   * All three of the players should have got the dummy's
   * hand—and of course the hand should belong to the dummy,
   * because if it were someone else's hand that was revealed,
   * it would be a disaster!
   */

  let hool = new HoolEngine({ db: mongoose.connection })

  let table = generateTable('dummyopen', 'dummied')
  // set up and save table
  table.contract = {
    level: 3,
    suit: 'NT',
    declarer: 'S'
  }
  table.turn = 'W'
  table.stage = 'play'
  await table.save()

  let ourTable = table.table_id


  t.is((await Table.getTableById(ourTable)).turn, 'W')

  // W plays 7C
  let events = await hool.handleCardplay({
    table: ourTable,
    user: 'dummiedW@hool.test',
    side: 'W',
    rank: '7',
    suit: 'C',
  })

  // filter in state update events (except to N, who is dummy)
  let stateEvents = events.filter(e =>
    e.event == 'state' &&
    e.to != 'dummiedN@hool.test'
  )

  // each of the three non-dummy sides should have got one state update
  t.is(stateEvents.length, 3)

  // all should have got N's hands (anyone else's would be a disaster!)
  t.assert(stateEvents.every(e => e.hands[0].side == 'N'))

  // hand sanity-check (we know N has card H7 in test data)
  t.assert(stateEvents[0].hands[0].cards.some(c =>
    c.rank == 7 &&
    c.suit == 'H'
  ))
})

test('opening lead: declarer reveal', async t => {
  /**
   * Here, we need to make sure the dummy gets to see the
   * declarer's cards. In this test data, N is the dummy
   * and S is the declarer.
   */

  let hool = new HoolEngine({ db: mongoose.connection })

  let table = generateTable('declareropen', 'declared')
  // set up and save table
  table.contract = {
    level: 3,
    suit: 'NT',
    declarer: 'S'
  }
  table.turn = 'W'
  table.stage = 'play'
  await table.save()

  let ourTable = table.table_id


  t.is((await Table.getTableById(ourTable)).turn, 'W')

  // W plays 7C
  let events = await hool.handleCardplay({
    table: ourTable,
    user: 'declaredW@hool.test',
    side: 'W',
    rank: '7',
    suit: 'C',
  })

  // filter in state update coming to N (who is dummy)
  let stateEvents = events.filter(e =>
    e.event == 'state' &&
    e.to == 'declaredN@hool.test'
  )


  /*
   * N should have only got hands of self or S (who is declarer)
   *
   * but remember, we aren't *assuming* N always gets self hand since
   * that may change in future!
   */
  t.assert(stateEvents.every(e => ['N','S'].includes(e.hands[0].side)))

  // find the declarer's hand
  let declarerHandEvents = stateEvents.filter(e => e.hands[0].side == 'S')

  // there should be only one
  t.is(declarerHandEvents.length, 1)

  // hand sanity-check (we know S has card C10 in test data)
  t.assert(declarerHandEvents[0].hands[0].cards.some(c =>
    c.rank == '10' &&
    c.suit == 'C'
  ))
})

test('declarer plays for dummy', async t => {
  /**
   * Make sure that the dummy can't play their cards;
   * the play must come from the declarer
   */

  let hool = new HoolEngine({ db: mongoose.connection })

  // set up and save table
  let ourPrefix = 'd4d'
  let table = generateTable('declarer4dummy', ourPrefix)
  table.contract = {
    level: 3,
    suit: 'NT',
    declarer: 'S'
  }
  table.turn = 'W'
  table.stage = 'play'
  await table.save()

  let ourTable = table.table_id

  t.is((await Table.getTableById(ourTable)).turn, 'W')

  // W plays C7
  let events = await hool.handleCardplay({
    table: ourTable,
    user: `${ourPrefix}W@hool.test`,
    side: 'W',
    rank: '7',
    suit: 'C',
  })

  // N (the dummy) tries to play C8 and fails
  events = await hool.handleCardplay({
    table: ourTable,
    user: `${ourPrefix}N@hool.test`,
    side: 'N',
    rank: '8',
    suit: 'C',
  })

  t.is(events.length, 1)
  t.assert(!!events[0].error)

  // S tries C8 from N's side; this one should succeed
  events = await hool.handleCardplay({
    table: ourTable,
    user: `${ourPrefix}S@hool.test`,
    side: 'N',
    rank: '8',
    suit: 'C',
  })

  // 4 events because each player gets one
  t.is(events.length, 4)

  // none of them should have errors
  t.assert(events.every(e => !e.error))
})

test('following suit: simple', async t => {
  /**
   * This is a simple test to make sure that, under
   * ordinary circumstances, players have to follow suit
   * (if a club is played, and you have clubs in your
   * hand, then you have to play clubs).
   */

  let hool = new HoolEngine({ db: mongoose.connection })

  // set up and save table
  let ourPrefix = 'firstsuitor'
  let table = generateTable('suitone', ourPrefix)
  table.contract = {
    level: 3,
    suit: 'NT',
    declarer: 'S'
  }
  table.turn = 'W'
  table.stage = 'play'
  await table.save()

  let ourTable = table.table_id

  t.is((await Table.getTableById(ourTable)).turn, 'W')

  // W plays C7
  let events = await hool.handleCardplay({
    table: ourTable,
    user: `${ourPrefix}W@hool.test`,
    side: 'W',
    rank: '7',
    suit: 'C',
  })

  // N tries D7, HQ and fails (S plays ∵ declarer)
  for (let badCard of [new Card('D7'), new Card('HQ')]) {
    events = await hool.handleCardplay({
      table: ourTable,
      user: `${ourPrefix}S@hool.test`,
      side: 'N',
      rank: badCard.rank,
      suit: badCard.suit,
    })

    t.is(events.length, 1)
    t.assert(!!events[0].error)
  }

  // N tries C8, and this time it should succeed (club follows club)
  events = await hool.handleCardplay({
    table: ourTable,
    user: `${ourPrefix}S@hool.test`,
    side: 'N',
    rank: '8',
    suit: 'C',
  })

  // 4 events because each player gets one
  t.is(events.length, 4)

  // none of them should have errors
  t.assert(events.every(e => !e.error))
})

test('following suit: end of trick', async t => {
  /**
   * Confirm that people don't have to continue following
   * suit once the round is over.
   *
   * In this example, East wins a club trick, and then should
   * be able to play a non-club card (in this case, a spade)
   * to open the next round. Suit following should continue
   * from thereon.
   */

  let hool = new HoolEngine({ db: mongoose.connection })

  // set up and save table
  let ourPrefix = 'secondsuitor'
  let table = generateTable('suittwo', ourPrefix)
  table.contract = {
    level: 3,
    suit: 'NT',
    declarer: 'S'
  }
  table.turn = 'W'
  table.stage = 'play'
  await table.save()

  let ourTable = table.table_id

  t.is((await Table.getTableById(ourTable)).turn, 'W')

  /*
   * Following cards are played in order:
   * - W plays C7
   * - N plays C8
   * - E plays CK
   * - S plays C2
   * ...and then we're ready for the main test, when E is
   * ready to play.
   */

  for (let [player, side, card] of [
    ['W', 'W', 'C7'],
    ['S', 'N', 'C8'],
    ['E', 'E', 'CK'],
    ['S', 'S', 'C2'],
  ]) {
    card = new Card(card)

    events = await hool.handleCardplay({
      table: ourTable,
      user: `${ourPrefix}${player}@hool.test`,
      side: side,
      rank: card.rank,
      suit: card.suit,
    })

    // should all happen without any errors
    t.assert(events.every(e => !e.error))
  }

  // E tries S5 (not a club), which should still succeed
  events = await hool.handleCardplay({
    table: ourTable,
    user: `${ourPrefix}E@hool.test`,
    side: 'E',
    rank: '5',
    suit: 'S',
  })

  // 4 events because each player gets one
  t.is(events.length, 4)

  // none of them should have errors
  t.assert(events.every(e => !e.error))
})

test('following suit: reset on new trick', async t => {
  /**
   * Check that "following suit" resumes after a new trick
   * is started, this time using the leading suit of the new
   * trick (not the old one) of course.
   *
   * For example: the "clubs" trick is over, and E has
   * started a new "spades" trick, so from now on following
   * suit should happen with spades.
   */

  let hool = new HoolEngine({ db: mongoose.connection })

  // set up and save table
  let ourPrefix = 'thirdsuitor'
  let table = generateTable('suitthree', ourPrefix)
  table.contract = {
    level: 3,
    suit: 'NT',
    declarer: 'S'
  }
  table.turn = 'W'
  table.stage = 'play'
  await table.save()

  let ourTable = table.table_id

  t.is((await Table.getTableById(ourTable)).turn, 'W')

  /*
   * Following cards are played in order:
   * - W plays C7
   * - N plays C8
   * - E plays CK
   * - S plays C2
   * - E leads with S5
   * ...and then we're ready for the main test, when S is
   * ready to play.
   */

  for (let [player, side, card] of [
    ['W', 'W', 'C7'],
    ['S', 'N', 'C8'],
    ['E', 'E', 'CK'],
    ['S', 'S', 'C2'],
    ['E', 'E', 'S5'],
  ]) {
    card = new Card(card)

    events = await hool.handleCardplay({
      table: ourTable,
      user: `${ourPrefix}${player}@hool.test`,
      side: side,
      rank: card.rank,
      suit: card.suit,
    })

    // should all happen without any errors
    t.assert(events.every(e => !e.error))
  }

  // S tries C10 (club, not spade), which should fail
  events = await hool.handleCardplay({
    table: ourTable,
    user: `${ourPrefix}S@hool.test`,
    side: 'S',
    rank: '10',
    suit: 'C',
  })

  t.is(events.length, 1)
  t.assert(!!events[0].error)

  // S tries QS, which should succeed this time
  events = await hool.handleCardplay({
    table: ourTable,
    user: `${ourPrefix}S@hool.test`,
    side: 'S',
    rank: 'Q',
    suit: 'S',
  })

  // 4 events because each player gets one
  t.is(events.length, 4)

  // none of them should have errors
  t.assert(events.every(e => !e.error))
})

test('following suit: discards', async t => {
  /**
   * Check that discards are allowed: that is, if you don't
   * have a card matching the current suit, then you don't
   * have to follow suit.
   *
   * In this example, we play until N, E, and S run out of
   * spades. Then W leads with a spade, and everyone else
   * discards in different suits.
   */

  let hool = new HoolEngine({ db: mongoose.connection })

  // set up and save table
  let ourPrefix = 'heartless'
  let table = generateTable('suitfour', ourPrefix)
  table.contract = {
    level: 3,
    suit: 'NT',
    declarer: 'S'
  }
  table.turn = 'W'
  table.stage = 'play'
  await table.save()

  let ourTable = table.table_id

  t.is((await Table.getTableById(ourTable)).turn, 'W')

  /*
   * By now I'm guessing you know what's going on in the
   * following cardplay sequence. If you don't, scroll up
   * and check the description in one of the previous test
   * functions 😉
   */

  for (let [player, side, card] of [
    ['W', 'W', 'C7'],
    ['S', 'N', 'C8'],
    ['E', 'E', 'CK'],
    ['S', 'S', 'C2'],

    ['E', 'E', 'S5'],
    ['S', 'S', 'SQ'],
    ['W', 'W', 'S3'],
    ['S', 'N', 'S2'],

    ['S', 'S', 'SK'],
    ['W', 'W', 'S6'],
    ['S', 'N', 'S4'],
    ['E', 'E', 'SA'],

    ['E', 'E', 'S9'],
    ['S', 'S', 'S10'],
    ['W', 'W', 'SJ'],
    ['S', 'N', 'S8'],

    ['W', 'W', 'S7'], // after this, everyone has to discard!
  ]) {
    card = new Card(card)

    events = await hool.handleCardplay({
      table: ourTable,
      user: `${ourPrefix}${player}@hool.test`,
      side: side,
      rank: card.rank,
      suit: card.suit,
    })

    // should all happen without any errors
    t.assert(events.every(e => !e.error))
  }

  /*
   * N tries H3 (hearts, not spades) which still works ∵ N
   * has no more hearts left
   */

  events = await hool.handleCardplay({
    table: ourTable,
    user: `${ourPrefix}S@hool.test`,
    side: 'N',
    rank: '3',
    suit: 'H',
  })

  // 4 events because each player gets one
  t.is(events.length, 4)

  // none of them should have errors
  t.assert(events.every(e => !e.error))

  /*
   * E discards D3: again, heartless.
   */

  events = await hool.handleCardplay({
    table: ourTable,
    user: `${ourPrefix}E@hool.test`,
    side: 'E',
    rank: '3',
    suit: 'D',
  })

  // 4 events because each player gets one
  t.is(events.length, 4)

  // none of them should have errors
  t.assert(events.every(e => !e.error))

  /*
   * S plays from own side this time, discarding a club
   */

  events = await hool.handleCardplay({
    table: ourTable,
    user: `${ourPrefix}S@hool.test`,
    side: 'S',
    rank: '6',
    suit: 'C',
  })

  // 8 events this time: one for cardplay and one for trick score!
  t.is(events.length, 8)

  // none of them should have errors
  t.assert(events.every(e => !e.error))

  // ...and that's it! All discards successful 🎉
})

// TODO: write some more tests to test trumps! (need to change contract first)

/**
 * Scoring function tests
 *
 * Test the scoring functions and check that they
 * give sane results!
 */


test('hoolScore function: test #1',async t => {
  contract = { declarer: 'N', level: 9-6, suit: 'S' }
   tricks_NS = 8
   tricks_EW = 5

  let score =await new Table().hoolScore(contract, tricks_NS, tricks_EW)
  t.is(score['EW'], 20)
  })

test('hoolScore function: test #2',async t => {
  contract = { declarer: 'N', level: 9-6, suit: 'S', double: false }
  tricks_NS = 12
  tricks_EW = 1

  let score =await  new Table().hoolScore(contract, tricks_NS, tricks_EW)
  t.is(score['NS'], 60)
})

test('hoolScore function: test #3',async t => {
  contract = { declarer: 'N', level: 9-6, suit: 'S', double: true }
  tricks_NS = 9
  tricks_EW = 4

  let score =await new Table().hoolScore(contract, tricks_NS, tricks_EW)
  t.is(score['NS'], 60)
})

test('hoolScore function: test #4',async t => {
  contract = { declarer: 'E', level: 9-6, suit: 'NT'}
  tricks_NS = 4
  tricks_EW = 9

  let score =await new Table().hoolScore(contract, tricks_NS, tricks_EW)
  t.is(score['EW'], 130)
})

test('hoolScore function: test #5',async t => {
  contract = { declarer: 'W', level: 9-6, suit: 'NT', double: true }
  tricks_NS = 8
  tricks_EW = 5

  let score =await new Table().hoolScore(contract, tricks_NS, tricks_EW)
  t.is(score['NS'], 160)
})

test('hoolScore function: test #6',async t => {
  contract = { declarer: 'S', level: 12-6, suit: 'H', redouble: true }
  tricks_NS = 13
  tricks_EW = 0

  let score =await new Table().hoolScore(contract, tricks_NS, tricks_EW)
  t.is(score['NS'], 1080)
})

test('hoolScore function: test #7',async t => {
  contract = { declarer: 'W', level: 13-6, suit: 'S', double: true }
  tricks_NS = 1
  tricks_EW = 12

  let  score =await new Table().hoolScore(contract, tricks_NS, tricks_EW)
  t.is(score['NS'], 40)
})

test('hoolScore function: test #8',async t => {
  contract = { declarer: 'N', level: 10-6, suit: 'S', double: false }
  tricks_NS = 11
  tricks_EW = 2

  let  score =await new Table().hoolScore(contract, tricks_NS, tricks_EW)
  t.is(score['NS'], 150)
})

test('hoolScore function: test #9',async t => {
  contract = { declarer: 'S', level: 11-6, suit: 'C', double: true }
  tricks_NS = 11
  tricks_EW = 2

  let  score = await new Table().hoolScore(contract, tricks_NS, tricks_EW)
  t.is(score['NS'], 300)
})

/**
 * Final cleanup
 *
 * Make sure the database connections are closed, and
 * delete all the test tables (if we were using a test
 * database) to make life easier for the next tester.
 *
 * Note that "if we're using a test database" is the
 * operative term here. If it's the main database we're
 * using (for some silly reason) then we shouldn't
 * delete any tables, because that *will* make life
 * difficult for everybody who's currently playing
 * and sees their tables vanish mid-game!
 */

test.after.always('guaranteed cleanup', async t => {
  // clean up if test database
  if (!!process.env.TABLEBOT_MONGODB_TEST_URL) {
    await Table.deleteMany()
  }

  await mongoose.connection.close()
})

// ...aand that's it: we're done! Yayy!!
